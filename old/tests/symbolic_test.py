#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 12 11:11:54 2020

Test some symbolic stuff

@author: nleht
"""

import numpy as np
import local_symbolic_array as SY
import local_common as CO
#from common import customize, decustomize

#%% For demo, we represent matrices as dense. But usually, we must have SET_SPARSE(True)!
CO.set_global_debug_level(2)
SY.set_sparse(False)

#%% Elementary operations
print('\n\n********** Elementary operations **********')
f = SY.SymbolicArray('f',5)
# All linear operations on f are represented as linear operators are M*x+V.
# The original vector is, of course, the unit operator
print('\nOriginal vector is represented by unit operator\n f.action.equation =\n',f.action.equation)
# The indexing is also a linear operator
print('\nOperator for f[1] =\n', f[1].action.equation)
# More complicated indexing involving slices, and operations for addition

#%% How to do something like this?
ave = f.allocate((f.ref.n,))
# ave.constrained = False by default!
ave[1:] = (f[1:] + f[:-1])/2
ave[0] = f[0]
# -- in order not to set ((f[1]+f[0])/2)[:] = f[0] instead of updating the operator

print('\nAveraging operator =\n ', ave.action.equation)

#%% Elementary operations
print('\n\n********** Elementary operations **********')
f2 = SY.SymbolicArray('f2',5)
# How to do something like this?
ave2 = (f2[1:] + f2[:-1])/2
ave2[0] = f2[0]
# -- in order not to set ((f[1]+f[0])/2)[:] = f[0] instead of updating the operator

print('\nAveraging operator with constraint =\n ', ave2.action.equation)

#%%
#g = SY.np_hstack((f[1]+f[2],f[2]+f[3]))
