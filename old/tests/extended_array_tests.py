#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun 13 11:13:18 2020

OBSOLETE, DO NOT USE!

@author: nleht
"""

import numpy as np
from fidelior import SY, common, Grid, Geometry1D, ExtendedArray1D, end, \
    Geometry2D, ExtendedArray2D, n_dif

#%% Stage 1
grid = Grid(n_cells = 5, delta = 0.2, start = 0.0)
geom = Geometry1D(grid)
f = ExtendedArray1D(geom,stag=1,nl=1,nu=1)
f.alloc(np.float)
f[:] = np.arange(1,6)
f[-1]=10
print(np.sin(f))
g = f.view[:] # want to get rid of this slicing

#%% Stage 2
f[end+1]=-10
try:
    f[end+2]
except IndexError as e:
    print('Caught exception:',repr(e))
# Copy constructor
ff = ExtendedArray1D.copy(f)
#%% Negative nl, nu
g = ExtendedArray1D(geom,stag=1,nl=-1,nu=-1)
g.alloc(np.float)
try:
    g[0]
except IndexError as e:
    print('Caught exception:',repr(e))
#%% Fake extended arrays
h = ExtendedArray1D(geom,stag=1,nl=1,nu=1)
SY.set_sparse(False)
# Instead of allocation, do a fake
h.alloc_symbolic('h')
h[-1] = 28
print('Symbolic Array h =\n',h)
print('The main matrix h.arr.op.M =\n',h.arr.op.M)
print('The constraint matrix h.arr.ref.cnst.op.M =\n',h.arr.ref.cnst.op.M)
hr = ExtendedArray1D.copy(h)
hr.arr = common.customize(np.arange(hr.nt))

#%% 2D Extended Arrays

#%% Stage 1
np.set_printoptions(threshold=1000)
gridx = Grid(n_cells = 5, delta = 0.2, start = 0.0)
gridy = Grid(n_cells = 10, delta = 0.2, start = 0.0)
geom = Geometry2D(gridx,gridy)
f = ExtendedArray2D(geom,stags=(1,1),nls=(1,1),nus=(1,1))
f.alloc(np.float)
f[:,:] = np.arange(50).reshape(5,10)
f[-1,:]=10
print(f)
print(np.sin(f))
#%% Stage 2
f[end+1,:]=-10
try:
    f[end+2,:]
except IndexError as e:
    print('Caught exception:',repr(e))
# Copy constructor
ff = ExtendedArray2D.copy(f)
#%% Negative nl, nu
g = ExtendedArray2D(geom,stags=(1,1),nls=(-1,1),nus=(-1,1))
g.alloc(np.float)
try:
    g[0,:]
except IndexError as e:
    print('Caught exception:',repr(e))
#%% Symbpolic extended arrays
gridy = Grid(n_cells = 2, delta = 0.2, start = 0.0)
geom = Geometry2D(gridx,gridy)
hn = ExtendedArray2D(geom,stags=(1,1),nls=(0,1),nus=(0,1))
#hn.alloc(np.float)
hn.arr=np.arange(np.prod(hn.nts)).reshape(hn.nts)
SY.set_sparse(True)
hs = ExtendedArray2D(geom,stags=(1,1),nls=(0,1),nus=(0,1))
hs.alloc_symbolic('h')
hs[:,-1] = 28
print('Symbolic Array hs =\n',hs)
print('The main matrix hs.arr.op.M =\n',hs.arr.op.M)
print('The constraint matrix hs.arr.ref.cnst.op.M =\n',hs.arr.ref.cnst.op.M)
#%%
hs.lo(0)
n_dif(hs,0)