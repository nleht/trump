#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun 13 10:49:43 2020

OBSOLETE, DO NOT USE!

@author: nleht
"""

import numpy as np
from fidelior import common
from fidelior.symbolic_arrays import set_sparse, symbolic_array

#%% Preliminaries. Also, define some functions
common.set_global_debug_level(2) # We could do common.DEBUGLEVEL = 2 but user should use this!
set_sparse(False) # Analgogously, USE_SPARSE=False

def get_dif(a):
    "User function, not part of package"
    return a[1:]-a[:-1]
def get_ave(a):
    "User function, not part of package"
    return (a[1:]+a[:-1])/2
def get_lap(a):
    "User function, not part of package"
    return get_dif(get_dif(a))

#%% Testing 1
print('\n\n********** TEST 1 **********\n')
f = SymbolicArray('f',5)
ave = (f[1:] + f[:-1])/2
dif = f[1:] - f[:-1]
ave_op = ave.action
dif_op = dif.action
print('ave_op =',ave_op,'\n\ndif_op =',dif_op,'\n')

#%% Testing 2
print('\n\n********** TEST 2 **********\n')
f = SymbolicArray('f',5)
# A trick to make operations like v * f and v + f work with ndarray v and
# arbitrary class f (NOTE: f * v and f + v work fine even without it)
v = common.customize(np.arange(1,6))
mul = v*f
mul_op = mul.action
print('mul_op =',mul_op,'\n')
add = v + f
add_op = add.action
print('add_op =',add_op,'\n')
sub1 = v - f
sub1_op = sub1.action
print('sub1_op =',sub1_op,'\n')
sub2 = f - v
sub2_op = sub2/f
print('sub2_op =',sub2_op,'\n')
div = f / v
div_op = div/f
print('div_op =',div_op,'\n')
notweird1 = f * v[1]
print('notweird1 =',notweird1)
tmp = v[1]
# Do NOT do this!:
weird = tmp * f
print('weird =',weird)
# Instead, use "customization" again!
notweird2 = common.customize(tmp) * f
print('notweird2 =',notweird2)

#%% Testing 3
print('\n\n********** TEST 3 **********\n')
f = SymbolicArray('f',5)
#f[:1]=np.array([1])
#f[-1:]=np.array([-1])
f[0]=1
f[-1]=-1
print(f.ref.cnst)
lap = get_lap(f)
lap_op = lap/f
print('lap_op =',lap_op,'\n')
rho = np.array([1,2,3])
phi = lap_op.solve(rho)
print('phi =',phi)
# introduce intentional error
phi[0]=phi[0]+0.1
print('phi_err =',phi)
(rho,err) = lap_op(phi)
print('rho =',rho,', err =',err)
op = lap_op.simplify
rho = op(phi[~lap_op.is_dependent])
print('Applied to indep components only: rho =',rho)

#%% Testing 4
print('\n\n********** TEST 4 **********\n')
# Note that here we have a periodic bc, so we also need a gauge condition, strictly speaking
# and must have sum(rho)==0
f = SymbolicArray('f',6)
f[:2]=f[-2:]
print(f.ref.cnst)
lap = get_lap(f)
lap_op = lap/f
print('lap_op =',lap_op,'\n')
phitest = np.array([1,2,3,4,1,2])
rho,err = lap_op(phitest)
print('\n\tphitest =',phitest,'\n\trho =',rho,'\n\terr=',err)
phisol = lap_op.solve(rho)
(rhonew,errnew) = lap_op(phisol)
print('\n\tphisol =',phisol,'\n\trhonew =',rhonew,'\n\terrnew=',errnew)

#%% Testing 5
for array_like in [False,True]:
    #
    print('\n\n********** TEST 5 **********\n')
    f = SymbolicArray('f',5,array_like=array_like)
    f[0]=f[-1:]
    # Constraint on the sum can be written as this:
    if array_like:
        f[1] = -f[0]-sum(f[2:]) + 1
    else:
        # incorrect syntax for real arrays
        sum(f)[:] = 1
    lap = get_dif(get_dif(f))
    lap_op = lap/f
    print('lap_op =',lap_op,'\n')
    rho=np.array([1,2,3])
    phi = lap_op.solve(rho)
    print('\n\trho =',rho,'\n\tphi =',phi,'\n\tsum(phi) =',np.sum(phi))
    lap_s = lap_op.simplify
    phi = lap_s.solve(rho)
    print('\n\trho =',rho,'\n\tphi =',phi,'\n\tsum(phi) =',np.sum(phi))

#%% Testing 6
print('\n\n********** TEST 6 **********\n')
# The wrong way to set up dependeces. Still does for array_like=False
f = SymbolicArray('f',5)
g = f[0] + f[1]
g[:] = 3.14
(f[0] + f[2])[:] = 2.78
# black magic
gop = f/f
op = gop.simplify
print('The independent components are',
      [i for (i,j) in enumerate(~gop.is_dependent) if j])
rhs = np.array([1,2,3])
sol = op(rhs)
print('sol =',sol)
print('Conditions check:')
print('sol[0]+sol[1]=',sol[0]+sol[1],'(must be 3.14)')
print(g.op(sol))
print('sol[0]+sol[2]=',sol[0]+sol[2],'(must be 2.78)')
h = f[0] + f[2]
print(h.op(sol))

#%% Same, the correct way
print('\n\n********** TEST 6A **********\n')
f = SymbolicArray('f',5,array_like=True)
f[0] = 3.14 - f[1]
f[2] = 2.78 - f[0]
# black magic
gop = f/f
op = gop.simplify
dep = gop.is_dependent
print('Indep:', [i for (i,j) in enumerate(dep) if not j],
      '; dep:', [i for (i,j) in enumerate(dep) if j])
rhs = np.array([1,2,3])
sol = op(rhs)
print('sol =',sol)
print('Conditions check:')
print('sol[0]+sol[1]=',sol[0]+sol[1],'(must be 3.14)')
print((f[0]+f[1]).op(sol))
print('sol[0]+sol[2]=',sol[0]+sol[2],'(must be 2.78)')
print((f[0]+f[2]).op(sol))
    
#%% Assigning and addition of single value to multiple indices
print('\n\n********** TEST 7 **********\n')
f = SymbolicArray('f',6,array_like=True)
f[0:2]=2*f[2]-f[4:2:-1]
print('Constraint is:\n',f.ref.cnst.op)

#%% Concatenation
print('\n\n********** TEST 7 **********\n')
f = SymbolicArray('f',6,array_like=True)
f1 = np_hstack((f[0],2*get_ave(f[1:])))
print('Operater np.hstack((f[0],2*get_ave(f[1:]))) is\n',f1.op)
