#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 16 21:35:09 2020

Stencils and difference schemes on the plane (2D)

See also advection2d_uniform_const_v.py

@author: nleht
"""

import numpy as np
import matplotlib.pyplot as plt
import fidelior as fdo
from fidelior import end, half, span
from fidelior import automatic_schemes as AS

fdo.set_global_debug_level(0)
#fdo.co.BOLD = False
fdo.print_motto()
if not fdo.numpy_override.entered:
    fdo.numpy_override.enter()

# Set up the grid and geometry
N = 100
ndim = 2
grids = tuple(fdo.Grid(num_cells=N) for k in range(ndim))
geom = fdo.GeometryCartesian(grids, nls=(5,5), nus=(5,5)) # ample wiggle room
# x = grids[0].r_n
# y = grids[1].r_n
# xc = grids[0].r_c
# yc = grids[1].r_c
# xe = xc[-half:end+half].flatten()
# ye = yc[-half:end+half].flatten()

####################################################################################################
#%% UT0 scheme may be represented as flux stuff (face value times x or y)
Arbx = AS.stencil_polynomial(2,1,[(0,0), (0,-1)])
Arbx.default()
r = np.random.randn(3)
r = np.array([0,2/3,-2/3]) # makes U(0,0) symmetric in x, y
r = np.array([0,0,0]) # no xy terms
Arbx[(0,0),:] = r
#Arbx = stencil_polynomial(2,1,[(0,0)])
#Arbx.default()
Arbx[(0,-1),:] = -r

Arby = AS.stencil_polynomial(2,1,[(0,0), (-1,0)])
Arby.default()
Arby[(0,0),:] = -r
Arby[(-1,0),:] = r

I3 = AS.stencil_polynomial(2, 3, [(0,0)])
I3.default()
I3[(0,0),(0,0)] = 1

#%% Correction to face value
fvx_p = AS.stencil_polynomial(2, 2, [(0,0), (0,1), (1,0), (0,-1), (-1, 0)])
fvx_p.default()
fvx_p[(-1,0),:] = [-1/6, 0, 0, 0, 0, 1/6] # [ -(1/6) + (1/6)x^2 ]
fvx_p[(0,1),:] =  [0, 1/2, 1/2, 0, 0, 0] # [ (1/2)y + (1/2)y^2 ]
fvx_p[(1,0),:] = [1/3, 0, 0, 1/2, 0, 1/6] # [ (1/3) + (1/2)x + (1/6)x^2 ]
fvx_p[(0,-1),:] = np.array([0, 0, 1/2, 0, 0, 0]) # (1/2)y^2, this is arbitrary! only sum with (0,0) is constrained
fvx_p[(0,0),:] = np.array([-1/6, -1/2, -1, -1/2, 0, -1/3]) # [ (5/6) - (1/2)y - y^2 - (1/2)x - (1/3)x^2 ]
fvx_p = fvx_p + Arbx.times(1)
fvx_m = AS.stencil_polynomial(2, 2, [(-1,0), (-1,1), (0,0), (-1,-1), (-2, 0)])
fvx_m.default()
fvx_m.coefmat = fvx_p.coefmat # must be same
# Symmetric x <-> y
fvy_p = AS.stencil_polynomial(2, 2, [(0,0), (0,1), (1,0), (0,-1), (-1, 0)])
fvy_p.default()
fvy_p[(0,-1),:] = [-1/6, 0, 1/6, 0, 0, 0] # [ -(1/6) + (1/6)y^2 ]
fvy_p[(1,0),:] =  [0, 0, 0, 1/2, 0, 1/2] # [ (1/2)x + (1/2)x^2 ]
fvy_p[(0,1),:] = [1/3, 1/2, 1/6, 0, 0, 0] # [ (1/3) + (1/2)y + (1/6)y^2 ]
fvy_p[(-1,0),:] = [0, 0, 0, 0, 0, 1/2] # (1/2)x^2, see comment above
fvy_p[(0,0),:] =  [-1/6, -1/2, -1/3, -1/2, 0, -1] # [ (5/6) - (1/2)x - x^2 - (1/2)y - (1/3)y^2 ]
fvy_p = fvy_p + Arby.times(0)
fvy_m = AS.stencil_polynomial(2, 2, [(0,-1), (0,0), (1,-1), (0,-2), (-1, -1)])
fvy_m.default()
fvy_m.coefmat = fvy_p.coefmat

#%% Check the result
CIR = AS.interpolating_scheme(geom, 1, AS.odd_order_stencil(ndim, 1))
UT0 = AS.interpolating_scheme(geom, 3, AS.odd_order_stencil(ndim, 2))
# UT0 = I + x (fvx_p - fvx_m) + y (fvy_p - fvy_m)
tmp =  UT0 - CIR.raise_order().raise_order() + fvx_m.times(0) - fvx_p.times(0) + fvy_m.times(1) - fvy_p.times(1)
print('tmp = ',repr(tmp))


#%%#################################################################################################
"""
Triangular grid

It may be obtained from the usual square grid by a linear transformation, so all the
stability properties are the same!!!

However, may be still interesting, e.g., for evaluation of Laplacians.
"""

#%% Some stencil shapes
def to_triangles(stencil):
    x,y = np.array(stencil).T
    return list(zip(x + y/2, np.sqrt(3)/2*y))

def triangular_stencil(ndim, order):
    assert ndim==2
    stencil = []
    for k in range(order+1):
        for l in range(order-k+1):
            stencil.append((l,k))
    return stencil

#%% Triangular (fully upwind)
for order in range(1,5):
    stencil = to_triangles(triangular_stencil(ndim, order))
    scheme = AS.interpolating_scheme(geom, order, stencil)
    # Only order = 1, 2 and 4 are stable
    AS.demo_stability(scheme, 201, 201, xlim=(-1,order+1), ylim=(-1,order+1), ke=7)

#%% Odd order
th = np.linspace(0,1,101)*2*np.pi
r = np.sqrt(3)/2
for n in range(1,5):
    order = 2*n-1
    s = AS.transformed_stencil(AS.odd_order_stencil(ndim, n), scale = -1)
    stencil = to_triangles(s)
    scheme = AS.interpolating_scheme(geom, order, stencil)
    # As expected, stability in the triangle for |c|<sqrt(3)/2
    #AS.demo_stability(scheme, 201, 201, xlim=(-n,n+1), ylim=(-n,n+1))
    AS.demo_stability(scheme, 201, 201, xlim=(-.1,1.1), ylim=(-.1,1.1))
    plt.plot(r*np.cos(th), r*np.sin(th), 'w')

#%% Even order
for n in range(1,5):
    order = 2*n
    s = AS.transformed_stencil(AS.even_order_stencil(ndim, n), shift = 1)
    stencil = to_triangles(s)
    scheme = AS.interpolating_scheme(geom, order, stencil)
    # As expected, stability in the triangle for |c|<sqrt(3)/2
    #AS.demo_stability(scheme, 201, 201, xlim=(-n+1,n+2), ylim=(-n+1,n+2))
    AS.demo_stability(scheme, 201, 201, xlim=(-.1,1.1), ylim=(-.1,1.1), ke=6)
    #plt.plot(r*np.cos(th), r*np.sin(th), 'w')

#%% Odd order, flipped
for n in range(1,5):
    order = 2*n-1
    s = AS.transformed_stencil(AS.odd_order_stencil(ndim, n), scale = (1,-1))
    stencil = to_triangles(s)
    scheme = AS.interpolating_scheme(geom, order, stencil)
    # As expected, stability in the triangle for |c|<sqrt(3)/2
    AS.demo_stability(scheme, 201, 201, xlim=(-n,n+1), ylim=(-n,n+1))
    #AS.demo_stability(scheme, 201, 201, xlim=(-.1,1.1), ylim=(-.1,1.1))

#%% Even order, flipped
for n in range(1,5):
    order = 2*n
    s = AS.transformed_stencil(AS.even_order_stencil(ndim, n), scale = (-1,1), shift=(-1,1))
    stencil = to_triangles(s)
    scheme = AS.interpolating_scheme(geom, order, stencil)
    # As expected, stability in the triangle for |c|<sqrt(3)/2
    AS.demo_stability(scheme, 201, 201, xlim=(-n-1,n), ylim=(-n+1,n+1), klim=2)
    #AS.demo_stability(scheme, 201, 201, xlim=(-.1,1.1), ylim=(-.1,1.1), ke=6)
    #plt.plot(r*np.cos(th), r*np.sin(th), 'w')

#%% Laplacians
for n in range(2,4):
    order = 2*n-1
    s = AS.transformed_stencil(AS.odd_order_stencil(ndim, n), scale = -1)
    stencil = to_triangles(s)
    scheme = AS.interpolating_scheme(geom, order, stencil)
    AS.plot_scheme(scheme.lap().partial(0,0).partial(1,0).frac(10))
 

####################################################################################################
#%% Schemes for an Adaptive Mesh Refinement

# Refinement by a factor of 2
UTr_stencil = AS.odd_right_stencil(ndim, 2)
UTl_stencil = AS.odd_left_stencil(ndim, 2)
UTxll = AS.interpolating_scheme(geom, 3, UTl_stencil)
UTxrr = AS.interpolating_scheme(geom, 3, UTr_stencil)

#%% Stability region of the original and shifted scheme
UTxrrs = AS.transformed_scheme(UTxrr, shift=1)
AS.demo_stability(UTxrr, 301, 301, xlim=(-2,1), ylim=(-2,1))
AS.demo_stability(UTxrrs, 301, 301, xlim=(-1,2), ylim=(-1,2))

#%% Symmetrize in all directions
def rotate(scheme):
    return AS.transformed_scheme(scheme, permute=(1,0), scale=(-1,1))
def sym_rot(scheme):
    res = 0
    for k in range(4):
        res = res + scheme/4
        scheme = rotate(scheme)
    return res

UTlsym = sym_rot(AS.transformed_scheme(UTxll, shift=0.5))
UTrsym = sym_rot(AS.transformed_scheme(UTxrr, shift=0.5))

UTsym = (UTlsym+UTrsym)/2

#%%
cross = AS.interpolating_scheme(geom, 2, [(0,0), (1,0), (0,1), (-1,0), (1,1), (0,-2)])
fork = AS.interpolating_scheme(geom, 2, [(0,0), (1,0), (0,1), (-1,0), (1,-2), (-1,-2)])
cross2 = AS.interpolating_scheme(geom, 2, [(0,0), (1,0), (0,1), (-1,0), (0,-2), (2,-2)])

#%% Try to get 3rd order scheme
cross3o = AS.interpolating_scheme(geom, 3, [(0,0), (1,0), (0,1), (-1,0), (0,-2),
                                (2,-2), (1,1), (-1,1), (2, 0), (0, 2)])
cross3o_lap = cross3o.lap().partial(0,0).partial(1,0)
print('Laplacian at vertex =',repr(cross3o_lap))
AS.plot_scheme(cross3o_lap)

#%%
fork3o = AS.interpolating_scheme(geom, 3, [(0,0), (1,0), (0,1), (-1,0), (1,-2), (-1,-2),
                               (1,1), (-1,1), (2, 0), (0, 2)])
fork3o_lap = fork3o.lap().partial(0,0).partial(1,0)
print('Laplacian at edge =',repr(fork3o_lap))
AS.plot_scheme(fork3o_lap)

#%% Only one point sticks out
fork3oA = AS.interpolating_scheme(geom, 3, [(0,0), (1,0), (0,1), (0,2), (-1,0), (1,-2),
                               (1,1), (-1,1), (-1,2), (-2, 1)])
fork3oA_lap = fork3oA.lap().partial(0,0).partial(1,0)
print('Laplacian at edge =',repr(fork3oA_lap))
AS.plot_scheme(fork3oA_lap)

#%% Only one point sticks out
fork3oB = AS.interpolating_scheme(geom, 3, [(0,0), (1,0), (0,1), (0,2), (-1,0), (-1,-2),
                               (1,1), (-1,1), (-1,2), (-2, 1)])
fork3oB_lap = fork3oB.lap().partial(0,0).partial(1,0)
print('Laplacian at edge =',repr(fork3oB_lap))
AS.plot_scheme(fork3oB_lap)

#%% Try something else
cross3o1 = AS.transformed_scheme(UTxll, scale=2)
cross3o1_lap = cross3o1.lap().partial(0,0).partial(1,0)
print('Another laplacian at vertex =',repr(cross3o1_lap))

#%%
fork3o1 = AS.transformed_scheme(UTxll, scale=2, shift=(1,0))
fork3o1_lap = fork3o1.lap().partial(0,0).partial(1,0)
print('Another laplacian at edge =',repr(fork3o1_lap))
AS.plot_scheme(fork3o1_lap)

#%%
grids = ([-3,-1,1,3],[-4,-2,0,1])
fork3o2 = (AS.interpolating_scheme(geom, 3, AS.stencil_on_Grid(UTl_stencil, grids)) +
           AS.interpolating_scheme(geom, 3, AS.stencil_on_Grid(UTr_stencil, grids)))/2    

#def square_stencil(*grids):
#    return list(zip(*[x.flatten() for x in fdo.co.ndgrid(*grids)]))

    
#%% Adaptive Mesh Refinement by a factor of R
R = 5
AMR = []
for k in range(R+1):
    AMR.append( AS.interpolating_scheme(geom, 2, [(0,0), (1,0), (0,1), (-1,0), (-k,-R), (R-k,-R)],
                                      is_symbolic=True))
print(fdo.co.blue('***** AMR, R = '+str(R)+' *****'))
for k in range(R+1):
    print(fdo.co.blue('Laplacian for shift = ' + str(k)))
    print(repr(AMR[k].lap()))
    AS.plot_scheme(AMR[k].lap())

#%%
# Another expression
AMR1 = []
UTxllS = AS.interpolating_scheme(geom, 3, UTl_stencil, is_symbolic=True)
for k in range(R+1):
    scheme = AS.transformed_scheme(UTxllS, scale=R, shift=(R-k,0))
    AMR1.append(scheme)
    print(fdo.co.blue('Laplacian for shift (alternative) = ' + str(k)))
    lap = scheme.lap().partial(0,0).partial(1,0).frac(10)
    print(repr(lap))
    AS.plot_scheme(lap)

#%%
def cover_demo_stability(scheme):
    x, y = np.array(scheme.stencil).T
    AS.demo_stability(scheme, 201, 201, xlim=(min(x),max(x)), ylim=(min(y), max(y)), ke=6)

def AMR_scheme(R,k):
    sr = AS.transformed_stencil(AS.odd_right_stencil(ndim, 2), shift=(0,1))
    sr[sr.index((0,-1))] = (k, -R)
    sr[sr.index((-1,-1))] = (k-R, -R)
    scr = AS.interpolating_scheme(geom, 3, sr)
    sl = AS.transformed_stencil(AS.odd_left_stencil(ndim, 2), shift=(0,1))
    sl[sl.index((0,-1))] = (k, -R)
    scl = AS.interpolating_scheme(geom, 3, sl)
    scheme = (scr + scl)/2
    return scheme

R = 5
for k in range(R):
    cover_demo_stability(AMR_scheme(R,k))