#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 22 11:05:13 2020

@author: nleht
"""

import numpy as np

def broadcast_ravel_multi_index(*index_shape_s):
    """
    Calculate raveled index into each array (i.e., index into a flattened array)

    Parameters
    ----------
    *index_shape_s : args
        One or more (index, shape) tuples.
    index : tuple (multi-index) with ``None``s in some positions
        Index into an array. `None` present in i1 indicate dimensions which should be broadcast.
    shape : tuple
        Shape of an array

    Returns
    -------
    A tuple of arrays with indices.

    """
    subshapes = ()
    tot_indices =[]
    for index, shape in index_shape_s:
        #print('index =',index,', shape =',shape)
        tot_index = np.arange(np.prod(shape)).reshape(shape)
        tot_indices.append(tot_index[index])
        subshapes += (tot_index[index].shape,)
    #print('totindices =', tot_indices)
    #print('subshapes =', subshapes)
    conj_reps = ()
    for elem_dims in zip(*subshapes):
        #print('elem_dims =', elem_dims)
        res_dim = np.max(elem_dims)
        for k in elem_dims:
            assert k==1 or k==res_dim
        conj_reps += (tuple(res_dim if k==1 else 1 for k in elem_dims),)
    reps = tuple(zip(*conj_reps))
    #print('reps =',reps)
    return tuple(np.tile(tot_index, rep).flatten() for tot_index, rep in zip(tot_indices, reps))

#%%
i1 = (None, slice(1,3,None))
shape1 = (6,)
i2 = (slice(0,4,None),None)
shape2 = (7,)
ishapes = ((i1,shape1), (i2, shape2))

#%%
res  = broadcast_ravel_multi_index(*ishapes)
print(res)

#%% More testing
dims = np.array([10, 13, 18])
sls = [slice(1,3,None), slice(4,None,None), slice(-4, None, -1)]

mfull = np.random.randn(*dims)
a0 = np.random.randn(dims[1], dims[2])
a1 = np.random.randn(dims[0], dims[2])
a2 = np.random.randn(dims[0], dims[1])
ia0 = (None,sls[1],sls[2])
ia1 = (sls[0],None,sls[2])
ia2 = (sls[0], sls[1], None)
b0 = np.random.randn(dims[0])
b1 = np.random.randn(dims[1])
b2 = np.random.randn(dims[2])
ib0 = (sls[0], None, None)
ib1 = (None, sls[1], None)
ib2 = (None, None, sls[2])

#%% Test 1
resc = a0[ia0] + a1[ia1]

ria0, ria1 = broadcast_ravel_multi_index((ia0, a0.shape), (ia1, a1.shape))

res = a0.flat[ria0] + a1.flat[ria1]

assert (resc.flat-res == 0).all()

#%% Test 2
resc = b0[ib0] + a2[ia2]
rib0, ria2 = broadcast_ravel_multi_index((ib0, b0.shape), (ia2, a2.shape))
res = b0.flat[rib0] + a2.flat[ria2]
assert (resc.flat == res).all()