#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 10 23:10:43 2020

Test of cylindrical Laplacian in a nonuniform grid.

The theory predicts a very strange formula.

@author: nleht
"""

import numpy as np
import matplotlib.pyplot as plt
import fidelior as fdo
from fidelior import end, span, half, c_diff, c_mean, n_diff, n_mean, random_grid_delta
from utils import anti_diff
#from fdo.extras2D import ea_plot
fdo.set_global_debug_level(3)
N = 20
L = 20
delta = random_grid_delta(L, N, 0.9)

#%% Old way to do it
grid = fdo.Grid(num_cells=N, delta=delta, start=0) # use delta=1 just for simplicity
geom = fdo.Geometry((grid,), nls=(3,), nus=(3,))
#grid.add_Geometry(geom, 0, nl=3, nu=3)
r = grid.r_n
rtrick = r.copy_arr() # not const
phi = 1-r**2
# Theoretical Laplacian of this is -4

#%% Check various schemes
Delta = c_diff(r) # same as r.hi()-r.lo() and same as grid.c_dr
rc = c_mean(r) # could have used also grid.c_r
rmean = n_mean(rc)
# Now, a trick: r_0 != 0
rtrick[0] = 1
grad = c_diff(phi)/Delta
Delta0 = n_mean(Delta) # same as grid.n_dr
carlap = n_diff(grad)/Delta0

#%%
# Naive laplacian
naive_rlap = n_diff(rc*grad)/Delta0
naive_lap = naive_rlap/rtrick

#%% Last formula, looks more convenient
correction = 2*(1-rmean/rtrick)*carlap
lap = naive_lap + correction

#%% Next to last formula (unfortunately, r=0 point is wrong)
lap2 = naive_lap - n_diff(Delta)*carlap/2/rtrick

# # This is screwed up
# rb0 = r.copy_arr()
# rb0[0] = 1
# rc1_hi = rb0.lo() + Delta.shifted(-1)/2
# rc1_lo = rb0.hi() - Delta.shifted(1)/2
# rlap1 = ((rc1_hi*grad_phi).hi()-(rc1_lo*grad_phi).lo())/Delta0
# lap1 = rlap1/rb0
# lap1[0] *= 2

# Another way to implement it which only involves "allowed" operations

#%% Using the formula for the average
grad_ave = n_mean(grad/Delta)/n_mean(1/Delta)
lap3 = carlap + grad_ave/rtrick


#%%
#ea_plot(phi)
plt.figure(1)
plt.clf()
plt.plot(r[span(phi)],phi[span],'.-')

plt.figure(2)
plt.clf()
plt.plot(r[span(lap)],lap[span],'.-')
plt.plot(r[span(lap)],naive_lap[span],'o-')
plt.plot(r[span(lap)],lap2[span],'x-')
plt.plot(r[span(lap)],lap3[span],'v-')

###################################################################################################
#%% Test the new cylindrical geometry
gridr = fdo.Grid(num_cells=N, delta=delta, start=0) # use delta=1 just for simplicity
geomcyl = fdo.GeometryCylindrical((gridr,), ('r',), nls=(3,), nus=(3,))
r = gridr.r_n
phi = 1-r**2
phigrad = geomcyl.grad(phi,0)
philap = geomcyl._div(phigrad, 0)
plt.figure(3)
plt.clf()
plt.plot(r[span(philap)], philap[span], '.-')