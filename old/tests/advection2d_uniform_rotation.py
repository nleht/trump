#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 25 13:35:17 2020

Test of various advection schemes in 2D non-uniform grids, with constant velocity

Periodic boundary conditions, automatic schemes with given stencil

@author: nle003
"""

#%% Mandatory imports
import sys
import time
import numpy as np
import matplotlib.pyplot as plt
#import itertools
import fidelior as fdo
from utils import nice_print, nice_figures
from fidelior import end, half, span, extended_array, field
from fidelior import n_mean, c_mean, n_diff, c_diff, n_ups, c_ups, n_upc, n_interp_ups, c_interp_ups
from fidelior.plotting import UpdateablePcolor, ea_plot
from fidelior import automatic_schemes as AS
from utils import makedir
#from fdo.automatic_schemes import stencil_polynomial, auto_scheme

nice_print()
nice_figures(False)
fdo.set_global_debug_level(0)

if not fdo.numpy_override.entered:
    fdo.numpy_override.enter()

#%% Set up the grid and geometry
Nx = 100
Ny = 100
Lx = 100
Ly = 100
case = 'uniform'
if case=='uniform':
    dx = Lx/Nx
    dy = Ly/Ny
elif case=='random':
    random_coef = 0.5
    dx = fdo.random_grid_delta(Lx, Nx, random_coef)
    dy = fdo.random_grid_delta(Ly, Ny, random_coef)
elif case=='growing':
    refine_coef = 0.1
    dx = fdo.growing_grid_delta(Lx, Nx, Lx/Nx*refine_coef)
    dy = fdo.growing_grid_delta(Ly, Ny, Ly/Ny*refine_coef)
else:
    raise Exception('unknown case')
gridx = fdo.Grid(num_cells=Nx, delta=dx, start=-Lx/2, periodic=True)
gridy = fdo.Grid(num_cells=Ny, delta=dy, start=-Ly/2, periodic=True)
geom = fdo.GeometryCartesian((gridx, gridy), nls=(5,5), nus=(5,5)) # ample wiggle room
x = gridx.r_n
y = gridy.r_n
xc = gridx.r_c
yc = gridy.r_c
xe = xc[-half:end+half].flatten()
ye = yc[-half:end+half].flatten()

#%% Rotation field
Rmax = np.sqrt((Lx/2)**2 + (Ly/2)**2)
vmax = 1.4
vangle=vmax/Rmax # angular velocity

# Zero-divergence velocity, curl of psi*z_unit
psi = fdo.ExtendedArray(geom.ncells, stags=(1,1), nls=(5,5), nus=(5,5))
psi.arr = np.full(psi.nts, fill_value=np.nan)
rc = np.sqrt(xc**2+yc**2)
psi.setv = -rc**2/2*vangle # at points (i-1/2,j-1/2)
vx =  geom.grad(psi, 1) # vx at points (i-1/2,j)
vy = -geom.grad(psi, 0) # vy at points (i,j-1/2)

#%% Odd-order schemes of order N = 2*n-1, odd_order_scheme(geom, n)
def basic_odd(geom, n):
    "Slightly less stable, but should be OK for sum(vi)<~1"
    stencil = AS.odd_order_stencil(geom.ndim, n)
    scheme = AS.interpolating_scheme(geom, 2*n-1, stencil)
    return AS.all_directions_scheme(scheme)

def stable_scheme(geom, order):
    "For vi<1, except order=1"
    return AS.all_directions_scheme(AS.most_stable_scheme(geom, order))

if True:
    ORD1 = stable_scheme(geom, 1)
    ORD3 = stable_scheme(geom, 3)
    ORD5 = stable_scheme(geom, 5)
    ORD7 = stable_scheme(geom, 7)
    ORD9 = stable_scheme(geom, 9)
else:
    ORD1 = basic_odd(geom, 1) # 3 stencil points; 5 in the covering stencil
    ORD3 = basic_odd(geom, 2) # 10 stencil points; 13 in the covering stencil
    ORD5 = basic_odd(geom, 3)
    ORD7 = basic_odd(geom, 4)
    ORD9 = basic_odd(geom, 5) # 55 stencil points; 61 in the covering stencil
SCHEMES = {'ORD1':ORD1, 'ORD3':ORD3, 'ORD5':ORD5, 'ORD7':ORD7, 'ORD9_st':ORD9}

#%% The previous location
dt = 1
if False:
    # The exact position of the previous point
    r = np.sqrt(x**2+y**2)
    th = np.arctan2(y, x)
    vxndt = r*(np.cos(th)-np.cos(th-vangle*dt))
    vyndt = r*(np.sin(th)-np.sin(th-vangle*dt))
else:
    # Interpolate v -- this is more useful if the exact position of the previous point
    # cannot be calculated. Much better results than, e.g., just taking a mean or upstream value.
    vxc = n_mean(vx, 0)
    vyc = n_mean(vy, 1)
    vxn = ORD3.interpolate(vxc, (-vxc*dt, -vyc*dt))
    vyn = ORD3.interpolate(vyc, (-vxc*dt, -vyc*dt))
    vxndt = (vxn + vxc)*dt/2
    vyndt = (vyn + vyc)*dt/2
dr_dt_ago = (-vxndt, -vyndt) # Where the previous point is

#%% The initial value and velocity
fini = ExtendedArray(geom.ncells, stags=(0, 0), nls=(0, 0), nus=(0, 0))
fini.arr = np.zeros(fini.nts)
mask = (np.abs(x)<Lx/4) & (np.abs(y-Ly/6)<Ly/6)
fini[mask] = 1
def smear(f,n):
    for k in range(n):
        f.setv = n_mean(c_mean(n_mean(c_mean(f,0),0),1),1)
    return f
fini = smear(fini, 5)

assert all([gr.uniform for gr in geom.grids]) # Nonunform will be in version 2
#methods = ['LN', 'ORD5', 'ORD7'] # odd-order schmes
methods = ['ORD9'] # very slow
methods = ['ORD3'] # very quick
nmethods = len(methods)

#%% Initial values
farr=[]
for kmethod, method in enumerate(methods):
    d = AS.get_boundary_thickness(SCHEMES[method]) # SCHEMES[method].order
    f = Field('f_'+method, geom.ncells, stags=(0,0), nls=(d,d), nus=(d,d))
    # The boundary conditions may set f0.bc[0]=f0.bc[end], too -- 
    # it is independent of direciton
    #f0.bc[-d:end+d, end:end+d] = f0.bc[-d:end+d, 0:d]
    #f0.bc[-d:end+d, -d:-1]     = f0.bc[-d:end+d, end-d:end-1]
    f.bc[span,    end:end+d]  = f.bc[span,            0:d]
    f.bc[span,        -d:-1]  = f.bc[span,    end-d:end-1]
    f.bc[end:end+d, 0:end-1]  = f.bc[0:d,         0:end-1]
    f.bc[-d:-1,     0:end-1]  = f.bc[end-d:end-1, 0:end-1]
    f.bc.freeze()
    f.setv = 0
    f.setv = fini
    f.bc.apply()
    farr.append(f)

plt.close('all')
figs = []
savedirs = methods
for kmethod in range(nmethods):
    #makedir(savedirs[kmethod])
    fig = UpdateablePcolor(100+kmethod, zmin=-0.5, zmax=1.5) #, savedir=savedirs[kmethod])
    figs.append(fig)

#%% Let's go!
Nt = 10000
t = 0
for kt in range(Nt+1):
    for kmethod, method in enumerate(methods):
        t0 = time.time()
        farr[kmethod].setv = SCHEMES[method].interpolate(farr[kmethod], dr_dt_ago)
        assert not np.isnan(farr[kmethod].arr).any()
        farr[kmethod].bc.apply()
        #print('dt =', time.time()-t0)
        assert not np.isnan(farr[kmethod].arr).any()
    t += dt
    if kt<100 or (kt<500 and kt % 10==0) or kt % 100==0:
        for kmethod in range(nmethods):
            fig = figs[kmethod]
            f = farr[kmethod]
            alg = methods[kmethod]
            if fig.started_plotting and not plt.fignum_exists(fig.fignum):
                print('Figure closed, exiting ...')
                plt.close('all')
                sys.exit()
            the_title = alg + ' in [{:g}, {:g}], nturn = {:,g}'.format(
                np.min(f), np.max(f), kt*vangle/(2*np.pi))
            fig.plot(geom, f, title=the_title)
        plt.pause(.1)
