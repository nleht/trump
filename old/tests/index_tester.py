#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul  2 14:11:28 2020

Two extended_arrays sharing the same arr (also symbolic ones)

@author: nle003
"""

#%%
import numpy as np
import matplotlib.pyplot as plt
import fidelior as fdo
from fidelior import end, half, span, c_diff, n_diff
from utils import nice_figures
#fdo.co.BOLD = False
fdo.print_motto()
if not fdo.numpy_override.entered:
    fdo.numpy_override.enter()

class IndexTester:
    def __init__(self):
        pass
    def __getitem__(self, i):
        return str(i)

itester = IndexTester()

#%%
print(itester[fdo.ea.reslice(end+half, nc=10, st=1, nl=1, nu=1)])

ea1 = fdo.ExtendedArray(ncells=(10,), stags=(0,), nls=(0,), nus=(1,), is_flat=True)
ea2 = fdo.ExtendedArray(ncells=(5,), stags=(0,), nls=(1,), nus=(0,), is_flat=True)
n = ea1.size + ea2.size
arr = fdo.sy.SymbolicArray('f',n)
addr1 = slice(None, ea1.size, None)
addr2 = slice(ea1.size, None, None)
ea1.arr = arr[addr1] # need these addresses
ea2.arr = arr[addr2]

#%% Boundary conditions
ea1[0] = 1
ea2[end] = -1
ea1[end:end+1] = ea2[-1:0]

#ref = ea1.arr.ref
#assert ea2.arr.ref is ref
#ref.cnst.freeze() # useful only for enforcing!

#%%
lap1 = n_diff(c_diff(ea1))
lap2 = n_diff(c_diff(ea2))

#%% The joint system of equations
laparr = np.hstack((lap1.arr, lap2.arr))
rho1 = ea1.like(ea1.stags, nls=(-1,), nus=(0,))
rho1.arr = np.ones(rho1.nts_real)
rho2 = ea2.like(ea2.stags, nls=(-1,), nus=(0,))
rho2.arr = 2*np.ones(rho2.nts_real)
rhoarr = np.hstack((rho1.arr, rho2.arr))

#%%
constraints = arr.ref.cnst.op
padding = np.zeros((constraints.num_eqs,))
op_full = laparr.op.cat(constraints)
b_full = np.hstack((rhoarr, padding))

tmp = op_full.solve(b_full)

ea1num = ea1.copy()
ea1num.arr = tmp[addr1]
ea2num = ea2.copy()
ea2num.arr = tmp[addr2]

nice_figures()
plt.figure(1)
plt.clf()
x1 = ea1num.dindex[0]
x2 = ea2num.dindex[0]
plt.plot(x1[span], ea1num[span], 'o-')
plt.plot(x2[span] + ea1.ncells[0]+1, ea2num[span], 'x-')
