#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 25 13:35:17 2020

Test of various advection schemes in 2D non-uniform grids, with constant velocity

Periodic boundary conditions, automatic schemes with given stencil

@author: nle003
"""

#%% Mandatory imports
import sys
import numpy as np
import matplotlib.pyplot as plt
#import itertools
import fidelior as fdo
from utils import nice_print, nice_figures
from fidelior import end, half, span
from fidelior.plotting import UpdateablePcolor
from fidelior import automatic_schemes as AS

nice_print()
nice_figures(False)
fdo.co.BOLD = True
fdo.set_global_debug_level(3)
fdo.print_motto()

if not fdo.numpy_override.entered:
    fdo.numpy_override.enter()

#%% Set up the grid and geometry
Nx = 100
Ny = 100
Lx = 100
Ly = 100
case = 'random'
if case=='uniform':
    dx = Lx/Nx
    dy = Ly/Ny
elif case=='random':
    random_coef = 0.5
    dx = fdo.random_grid_delta(Lx, Nx, random_coef)
    dy = fdo.random_grid_delta(Ly, Ny, random_coef)
elif case=='growing':
    refine_coef = 0.1
    dx = fdo.growing_grid_delta(Lx, Nx, Lx/Nx*refine_coef)
    dy = fdo.growing_grid_delta(Ly, Ny, Ly/Ny*refine_coef)
elif case=='wall':
    dx = np.hstack(( np.ones((Nx//2,)), 0.3*np.ones((Nx//2,)) ))
    dx = dx*Lx/np.sum(dx)
    dy = np.hstack(( np.ones((Ny//2,)), 0.5*np.ones((Ny//2,)) ))
    dy = dy*Ly/np.sum(dy)
else:
    raise Exception('unknown case')
gridx = fdo.Grid(num_cells=Nx, delta=dx, start=-Lx/2, periodic=True)
gridy = fdo.Grid(num_cells=Ny, delta=dy, start=-Ly/2, periodic=True)
geom = fdo.GeometryCartesian((gridx, gridy), nls=(6,6), nus=(6,6)) # ample wiggle room
x = gridx.r_n
y = gridy.r_n
xc = gridx.r_c
yc = gridy.r_c
xe = xc[-half:end+half].flatten()
ye = yc[-half:end+half].flatten()


#%% One-dimensional schemes
# grid1d = fdo.Grid(num_cells=Nx, delta=dx, start=-Lx/2, periodic=True)
# geom1d = fdo.GeometryCartesian((grid1d,), nls=(5,), nus=(5,)) # ample wiggle room
# CIR1 = AS.auto_scheme(geom1d, 1, [(0,), (-1,)])
# LW1 = AS.auto_scheme(geom1d, 2, [(1,), (0,), (-1,)])
# QUICKEST = AS.auto_scheme(geom1d, 3, [(1,), (0,), (-1,), (-2,)])

#%% Some popular stencils
def odd_order_auto_scheme_nu(geom, n, f_indep):
    return AS.interpolating_scheme_nonuniform(geom, 2*n-1, AS.odd_order_stencil(geom.ndim, n), f_indep)

def right_auto_scheme_nu(geom, n, f_indep):
    #print('right ...',flush=True)
    res = AS.interpolating_scheme_nonuniform(geom, 2*n-1, AS.odd_right_stencil(geom.ndim, n), f_indep)
    #print('right done', flush=True)
    return res

def left_auto_scheme_nu(geom, n, f_indep):
    #print('left ...',flush=True)
    s = AS.odd_right_stencil(geom.ndim, n)
    s = AS.transformed_stencil(s, permute=(1,0))
    res = AS.interpolating_scheme_nonuniform(geom, 2*n-1, s, f_indep)
    #print('left done',flush=True)
    return res

#%% f_indep -- just indicates where the values are updated
f_indep = fdo.ExtendedArray(geom.ncells, stags=(0,0), nls=(0,0), nus=(0,0))
#f_indep = fdo.ExtendedArray(geom.ncells, stags=(0,0), nls=(1,1), nus=(1,1))
# stencil = AS.odd_order_stencil(geom.ndim, 1)
# scheme = AS.auto_scheme_nonuniform(geom, 1, stencil, f_indep)

#%%

CIR = odd_order_auto_scheme_nu(geom, 1, f_indep) # Same as CIR
LN = odd_order_auto_scheme_nu(geom, 2, f_indep) # Same as UT0
LNr = right_auto_scheme_nu(geom, 2, f_indep)
LNl = left_auto_scheme_nu(geom, 2, f_indep)

class composite_scheme:
    def __init__(self, scheme_list, coef_list):
        self.scheme_list = scheme_list
        self.coef_list = coef_list
        stencils = [scheme.stencil for scheme in scheme_list]
        self.stencil = stencils[0]
        for stencil in stencils[1:]:
            self.stencil, _, _ = fdo.co.list_union(self.stencil, stencil)
    def interpolate(self, ea, r):
        res = None
        for scheme, coef in zip(self.scheme_list, self.coef_list):
            t = coef*scheme.interpolate(ea, r)
            if res is None:
                res = t
            else:
                res += t
        return res
    
def add_nu_schemes(scheme_list, coef_list):
    res = None
    for scheme, coef in zip(scheme_list, coef_list):
        if res is None:
            res = AS.interpolating_scheme_nonuniform(scheme._geom, scheme.order, scheme.stencil,
                                            scheme.f_indep, fill=False)
            res._coefmat = coef*scheme._coefmat
            nts = res.f_indep.nts
        else:
            assert (res.f_indep is scheme.f_indep)
            stencil, addr1, addr2 = fdo.co.list_union(res.stencil, scheme.stencil)
            res.stencil = stencil
            npoints = len(stencil)
            dof = len(res.atoms)
            coefmat = np.zeros((npoints, dof)+nts)
            coefmat[addr1,:] += res._coefmat
            coefmat[addr2,:] += coef*scheme._coefmat
            res._coefmat = coefmat
        assert res.atoms==scheme.atoms
    return res
            
LNx1 = composite_scheme([LNr, LNl], [0.5, 0.5]) # stores all these schemes separately

LNx = add_nu_schemes([LNr, LNl], [0.5, 0.5]) # maybe a little faster?

def xsym_scheme_nu(geom, n, f_indep):
    return add_nu_schemes([right_auto_scheme_nu(geom, n, f_indep),
                           left_auto_scheme_nu(geom, n, f_indep)],[0.5, 0.5])

ORD5x = xsym_scheme_nu(geom, 3, f_indep)
print('ORD5x: done',flush=True)
ORD7x = xsym_scheme_nu(geom, 4, f_indep)
print('ORD7x: done',flush=True)
ORD9x = xsym_scheme_nu(geom, 5, f_indep)
print('ORD9x: done',flush=True)

#%%
SCHEMES = {'CIR':CIR, 'LN':LN, 'LNr':LNr, 'LNl':LNl, 'LNx':LNx,
           'ORD5x':ORD5x, 'ORD7x':ORD7x, 'ORD9x':ORD9x}

#%% The initial value and velocity
fini = fdo.ExtendedArray(geom.ncells, stags=(0, 0), nls=(0, 0), nus=(0, 0))
fini.arr = np.zeros(fini.nts)
mask = (np.abs(x)<Lx/4) & (np.abs(y)<Ly/6)
fini[mask] = 1
def smear(f,n):
    for k in range(n):
        f.setv = fdo.n_mean(fdo.c_mean(fdo.n_mean(fdo.c_mean(f,0),0),1),1)
    return f
fini = smear(fini, 10)
# Seed some instabilities
noise = 0
fini += noise*np.random.randn(*fini.nts)

# c = V, so this may be used to determine the stability region, which is sum(V)<1 for odd-order methods
# or all(abs(V)<1) for UTOPIA
V = np.array([.5, .3])

#assert all([gr.uniform for gr in geom.grids]) # Nonunform will be in version 2
dr = np.array([np.min(dx), np.min(dy)])
dti = np.max(np.abs(V)/dr)
#c = V*dt
dt = 0.99/dti
c = V*dt
#methods = ['CIR','LWright','LWleft','UT1']
#methods = ['LWright', 'LWleft', 'LWtail', 'McCormack']
methods = ['LNx','ORD5x','ORD7x','ORD9x']
#methods = ['ORD3', 'ORD5', 'ORD7'] # odd-order schmes
nmethods = len(methods)

#%% Initial values
farr=[]
for kmethod, alg in enumerate(methods):
    d = AS.get_boundary_thickness(SCHEMES[alg])+1 # SCHEMES[method].order
    f = fdo.Field('f_'+alg, geom.ncells, stags=(0,0), nls=(d,d), nus=(d,d))
    # The boundary conditions may set f0.bc[0]=f0.bc[end], too -- 
    # it is independent of direciton
    #f0.bc[-d:end+d, end:end+d] = f0.bc[-d:end+d, 0:d]
    #f0.bc[-d:end+d, -d:-1]     = f0.bc[-d:end+d, end-d:end-1]
    f.bc[span,    end:end+d]  = f.bc[span,            0:d]
    f.bc[span,        -d:-1]  = f.bc[span,    end-d:end-1]
    f.bc[end:end+d, 0:end-1]  = f.bc[0:d,         0:end-1]
    f.bc[-d:-1,     0:end-1]  = f.bc[end-d:end-1, 0:end-1]
    f.bc.freeze()
    f.setv = 0
    f.setv = fini
    f.bc.apply()
    farr.append(f)

plt.close('all')
figs = []
for kmethod in range(nmethods):
    fig = UpdateablePcolor(100+kmethod, zmin=-0.5, zmax=1.5)
    figs.append(fig)

#%% Let's go!
Nt = 10000
t = 0
for kt in range(Nt+1):
    for kmethod, alg in enumerate(methods):
        f = farr[kmethod]
        f.setv = SCHEMES[alg].interpolate(f, -c)
        assert not np.isnan(f.arr).any()
        f.bc.apply()
        assert not np.isnan(f.arr).any()
    t += dt
    if kt<100 or (kt<500 and kt % 10==0) or kt % 100==0:
        for kmethod in range(nmethods):
            fig = figs[kmethod]
            f = farr[kmethod]
            alg = methods[kmethod]
            if fig.started_plotting and not plt.fignum_exists(fig.fignum):
                print('Figure closed, exiting ...')
                plt.close('all')
                sys.exit()
            #plt.figure(100+kmethod)
            #Dx = np.int(t*V[0]/dx)
            #Dy = np.int(t*V[1]/dy)
            the_title = alg + ' in [{:g}, {:g}]'.format(np.min(f), np.max(f))
            fig.plot(geom, f, title=the_title)
        plt.pause(.1)
