#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 10 17:08:14 2020

Gaussian elimination for many matrices at once.

Makes big difference only for K<=16 ON MULTIPROCESSOR COMPUTERS

Total processor time (from all processors) is VERY improved for all K

Otherwise, it is more efficient just to invert the matrices one-by-one

@author: nle003
"""

import numpy as np
import time
from fidelior.common import invert_many_matrices

#%%
Kx = 5
Nx = 1000000
A = np.random.randn(Kx,Kx,Nx)
B = np.zeros((Kx,Kx,Nx))

#%%
def get_error(A, B):
    K = A.shape[0]
    N = A.shape[2]
    AB = np.sum(np.tile(A[:,:,np.newaxis,:],[1,1,K,1])*np.tile(B[np.newaxis,:,:,:],[K,1,1,1]), axis=1)
    return np.max(np.abs(AB - np.tile(np.eye(K)[:,:,None],[1,1,N])))

def mytime():
    return time.process_time() # from all processors
    #return time.perf_counter() # wall time excluding something?
    #return time.time() # wall time

for alg in range(3):
    print('***** Algorithm #',alg,'*****')
    t0 = mytime()
    B = invert_many_matrices(A, alg=alg, out=B)
    print('err =',get_error(A, B),', time =',mytime()-t0, flush=True)

