#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul  9 15:40:25 2020

@author: nleht
"""


#%% Mandatory imports
import sys
import numpy as np
import matplotlib.pyplot as plt
#import itertools
import fidelior as fdo
from utils import nice_print, nice_figures
from fidelior import end, half, span, extended_array, field
from fidelior import n_mean, c_mean, n_diff, c_diff, n_ups, c_ups, n_interp_ups, c_interp_ups
from fidelior.plotting import UpdateablePcolor
from fidelior import automatic_schemes as AS

nice_print()
nice_figures(False)
fdo.co.BOLD = True
fdo.set_global_debug_level(0)
fdo.print_motto()

if not fdo.numpy_override.entered:
    fdo.numpy_override.enter()

def one_dim_stencil(order, up=0):
    return [(n,) for n in range(-((order+1-up)//2),(order+up)//2+1)]

#%% Set up the grid and geometry
N = 100
grid = fdo.Grid(num_cells=N)
geom = fdo.GeometryCartesian((grid,), nls=(5,), nus=(5,)) # ample wiggle room
x = grid.r_n
xc = grid.r_c
xe = xc[-half:end+half].flatten()


#%%
def one_dim_scheme(geom, order):
    return AS.interpolating_scheme(geom, order, one_dim_stencil(order, up=1))

def gamera(geom, order):
    scheme = one_dim_scheme(geom, order)
    return (scheme - scheme.deriv(0).raise_order()/2).partial(0,1/2)

def one_dim_unit(order):
    I = AS.stencil_polynomial(1, order, [(0,)])
    I.default()
    I[(0,),(0,)] = 1
    return I

#%% Find the scheme automagically
def face_value(geom, order):
    """Table 3 in GAMERA paper lists fv.partial(0,0)"""
    scheme = AS.interpolating_scheme(geom, order, one_dim_stencil(order, up=0))
    c = (scheme-one_dim_unit(order)).divide(0).coefmat
    t = -np.cumsum(c, axis=0)[:-1,:]
    fv = AS.stencil_polynomial(1, order-1, one_dim_stencil(order-1, up=1))
    fv.default()
    fv.coefmat = t
    return fv

#%% Decomposition hypothesis
# face_value = Integrate[fv(t), {t, 0, 1}]
# fv(t) = fv0 - u(x=1/2,t=0) + u(x=1/2,t)
# u(x=1/2,t) = u(x=1/2-ct,t=0) because of convection
# u(1/2-ct) = u(1/2) + (-ct)(du/dx) + (-ct)^2/2! (du^2/d^2x) + ...
# After integration we get
# face_value = fv0 + x (du/dx at (1/2))/2! + x^2 (d^u/d^2x at 1/2)/ 3! + ...

def get_term(n, order):
    term = one_dim_scheme(geom, order-1)
    for k in range(n):
        term = term.deriv(0)
    term = term.partial(0,1/2)
    for k in range(1,n+1):
        term = term.times(0)/k
    return term/(n+1)

#%% We have corrections!
order = 4
fv = face_value(geom, order)
rest = -fv + fv.partial(0,0) + get_term(1, order) + get_term(2, order) + get_term(3, order) \
    - get_term(3, order).divide(0).divide(0).raise_order().raise_order()/2
print('rest =',repr(rest))

#%%
order = 5
fv = face_value(geom, order)
rest = -fv + fv.partial(0,0) + get_term(1, order) + get_term(2, order) + get_term(3, order) \
    + get_term(4, order) \
    - get_term(3, order).divide(0).divide(0).raise_order().raise_order()/2 \
    - get_term(4, order).divide(0).divide(0).raise_order().raise_order()*5/6
print('rest =',repr(rest))

#%%
order = 6
fv = face_value(geom, order)
rest = -fv + fv.partial(0,0) + get_term(1, order) + get_term(2, order) + get_term(3, order) \
    + get_term(4, order) + get_term(5, order) \
    - get_term(3, order).divide(0).divide(0).raise_order().raise_order()/2 \
    - get_term(4, order).divide(0).divide(0).raise_order().raise_order()*5/6 \
    - get_term(5, order).divide(0).divide(0).raise_order().raise_order()*5/4 \
    + get_term(5, order).divide(0).divide(0).divide(0).divide(0).raise_order().raise_order()\
        .raise_order().raise_order()*7/16
print('rest =',repr(rest.frac(11)))
