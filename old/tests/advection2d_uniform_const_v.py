#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 25 13:35:17 2020

Test of various advection schemes in 2D non-uniform grids, with constant velocity

Periodic boundary conditions, automatic schemes with given stencil

@author: nle003
"""

#%% Mandatory imports
import sys
import numpy as np
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
#import itertools
import fidelior as fdo
from utils import nice_print, nice_figures
from fidelior import end, half, span, extended_array, field
from fidelior import n_mean, c_mean, n_diff, c_diff, n_ups, c_ups, n_interp_ups, c_interp_ups
from fidelior.plotting import UpdateablePcolor
from fidelior import automatic_schemes as AS
#from fdo.automatic_schemes import stencil_polynomial, auto_scheme

nice_print()
nice_figures(False)
fdo.set_global_debug_level(0)

if not fdo.numpy_override.entered:
    fdo.numpy_override.enter()

#%% Set up the grid and geometry
Nx = 100
Ny = 100
Lx = 100
Ly = 100
case = 'uniform'
if case=='uniform':
    dx = Lx/Nx
    dy = Ly/Ny
elif case=='random':
    random_coef = 0.5
    dx = fdo.random_grid_delta(Lx, Nx, random_coef)
    dy = fdo.random_grid_delta(Ly, Ny, random_coef)
elif case=='growing':
    refine_coef = 0.1
    dx = fdo.growing_grid_delta(Lx, Nx, Lx/Nx*refine_coef)
    dy = fdo.growing_grid_delta(Ly, Ny, Ly/Ny*refine_coef)
else:
    raise Exception('unknown case')
gridx = fdo.Grid(num_cells=Nx, delta=dx, start=-Lx/2, periodic=True)
gridy = fdo.Grid(num_cells=Ny, delta=dy, start=-Ly/2, periodic=True)
geom = fdo.GeometryCartesian((gridx, gridy), nls=(5,5), nus=(5,5)) # ample wiggle room
x = gridx.r_n
y = gridy.r_n
xc = gridx.r_c
yc = gridy.r_c
xe = xc[-half:end+half].flatten()
ye = yc[-half:end+half].flatten()

#%% Popular advection algorithms
def DERIV(f, cx, cy, s):
    return - n_diff(cx*c_ups(f, s*cx, 0), 0) - n_diff(cy*c_ups(f, s*cy, 1), 1)

def next_value(f, c, alg):
    cx, cy = c
    ncx = np.sign(cx)
    ncy = np.sign(cy)
    dfx = c_diff(f,0)
    dfy = c_diff(f,1)       
    fv1x = c_ups(f, cx, 0)
    fv1y = c_ups(f, cy, 1)
    if alg=='CIR':
        # First-order upstream method [Courant, Isaacson and Rees, 1952]
        fnext = f + DERIV(f, cx, cy, 1)
    elif alg=='MacCormack_orig':
        fi = f - n_diff(cx*fv1x, 0) - n_diff(cy*fv1y, 1)
        fnext = (f+fi)/2 - n_diff(cx*c_ups(fi, -cx, 0), 0)/2 - n_diff(cy*c_ups(fi, -cy, 1), 1)/2
    elif alg=='MacCormack':
        # Predictor-corrector, aka Heun
        k1 = DERIV(f, cx, cy, 1)
        k2 = DERIV(f + k1, cx, cy, -1)
        fnext = f + k1/2 + k2/2
    elif alg=='MacCormack_reverse_orig':
        fi = f - n_diff(cx*c_ups(f, -cx, 0), 0) - n_diff(cy*c_ups(f, -cy, 1), 1)
        fnext = (f+fi)/2 - n_diff(cx*c_ups(fi, cx, 0), 0)/2 - n_diff(cy*c_ups(fi, cy, 1), 1)/2
    elif alg=='MacCormack_reverse':
        k1 = DERIV(f, cx, cy, -1)
        k2 = DERIV(f + k1, cx, cy, 1)
        fnext = f + k1/2 + k2/2
    elif alg=='NaiveMacCormack':
        # Midpoint method -- unstable
        k1 = DERIV(f, cx, cy, -1)
        k2 = DERIV(f + k1/2, cx, cy, 1)
        fnext = f + k2
    elif alg=='UTOPIA_Predictor_save':
        # Predictor-corrector. Almost there!
        k1 = DERIV(f, cx, cy, 1)
        k2 = DERIV(f + k1, cx, cy, -1)
        k3 = DERIV(f + k1/2 + k2/2, cx, cy, 1)
        fnext = f + k2/2 + k3/2
    elif alg=='UTOPIA_Predictor':
        # Predictor-corrector
        k1 = DERIV(f, cx, cy, 1)
        k2 = DERIV(f + k1, cx, cy, -1)
        k3 = DERIV(f + k1/2 + k2/2, cx, cy, 1)
        fnext = f + k2/2 + k3/2
    elif alg=='UTOPIA_Kutta':
        # 3rd order Kutta, unstable
        k1 = DERIV(f, cx, cy, -1)
        k2 = DERIV(f + k1/2, cx, cy, 1)
        k3 = DERIV(f - k1 + 2*k2, cx, cy, 1)
        fnext = f + k1/6 + k2*2/3 + k3/6
    elif alg=='UTOPIA_Heun':
        # 3rd order Heun: somehow result looks 1st order!
        k1 = DERIV(f, cx, cy, 1)
        k2 = DERIV(f + k1/3, cx, cy, -1)
        k3 = DERIV(f + k2*2/3, cx, cy, 1)
        fnext = f + k1/4 + k3*3/4
    elif alg=='UTOPIA_Ralston':
        # Ralston 3rd order. Looks 2nd order but almost there!
        k1 = DERIV(f, cx, cy, 1)
        k2 = DERIV(f + k1/2, cx, cy, -1)
        k3 = DERIV(f + k2*3/4, cx, cy, 1)
        fnext = f + k1*2/9 + k2/3 + k3*4/9
    elif alg=='SSPRK3':
        # From wikipedia, https://en.wikipedia.org/wiki/List_of_Runge%E2%80%93Kutta_methods
        k1 = DERIV(f, cx, cy, -1)
        k2 = DERIV(f + k1, cx, cy, -1)
        k3 = DERIV(f + k1/4 + k2/4, cx, cy, 1)
        fnext = f + k1/6 + k2/6 + k3*2/3
    elif alg=='RK4':
        # Classic RK4, should have been 4th order but is in fact second order at best!
        k1 = DERIV(f, cx, cy, 1)
        k2 = DERIV(f + k1/2, cx, cy, -1)
        k3 = DERIV(f + k2/2, cx, cy, 1)
        k4 = DERIV(f + k3, cx, cy, -1)
        fnext = f + k1/6 + k2/3 + k3/3 + k4/6
    elif alg=='UTOPIA':
        # Weighted-averaged with velocity
        dfxw = n_interp_ups(dfx, cy, 1)
        dfyw = n_interp_ups(dfy, cx, 0)
        # Corrected dfx,dfy for 3rd-order method
        dfxc = c_interp_ups(dfxw,(ncx+cx)/3, 0)
        dfyc = c_interp_ups(dfyw,(ncy+cy)/3, 1)
        dfv3 = [[(ncx-cx)/2.*dfxc, -c_ups(n_ups(cy*dfy,cy,1),cx,0)/2.],
                [(ncy-cy)/2.*dfyc, -c_ups(n_ups(cx*dfx,cx,0),cy,1)/2.]]
        # Also works:
        #dfv3 = [[(ncx-cx)/2.*dfxc, -c_ups(n_ups(cy*dfy,cy,1),cx,0)],
        #        [(ncy-cy)/2.*dfyc, 0]]
        fvx = fv1x + dfv3[0][0] + dfv3[0][1]
        fvy = fv1y + dfv3[1][0] + dfv3[1][1]
        fnext = f - n_diff(cx*fvx, 0) - n_diff(cy*fvy, 1)
    # Third-order face values
    elif alg=='dfv_x':
        fnext = ncx*n_mean(dfx, 0)/2 + n_diff(dfx, 0)/12 \
            + cx*n_diff(cx*dfx, 0)/6 + cy*n_diff(cy*dfy, 1)/2 \
            - cx*n_ups(dfx, -cx, 0)/2 - cy*n_ups(dfy, -cy, 1)/2
    elif alg=='dfv_y':
        fnext = ncy*n_mean(dfy, 1)/2 + n_diff(dfy, 1)/12 \
            + cx*n_diff(cx*dfx, 0)/2 + cy*n_diff(cy*dfy, 1)/6 \
            - cx*n_ups(dfx, -cx, 0)/2 - cy*n_ups(dfy, -cy, 1)/2
    elif alg=='dfv_xc':
        fnext = np.sign(cx)*n_mean(dfx, 0)/2 + n_diff(dfx, 0)/12 \
            + cx*n_diff(cx*dfx, 0)/6 + cy*n_diff(cy*dfy, 1)/2 \
            - n_ups(cx*dfx, cy, 1)/2 - n_ups(cy*dfy, cx, 0)/2
    elif alg=='dfv_yc':
        fnext = np.sign(cy)*n_mean(dfy, 1)/2 + n_diff(dfy, 1)/12 \
            + cx*n_diff(cx*dfx, 0)/2 + cy*n_diff(cy*dfy, 1)/6 \
            - cx*n_ups(dfx, -cx, 0)/2 - cy*n_ups(dfy, -cy, 1)/2
    else:
        raise ValueError('Unknown algorightm = '+alg)
    return fnext

#%% One-dimensional schemes
grid1d = fdo.Grid(num_cells=Nx, delta=dx, start=-Lx/2, periodic=True)
geom1d = fdo.GeometryCartesian((grid1d,), nls=(5,), nus=(5,)) # ample wiggle room
CIR1 = AS.interpolating_scheme(geom1d, 1, [(0,), (-1,)])
LW1 = AS.interpolating_scheme(geom1d, 2, [(1,), (0,), (-1,)])
QUICKEST = AS.interpolating_scheme(geom1d, 3, [(1,), (0,), (-1,), (-2,)])

####################################################################################################
#%% Some popular 2D stencils

#%% First order schemes
CIR = AS.interpolating_scheme(geom, 1, [(0,0), (-1,0), (0,-1)]) # stable for sum(ci)<1
CIRr = AS.interpolating_scheme(geom, 1, [(0,0), (0, -1), (-1, -1)]) # stable to the left or right
CIRl = AS.interpolating_scheme(geom, 1, [(0,0), (-1, 0), (-1, -1)])
CIRs = (CIR + CIRr - CIRl) # weird stability region, useless!

#%% Second order schemes
LWright = AS.interpolating_scheme(geom, 2, [(0,0), (-1,0), (0,-1), (1,0), (0,1), (1, -1)])
LWleft = AS.interpolating_scheme(geom, 2, [(0,0), (-1,0), (0,-1), (1,0), (0,1), (-1, 1)])
LWtail = AS.interpolating_scheme(geom, 2, [(0,0), (-1,0), (0,-1), (1,0), (0,1), (-1, -1)]) # very stable
WBtail = AS.interpolating_scheme(geom, 2, [(0,0), (-1,0), (0,-1), (-1,-1), (-1,-2), (-2,-1)]) # Flipped/shifted
# -- also warming_beam_scheme(geom, 1)
Fromm_old = (LWtail+WBtail)/2 # also fromm_scheme(geom, 1)
Fromm2xy = (AS.transformed_scheme(LWtail, scale=2, shift=1) +
            AS.transformed_scheme(WBtail, scale=2, shift=1))/2
MacCormack_auto = (LWleft+LWright)/2 # Not to confuse with FD "MacCormack"
LWnose = AS.interpolating_scheme(geom, 2, [(0,0), (-1,0), (0,-1), (1,0), (0,1), (1, 1)]) # flipped LWtail
LWfly = AS.interpolating_scheme(geom, 2,  [(0,0), (-1,0), (0,-1), (-1, -1), (1,-1), (-1, 1)]) # Very stable! 0<cx+cy
FrommVar = (LWfly + AS.transformed_scheme(LWfly, scale=-1, shift=-1))/2
# -- this turns out to be Fromm, flipped accordingly, which is checked by direct calculation:
# LWfly2 = AS.transformed_scheme(LWfly, scale=-1, shift=-1)
# tmp = (AS.transformed_scheme(LWfly, scale=(-1,1), shift=(-1,0)) +
# AS.transformed_scheme(LWfly2, scale=(-1,1), shift=(-1,0)))/2
# tmp - Fromm
# (must get zero)
# -- unfortunately, the higher-order analogs of this are unstable.
LWindexr = AS.interpolating_scheme(geom, 2,  [(0,0), (-1,0), (0,-1), (-1, -1), (1, 0), (-1, 1)])
LWindexl = AS.interpolating_scheme(geom, 2,  [(0,0), (-1,0), (0,-1), (-1, -1), (0, 1), (1, -1)])
LWindexsym = (LWindexr+LWindexl)/2
LWsym = (LWright+LWleft+LWtail+LWnose)/4
# -- stable in any direction, but with small radius abs(c)<0.5
# To see it, run
# demo_stability(LWsym, 501, 501, ke=6)
# th = np.linspace(0,1)*2*np.pi
# r = 0.5; plt.plot(r*np.cos(th), r*np.sin(th), color='white')

# The "combination_scheme" class which keeps the information about the constituting
# interpolating schemes
Fromm = AS.combination_scheme(geom, 2, [LWtail.stencil, WBtail.stencil], [.5, .5])

#%% Just playin'
if False:
    LWtaile1 = AS.interpolating_scheme(geom, 2, [(0,0), (-1,0), (0,-1), (-1, -1), (1, 0), (0, 1.25)])
    LWtaile2 = AS.interpolating_scheme(geom, 2, [(0,0), (-2,0), (0,-2), (-2, -2), (2, 0), (0, 2.5)])
    LWtaile3 = AS.interpolating_scheme(geom, 2, [(0,0), (-4,0), (0,-4), (-4, -4), (4, 0), (0, 5)])

#%% UTOPIA (3rd-order) schemes
UT0 = AS.interpolating_scheme(geom, 3, [(0,0), (-1,0), (0,-1), (1,0), (0,1), (-1, 1), (1, -1), (-2,0), (0,-2), (-1,-1)])
UTr = AS.interpolating_scheme(geom, 3, [(0,0), (-1,0), (0,-1), (1,0), (0,1), (-1, 1), (1, -1), (-2,0), (0,-2), (-1,-2)])
UTl = AS.interpolating_scheme(geom, 3, [(0,0), (-1,0), (0,-1), (1,0), (0,1), (-1, 1), (1, -1), (-2,0), (0,-2), (-2,-1)])
# T-shaped stencils give singular matrix:
#UTTr = auto_scheme(geom, 3, [(0,0), (-1,0), (0,-1), (1,0), (0,1), (-1, 1), (1, -1), (-1,-1), (0,-2), (-1,-2)])
#UTTl = auto_scheme(geom, 3, [(0,0), (-1,0), (0,-1), (1,0), (0,1), (-1, 1), (1, -1), (-1,-1), (-2, 0), (-2,-1)])
# "Tail" stencil -- has a smaller stability region
UTt = AS.interpolating_scheme(geom, 3, [(0,0), (-1,0), (0,-1), (1,0), (0,1), (-1, 1), (1, -1), (-2,0), (0,-2), (-2,-2)])
# UTcl -- useless (singular matrix)
#UTcl = auto_scheme(geom, 3, [(1,0), (0,0), (-1,0), (-2,0), (1,1), (0,-1), (-1, -1), (-2,-1), (0,1), (0,-2)])
UTx = AS.interpolating_scheme(geom, 3, [(1,0), (0,1), (0,0), (-1,0), (0,-1), (-2,0), (0,-2), (-1,-1), (-2,-1), (-1,-2)])
UTxll = AS.interpolating_scheme(geom, 3, [(1,0), (0,1), (0,0), (-1,0), (0,-1), (-2,0), (0,-2), (-1,-1), (-2,-1), (-1,1)])
UTxrr = AS.interpolating_scheme(geom, 3, [(1,0), (0,1), (0,0), (-1,0), (0,-1), (-2,0), (0,-2), (-1,-1), (1,-1), (-1,-2)])
# The next two give singular matrices
#UTbugr = auto_scheme(geom, 3, [(1,0), (0,1), (0,0), (-1,0), (0,-1), (-2,0), (0,-2), (-1,-1), (-2,-1), (1,-1)])
#UTbugl = auto_scheme(geom, 3, [(1,0), (0,1), (0,0), (-1,0), (0,-1), (-2,0), (0,-2), (-1,-1), (-1,1), (-1,-2)])

# Ingenious idea. This scheme is actually pretty stable
UTxsym_old = (UTxll + UTxrr)/2
UTxsym = AS.combination_scheme(geom, 3, [UTxll.stencil, UTxrr.stencil], [0.5, 0.5])

#%% Tryangle schemes: Unfortunately, they are unstable!!! (except CIR, LWfly, FLY4)
def fully_upwind_stencil(ndim, order):
    assert ndim==2
    stencil = []
    for k in range(order+1):
        for l in range(order-k+1):
            stencil.append((-l,-k))
    return stencil

def even_fly_stencil(ndim, n):
    return AS.transform_stencil(fully_upwind_stencil(ndim, 2*n), scale=-1, shift=-n)
   

def fully_upwind_scheme(geom, order):
    return AS.interpolating_scheme(geom, order, fully_upwind_stencil(geom.ndim, order))

def even_fly_scheme(geom, n):
    return AS.transformed_scheme(fully_upwind_scheme(geom, 2*n), scale=-1, shift=-n)

UP3 = fully_upwind_scheme(geom, 3)
UP5 = fully_upwind_scheme(geom, 5)
FLY2 = even_fly_scheme(geom, 1) # Same as LWfly
FLY4 = even_fly_scheme(geom, 2) # Stable only for cx+cy>1
#FLY6 = even_fly_scheme(geom, 3) # Unstable


#%% Automatic schemes
def odd_order_auto_scheme(geom, n):
    return AS.interpolating_scheme(geom, 2*n-1, AS.odd_order_stencil(geom.ndim, n))
def even_order_auto_scheme(geom, n):
    return AS.interpolating_scheme(geom, 2*n, AS.even_order_stencil(geom.ndim, n))
def warming_beam_scheme(geom, n):
    return AS.transformed_scheme(even_order_auto_scheme(geom, n), scale=-1, shift=-1)
def fromm_scheme(geom, n):
    "Scheme of order 2*n, very stable (0<ci<1)"
    return (even_order_auto_scheme(geom, n) + warming_beam_scheme(geom, n))/2
def odd_right_scheme(geom, n):
    return AS.interpolating_scheme(geom, 2*n-1, AS.odd_right_stencil(geom.ndim, n))
def xsym_scheme(geom, n):
    """Analog of Fromm scheme of order 2*n-1, very stable (0<ci<1), except for n==1"""
    rr = odd_right_scheme(geom, n)
    return (rr + AS.transformed_scheme(rr, permute=(1,0)))/2

def most_stable_scheme(geom, order):
    """Scheme stable for 0<ci<1 of arbitrary order (except for order==1)"""
    assert geom.ndim==2
    if order%2==0:
        n = order//2
        return fromm_scheme(geom, n)
    else:
        n = (order+1)//2
        if n>1:
            return xsym_scheme(geom, n)
        else:
            # There is no such scheme in 1st order, but return the second best, namely CIR
            return odd_order_auto_scheme(geom, 1)

ORD1 = odd_order_auto_scheme(geom, 1) # Same as CIR
ORD3 = odd_order_auto_scheme(geom, 2) # Same as UT0
ORD5 = odd_order_auto_scheme(geom, 3)
ORD7 = odd_order_auto_scheme(geom, 4)
ORD9 = odd_order_auto_scheme(geom, 5)

ORD2 = even_order_auto_scheme(geom,1) # Same as LWtail
ORD4 = even_order_auto_scheme(geom,2)
ORD6 = even_order_auto_scheme(geom,3)
ORD8 = even_order_auto_scheme(geom,4)

WB2 = warming_beam_scheme(geom, 1) # Same as WBtail
WB4 = warming_beam_scheme(geom, 2)

Fromm4 = fromm_scheme(geom, 2) # Stability region 0<ci<1, pretty good

UTflip_old = (AS.transformed_scheme(UTxll, scale=-1, shift=-1) + 
          AS.transformed_scheme(UTxrr, scale=-1, shift=-1))/2
UTflip = AS.transformed_scheme(UTxsym, scale=-1, shift=-1)

#%% Enlarged 3-rd order schemes
UT02x = AS.transformed_scheme(UT0, scale=2, shift=(1,0))
UT02xy = AS.transformed_scheme(UT0, scale=2, shift=1)

LWtail2xy = AS.transformed_scheme(LWtail, scale=2, shift=1)

UTx2xy = AS.transformed_scheme(UTx, scale=2, shift=1)
UTxll2xy = AS.transformed_scheme(UTxll, scale=2, shift=1)
UTxrr2xy = AS.transformed_scheme(UTxrr, scale=2, shift=1)
UTxsym2xy = (UTxll2xy+UTxrr2xy)/2

UTx2x = AS.transformed_scheme(UTx, scale=2, shift=(1,0))
UTxll2x = AS.transformed_scheme(UTxll, scale=2, shift=(1,0))
UTxrr2x = AS.transformed_scheme(UTxrr, scale=2, shift=(1,0))
UTxsym2x = (UTxll2x+UTxrr2x)/2

#%%
SCHEMES = {'CIR':CIR, 'LWright':LWright, 'LWleft':LWleft, 'LWtail':LWtail,
           'MacCormack_auto':MacCormack_auto, # Not to confuse with FD
           'LWtail2xy':LWtail2xy, 'LWsym':LWsym, 'Fromm':Fromm, 'Fromm2xy':Fromm2xy,
           'LWfly':LWfly,'FrommVar':FrommVar,
           'UT0':UT0, 'UTr':UTr, 'UTl':UTl, 'UTt':UTt, 'UTx':UTx,'UTxll':UTxll,'UTxrr':UTxrr,
           'UTxsym':UTxsym, 'UTflip':UTflip,
           'ORD1':ORD1, 'ORD3':ORD3, 'ORD5':ORD5, 'ORD7':ORD7, 'ORD9':ORD9,
           'WB4':WB4, 'Fromm4':Fromm4,
           'UT02x':UT02x, 'UT02xy':UT02xy,
           'UTx2xy':UTx2xy, 'UTxll2xy':UTxll2xy, 'UTxrr2xy':UTxrr2xy, 'UTxsym2xy':UTxsym2xy,
           'UTx2x':UTx2x, 'UTxll2x':UTxll2x, 'UTxrr2x':UTxrr2x, 'UTxsym2x':UTxsym2x}

for order in range(12):
    SCHEMES['MS'+str(order)]=most_stable_scheme(geom, order)

#%% Test for methods
def untangle_schemes(alg, scheme_list, c=None):
    ftest = ExtendedArray(geom.ncells, stags=(0,0), nls=(3,3), nus=(3,3))
    ftest.arr = np.random.randn(*ftest.nts)
    if c is None:
        c = np.random.rand(2)/2 # must satisfy CFL
    else:
        c = np.array(c)
    fnext = next_value(ftest, c, alg)
    fauto = [scheme.interpolate(ftest, -c) for scheme in scheme_list]
    M = np.array([[np.sum(f1[:,:]*f2[:,:]) for f2 in fauto] for f1 in fauto])
    B = np.array([np.sum(fnext[:,:]*f[:,:]) for f in fauto])
    A = np.linalg.solve(M, B)
    ferror = fnext - sum([A_*f for A_,f in zip(A, fauto)])
    err = np.max(np.abs(ferror))
    return A, err

#%% UTOPIA scheme may be represented as a linear combination of the standard schemes UTx, UTxll, UTxrr
if False:
    #print(find_auto_scheme_coefs('McCormack', [LWleft, LWright, LWtail]))
    cx = np.arange(0.01,1,0.01)
    cy = cx
    #schemes = [UT0, UTsym, UTasym] #, CIR, McCormack, LWtail]
    schemes = [UTx, UTxrr, UTxll]
    A = np.full((len(cx),len(cy),len(schemes)),fill_value=np.nan)
    for kcx, cx_ in enumerate(cx):
        print(cx_)
        for kcy, cy_ in enumerate(cy):
            A_, err = untangle_schemes('UTOPIA', schemes , c=[cx_, cy_])
            A[kcx,kcy,:] = A_
            #print('A=', A_, ', err=', err)
    # There is a linear velocity dependence
    cxm, cym = np.meshgrid(cx, cy, indexing='ij')
    def lin_func(c, a, bx, by):
        cx, cy = c
        return a + bx*cx + by*cy
    coef = np.full((3,3),fill_value=np.nan)
    for k in range(3):
        coef[k,:], _ = curve_fit(lin_func, (cxm.flatten(), cym.flatten()), A[:,:,k].flatten())
    # The result shows that UTOPIA is a combination:
    # UTOPIAx = (UTx*(-1 + cx + cy) + UTxrr*(2 - cx) + UTxll*(2 - cy))/3 

#%% Previous versions -- only cx dependence
if False:
    #print(find_auto_scheme_coefs('McCormack', [LWleft, LWright, LWtail]))
    cx = np.arange(0.01,1,0.01)
    #schemes = [UT0, UTsym, UTasym] #, CIR, McCormack, LWtail]
    schemes = [UTx, UTxll, UTxrr]
    A = np.full((len(cx),len(schemes)),fill_value=np.nan)
    for kcx, cx_ in enumerate(cx):
        A_, err = untangle_schemes('UTOPIA', schemes , c=[cx_, cx_])
        A[kcx,kcy,:] = A_
        print('A=', A_, ', err=', err)


#%% The initial value and velocity
fini = ExtendedArray(geom.ncells, stags=(0, 0), nls=(0, 0), nus=(0, 0))
fini.arr = np.zeros(fini.nts)
mask = (np.abs(x)<Lx/4) & (np.abs(y)<Ly/6)
fini[mask] = 1
def smear(f,n):
    for k in range(n):
        f.setv = n_mean(c_mean(n_mean(c_mean(f,0),0),1),1)
    return f
fini = smear(fini, 3)
# Seed some instabilities
noise = 0
fini += noise*np.random.randn(*fini.nts)

# c = V, so this may be used to determine the stability region, which is sum(V)<1 for odd-order methods
# or all(abs(V)<1) for UTOPIA
V = np.array([.7, .9])

assert all([gr.uniform for gr in geom.grids]) # Nonunform will be in version 2
#dr = np.array([dx, dy])
#dt = .5/np.sum(np.abs(V)/dr)
#c = V*dt
dt = 1
c = V

# UTOPIA untangled as a combination of "standard" schemes (unfortunately, c-dependent):
UTOPIAx = (UTx*(-1 + c[0] + c[1]) + UTxrr*(2 - c[0]) + UTxll*(2 - c[1]))/3
# Or, it may be represented as the 4th order scheme
UTOPIA4 = -UTx.raise_order()/3 - UTx.times(0)/3 - UTx.times(1)/3 +\
    UTxrr.raise_order()*2/3 + UTxrr.times(0)/3 +\
        UTxll.raise_order()*2/3 + UTxll.times(1)/3

UTOPIAx2xy = (UTx2xy*(-1 + c[0] + c[1]) + UTxrr2xy*(2 - c[0]) + UTxll2xy*(2 - c[1]))/3
# But, any linear combination with total sum of coefs 1 is a valid 3-rd order scheme (approximately)!

# UTxll and UTxrr are stable in a slightly bigger region of c than other UT*
# However, UTOPIA (==UTOPIAx) is the most stable!

# It may be represented as other combinations, e.g, UT0 + UTl + UTr, but c-dependence is too complicated.
# The face values are also simple
SCHEMES.update(UTOPIAx=UTOPIAx,  UTOPIAx2xy=UTOPIAx2xy, UTOPIA4=UTOPIA4)
#SCHEMES['UTOPIAx'] = UTOPIAx
#SCHEMES['UTOPIAx2'] = UTOPIAx2

methods = ['UTxsym','Fromm4','ORD5'] # 3,4,5 order
methods = ['UTxsym', 'Fromm', 'FrommVar'] # 3, almost-3, almost-3
#methods = ['MS2', 'MS3', 'MS4', 'MS5', 'MS6'] # most stable schemes
#methods = ['MS7', 'MS8', 'MS9', 'MS10', 'MS11']
methods = ['MS3', 'MS5', 'MS7', 'MS9']
#methods = ['UTxsym', 'UTflip']
#methods = ['LWtail2xy','LWsym', 'Fromm2xy'] # stable in any direction
#methods = ['LWtail2xy','UTxsym2xy','UT02xy']
#methods = ['CIR','UTOPIA','MacCormack','UTOPIA_Ralston', 'SSPRK3']
#methods = ['ORD3', 'ORD5', 'ORD7', 'ORD9'] # odd-order schmes
#methods = ['UTOPIA', 'UTOPIAx', 'UTOPIA4'] # compare the UTOPIAs
nmethods = len(methods)

#%% Initial values
farr=[]
for kmethod, alg in enumerate(methods):
    if alg in SCHEMES.keys():
        d = AS.get_boundary_thickness(SCHEMES[alg]) # SCHEMES[method].order
    else:
        d = 4
    f = Field('f_'+alg, geom.ncells, stags=(0,0), nls=(d,d), nus=(d,d))
    # The boundary conditions may set f0.bc[0]=f0.bc[end], too -- 
    # it is independent of direciton
    #f0.bc[-d:end+d, end:end+d] = f0.bc[-d:end+d, 0:d]
    #f0.bc[-d:end+d, -d:-1]     = f0.bc[-d:end+d, end-d:end-1]
    f.bc[span,    end:end+d]  = f.bc[span,            0:d]
    f.bc[span,        -d:-1]  = f.bc[span,    end-d:end-1]
    f.bc[end:end+d, 0:end-1]  = f.bc[0:d,         0:end-1]
    f.bc[-d:-1,     0:end-1]  = f.bc[end-d:end-1, 0:end-1]
    f.bc.freeze()
    f.setv = 0 # to clear the ghost cells
    f.setv = fini
    f.bc.apply()
    farr.append(f)

plt.close('all')
figs = []
for kmethod in range(nmethods):
    fig = UpdateablePcolor(100+kmethod, zmin=-0.5, zmax=1.5)
    figs.append(fig)

#%% Let's go!
Nt = 10000
t = 0
for kt in range(Nt+1):
    for kmethod, alg in enumerate(methods):
        f = farr[kmethod]
        if alg in SCHEMES.keys():
            f.setv = SCHEMES[alg].interpolate(f, -c)
        else:
            f.setv = next_value(f, c, alg)
        assert not np.isnan(f.arr).any()
        f.bc.apply()
        assert not np.isnan(f.arr).any()
    t += dt
    if kt<100 or (kt<500 and kt % 10==0) or kt % 100==0:
        for kmethod in range(nmethods):
            fig = figs[kmethod]
            f = farr[kmethod]
            alg = methods[kmethod]
            if fig.started_plotting and not plt.fignum_exists(fig.fignum):
                print('Figure closed, exiting ...')
                plt.close('all')
                sys.exit()
            #plt.figure(100+kmethod)
            Dx = np.int(t*V[0]/dx)
            Dy = np.int(t*V[1]/dy)
            the_title = alg + ' in [{:g}, {:g}], D = [{:d}, {:d}]'.format(
                np.min(f), np.max(f), Dx, Dy)
            fig.plot(geom, f, title=the_title)
            # fig_.clear()
            # #plt.clf()
            # ax = fig_.subplots()
            # ax.pcolor(xe, ye, farr[kmethod][:,:].T)
            # ax.set_aspect('equal')
            # ax.set_title(methods[kmethod] + ': D = [{:d}, {:d}]'.format(Dx,Dy))
            # fig_.canvas.draw()
        #plt.draw_all()
        plt.pause(.1)
        #fig[0].waitforbuttonpress() # progress by clicking mouse in Figure 100