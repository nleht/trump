#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 15 12:49:44 2020

@author: nle003
"""

import numpy as np
import matplotlib.pyplot as plt
import fidelior as fdo
#from fdo.symbolic_array import SymbolicArray
#from fdo.common import customize
from fidelior import sy, co, ea, extended_array, field, end, half, span, c_diff, n_diff, solver

#%%
fdo.set_global_debug_level(2)
fdo.set_sparse(False)

#%% First, test the FakeArray class
print('\n\n********** TEST SymbolicArray **********\n')
f = sy.SymbolicArray('f',5)
# A trick to make operations like v * f and v + f work with ndarray v and
# arbitrary class f (NOTE: f * v and f + v work fine even without it)
v = co.customize(np.arange(1,6))
mul = v*f
mul_op = mul.action
print('mul_op =',mul_op,'\n')
add = v + f
add_op = add.action
print('add_op =',add_op,'\n')
sub1 = v - f
sub1_op = sub1.action
print('sub1_op =',sub1_op,'\n')
sub2 = f - v
sub2_op = sub2.action
print('sub2_op =',sub2_op,'\n')
div = f / v
div_op = div.action
print('div_op =',div_op,'\n')

#%% Boundary conditions
print(co.blue('\n*** Boundary conditions ***\n'))
geom = fdo.Geometry((fdo.Grid(num_cells=5,delta=0.2,start=0),))
f = Field('f', geom.ncells, stags=(0,), nls=(0,), nus=(0,))
f.bc.record('end')
f.bc[end]=1
f.bc.record('zero',is_variable=True)
f.bc[0]=1
f.bc.freeze()
f[:] = np.random.rand(*f.nts)
f.bc.apply()
print(f.arr)
f.bc.update('zero')
f.bc[0]=10
f.bc.apply()
print(f.arr)
f.bc.print()

#%% Shortened geometry
#geomshort = ea.geom_extend(geom,-1,-1)
area = ExtendedArray(geom, stags=(0,), nls=(1,), nus=(1,))
curv = 7
area.arr = np.exp(curv*area.xe())
areac = ExtendedArray(geom, stags=(1,), nls=(1,), nus=(1,))
areac.arr = np.exp(curv*areac.xe())

#%% Fake field
print(co.blue('\n*** Fake field ***\n'))
phi = Field('phi', geom, stags=(0,), nls=(1,), nus=(1,))
phi.bc[-1]=0
phi.bc.record('end',is_variable=True)
phi.bc[end+1]=0
phi.bc.freeze()

lapl = -n_diff(areac*c_diff(phi.symb))/geom.grids[0].delta**2/area
gop = lapl.arr/phi.symb.arr
op1 = gop.total
op = gop.simplify
print(op)

#%% Fake field 2
print(co.blue('\n*** Fake field 2 ***\n'))
area = Field('area', geom, stags=(0,), nls=(0,), nus=(0,))
curv = 7
area.arr=np.exp(curv*area.xe())
areac = Field('areac', geom, stags=(1,), nls=(0,), nus=(0,))
areac.arr = np.exp(curv*areac.xe())

rho = Field('rho', geom, stags=(0,), nls=(-1,), nus=(-1,))
rho.arr=np.array([1,2,3,4])
phi = Field('phi', geom, stags=(0,), nls=(0,), nus=(0,))
phi.bc[end] = 0
phi.bc.record('start',is_variable=True)
phi.bc[0] = 0
phi.bc.freeze()

lapl = -n_diff(areac*c_diff(phi.symb))/geom.grids[0].delta**2/area
phi_solver = solver(phi,lapl)
#phi[:] = phi_solver.solve_full(rho)
phi_solver.solve_full(phi,rho)
#gop = lapl.arr/phi.symb.arr
#op1 = sy.gen_op_total(gop)
#op = sy.gen_op_simplify(gop)
#print(op)
print('phi(rho) =', phi)
plt.figure(1)
plt.clf()
plt.plot(phi.xe(),phi[span],'.-')

#%
phi.bc.update('start')
phi.bc[0] = 1
#print('phi(rho) =', sy.gen_op_solve(lapl.arr/phi.symb.arr,rho.arr))
#gop = lapl.arr/phi.symb.arr
#op1 = sy.gen_op_total(gop)
#op = sy.gen_op_simplify(gop)
#print(op)
phi_solver.solve_full(phi,rho)
print('phi(rho) =', phi)
plt.figure(2)
plt.clf()
plt.plot(phi.xe(),phi[span],'.-')
phi.bc.print()


#%% 2D field
print(co.blue('\n*** 2D field ***\n'))
fdo.set_sparse(False)
np.set_printoptions(threshold=1000)
gridx = fdo.Grid(num_cells=10,delta=.1,start=0.0)
gridy = fdo.Grid(num_cells=10,delta=.1,start=0.0)
geoms = fdo.Geometry((gridx, gridy), nls=(3,3), nus=(3,3))
rho = Field('rho', geoms.ncells, stags=(0,0), nls=(0,0), nus=(0,0))
rho[:,:]=1 #np.ones(rho.nts)
phi = Field('phi', geoms.ncells, stags=(0,0), nls=(1,1), nus=(1,1))
#phi.symb[end+1,-1:end+1]=0 # gives an error
#phi.arr[:,:]=np.random.rand(*phi.nts)
phi.bc[end+1,-1:end+1]=0
phi.bc[:,-1]=0
phi.bc[:,end+1]=0
phi.bc.record('left',is_variable=True)
phi.bc[-1,-1:end+1]=0
phi.bc.freeze()
def Laplacian2D(geom,f):
    g = geom.grids
    return -n_diff(c_diff(f,0),0)/g[0].delta**2 - n_diff(c_diff(f,1),1)/g[1].delta
lapl = Laplacian2D(geoms, phi.symb)
phi_solver = solver(phi,lapl)
phi_solver.solve_full(phi,rho) # bc are included
plt.figure(3)
plt.clf()
x_pcolor = gridx.r_c[-half:end+half].flatten()
y_pcolor = gridy.r_c[-half:end+half].flatten()
plt.pcolor(x_pcolor, y_pcolor, phi[:,:].T)

#%%
phi.bc.update('left')
phi.bc[-1,-1:end+1]=1
phi_solver.solve_full(phi,rho)
plt.figure(4)
plt.clf()
plt.pcolor(x_pcolor, y_pcolor, phi[:,:].T)
plt.colorbar()
phi.bc.print()
#plt.pcolor(phi.x(0),phi.x(1),phi.val.T)
