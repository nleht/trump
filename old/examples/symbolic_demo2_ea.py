#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  2 13:11:11 2020

Symbpolic Extended Arrays
-------------------------

@author: nleht
"""

import numpy as np
import fidelior_old as fdo
from fidelior_old import co, sy, c_diff, c_mean, n_diff, ExtendedArray, end, half, span
fdo.set_sparse(False)
fdo.set_global_debug_level(4)

#%% Grid and elementary (uniformly spaced) geometry
#grid = fdo.Grid(num_cells=5,delta=0.2,start=0)
#geom = fdo.geometry_flat((grid,))
ncells = (5,)
delta = 0.2

#%% A numeric array
print(co.blue('\n*** A numeric array ***'))
c = ExtendedArray(ncells, stags=(0,), nls=(0,), nus=(0,))
c.arr = np.random.randn(*c.nts)
print('c =',c)

#%% A symbolic array
print(co.blue('\n*** A symbolic array ***'))
f = c.copy()
f.is_flat = True
f.arr = sy.SymbolicArray('f',f.size)
# Add a constraint
f[0] = 1 # Dirichlet bc
f[end] = f[end-1] # Neumann bc
cnst = f.arr.ref.cnst
print('Constraints on f.arr are represented by operator M*x+V=0:\n',cnst.op)
print('\nThe array itself is represented by unit matrix operator\n',f.arr.op)
print('\nElements and slices are also operators, i.e. f[1:2] is represented by:\n',f[1:2].op)

#%% A simple laplacian
print(co.blue('\n*** A simple Laplacian ***'))
def Laplacian0(y):
    "Returns a non-extended array"
    return (y[0:end-2]-2*y[1:end-1]+y[2:end])/delta**2
def Laplacian1(y):
    "A slightly more convenient way to write the same, because it returns an extended array"
    return (y.shifted((-1,))-2*y+y.shifted((1,)))/delta**2
def Laplacian2(y):
    "The most convenient way to write the same: take double derivative"
    return n_diff(c_diff(y))/delta**2
op0 = Laplacian0(f).op
op1 = Laplacian1(f).arr.op
op2 = Laplacian2(f).arr.op
# It may be also calculated as
lapl_gen_op = Laplacian1(f).arr.action # Generalized linear operator, with constraints
op = lapl_gen_op.equation # only the "active" part of the generalized operator (no constraints)
print('\nLaplacian is represented as an operator:\n',op)
assert op0 == op
assert op1 == op
assert op2 == op
#print('Difference in laplacians =',np.max(np.abs(op.M-op2.M)))

#%% Demonstrate that operators may be applied symbolically
print(co.blue('\n*** Demonstrate that operators may be applied symbolically ***'))
r1 = ExtendedArray(ncells, stags=(0,), nls=(-1,), nus=(-1,))
(r1.arr, err) = lapl_gen_op(c.arr)
print('\nLaplacian(c) without enforcing constraints =',r1,', error =',err)
r2 = Laplacian1(c)
print('Laplacian(c) calculated directly =',r2)

#%% Effect of constraints
print(co.blue('\n*** Effect of constraints ***'))
print('\nBefore enforcing constraints: c =',c)
cnst.enforce(c.arr) # enforce them in-place
print('After enforcing contraints (check the bc): c =',c)

#%% How the operator works after the constraints
print(co.blue('\n*** How the operator works after the constraints ***'))
print('\nLaplacian(c) after enforcing constraints:',Laplacian1(c))
(r1.arr, err) = lapl_gen_op(c.arr)
print('Applying the symbolic operator: Laplacian(c) =',r1,', error =',err)

#%% Operations with 'regular' extended arrays
# The binary operations can go both ways now, because extended arrays are 'customized' by default.
# Compare to what we had in 'symbolic_demo1.py, part 'Binary operations with non-symbolic arrays'!
print(co.blue('\n*** Binary operations ***'))
print('\n(c_dif(c)*c_dif(f)).arr.op =\n',(c_diff(c)*c_diff(f)).arr.op)
print('\n(c[1]*f).arr.op =\n',(c[1]*f).arr.op)

#%% Switch from setting constraints to setting elements
print(co.blue('\n*** Set elements instead of constraints ***'))
f = c.copy()
f.is_flat = True # Symbolic arrays are flat
f.arr = sy.SymbolicArray('f', c.size)
#g = ExtendedArray(geom, stags=(1,), nls=(1,), nus=(1,))
#g.alloc_symb(symbolic_ea=f) # instead of g.alloc(np.double)
def custom_operator(f):
    g = f.like(stags=(1,), nls=(1,), nus=(1,)) # copy, with different stags, nls, nus
    tmp = c_diff(f)
    g[half] = tmp[half]
    g[-half] = 1
    g[1+half:end+half] = tmp[:]
    return g

g = custom_operator(f)
assert g.arr.op.complete # there are no unset values
gnum = custom_operator(c)
print('\nc =\n',c)
print('\ncustom_operator(c) =\n', gnum, '=\n', g.arr.op(c.arr))