#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  2 13:11:11 2020

Symbpolic arrays demo.

See more tests in the end of file 'symbolic_array.py'

@author: nleht
"""

import numpy as np
import fidelior_old as fdo
from fidelior_old import co, sy

#%% For demo, we represent matrices as dense. But usually, we must have SET_SPARSE(True)!
fdo.set_global_debug_level(3)
fdo.set_sparse(False)

#%% Elementary operations
print(co.blue('\n\n********** Elementary operations **********'))
f = sy.SymbolicArray('f',5)
# All linear operations on f are represented as linear operators are M*x+V.
# The original vector is, of course, the unit operator
print('\nOriginal vector is represented by unit operator\n f.op =\n', f.op)
# The indexing is also a linear operator
print('\nOperator for f[1] =\n', f[1].op)
# More complicated indexing involving slices, and operations for addition
ave = (f[1:] + f[:-1])/2
dif = f[1:] - f[:-1]
print('\nAveraging operator =\n ', ave.op)
print('\nDifference operator =\n ', dif.op)
# One may concatenate symbolic arrays:
with sy.SY_OVERRIDE: # In this context, we override the numpy function np.hstack
    large = np.hstack((ave, dif)) # New capability in v0.4
print('\nConcatenated ave + dif =\n', large.op)

#%% Binary operations with non-symbolic arrays
print(co.blue('\n\n********** Binary operations with non-symbolic arrays **********'))
# A trick to make operations like v * f and v + f work with ndarray v and
# arbitrary class f (NOTE: f * v and f + v work fine even without it)
v1 = np.arange(1.,6.)
v = co.customize(v1)
mul1 = f*v1 # f*v will work as well
print('v =',v1,'=',v)
print('\nOperator for f*v =\n ', mul1.op)
mul2 = v*f # unfortunately, v1*f does not work because the ndarray v1 takes over control of __mul__
print('\nOperator for v*f =\n ', mul2.op)
print('\nOperator for f+v =\n ', (f+v).op)
print('\nOperator for v+f =\n ', (v+f).op)
print('\nOperator for f-v =\n ', (f-v).op)
print('\nOperator for v-f =\n ', (v-f).op)
print('\nOperator for f/v =\n ', (f/v).op)
# We cannot do v/f because it is nonlinear operation
print('\nOperator for f*v[1] =', (f*v[1]).op)
# The following shows that it is tricky with operations v[k] @ f even if it looks like a scalar
print('\nOperator for 2.*f =\n',(2.*f).op)
print('\nOperator for v[1]*f =',(v[1].item()*f).op)
print('\nOperator for v[1]*f =',(co.customize(v[1])*f).op)

#%% Constraints
print(co.blue('\n\n********** Constraints and simple equations **********'))
f = sy.SymbolicArray('f',5) # use a new copy
def get_dif(a):
    "Difference"
    return a[1:]-a[:-1]
def get_lap(a):
    "Laplacian"
    return get_dif(get_dif(a))
f[0]=1
f[-1]=-1
constraints = f.ref.cnst
print('\nConstraints:\n Mask for dependent variables: ',constraints.dep)
print(' Equations M*f + V = 0 must be satisfied, where M, V are given by constraints.op =\n ',
      constraints.op)
lap = get_lap(f)
print('Constraints are stored inside the linear operator, too:\n lap_op =\n',lap.action)
# Now, we can solve equation get_lap(phi)=rho
rho = np.array([1,2,3])
phi = lap.action.solve(rho)
print('phi =',phi)
# introduce intentional error
phi[0]=phi[0]+0.1
# Apply the constrained operator to phi
(rho,err) = lap.action(phi)
print('Restored rho =',rho,', Error in the constraints =',err)
# Another way to do it
op = lap.action.simplify
print('Simplified operator which is applied only to independent elements of phi\n op =\n',op)
rho = op(phi[~lap.action.is_dependent])
print('Applied to indep components only: rho =',rho)

#%% Creating operators as a combination of previously available ones
print(co.blue('\n\n*** Creating new operators ***'))
f = sy.SymbolicArray('f',5)
ave = (f[1:] + f[:-1])/2
dif = f[1:] - f[:-1]
# The following operation should be used carefully!
g = f.allocate((len(ave)+len(dif),))
g[::2] = ave
print('g is complete:',g.op.complete)
g[1::2] = dif
assert g.op.complete
print(co.info('\n[::2] is ave, [1::2] is dif:\n'), g)
# Another way to create new operators
h = ave.cat(dif)
print(co.info('\nconcatenation(ave, dif) =\n'), h)
