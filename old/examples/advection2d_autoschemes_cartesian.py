#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 25 13:35:17 2020

Test of various advection schemes in 2D non-uniform grids, with constant velocity

Periodic boundary conditions, automatic schemes with given stencil

@author: Nikolai G. Lehtinen
"""

#%% Mandatory imports
import sys
import os
import numpy as np
import matplotlib.pyplot as plt
import fidelior_old as fdo
from fidelior_old import end, half, span
from fidelior_old.plotting import UpdateablePcolor, ea_plot
from fidelior_old import autoschemes as AS
    
from fidelior_old import extended_arrays as EA
from fidelior_old import common as CO

fdo.set_global_debug_level(0)
if not fdo.numpy_override.entered:
    fdo.numpy_override.enter()

def makedir(dirname):
    "Create directory *dirname* with subdirectories, warn if create, do nothing if exists"
    if not os.path.isdir(dirname):
        print(fdo.co.red('Warning:'),'Creating directory',dirname)
        os.makedirs(dirname)
        
class neat_float(float):
    "Short-print of floats"
    def __str__(self):
        return "%0.5g" % self.real
    def __repr__(self):
        return self.__str__()

#%% Settings
deterministic = False
do_centered = True
#AS.FEM_ALGORITHM = 'simple integration' # alternatives: 'integration', 'face value'
AS.FEM_ALGORITHM = 'face value' # recommended
#AS.VOLUME_AVERAGING = True # True is recommended (?)
savefig = None # ['png'] # None
fignumshift = 1+(int(do_centered)*2 + int(AS.FEM_ALGORITHM_NUM[AS.FEM_ALGORITHM]))*100
if savefig is not None:
    figdir = 'fig_centered/'
speedtype = 'rotation' # Alternatives: 'uniform', 'rotation', 'div-less'
shapetype = 'rectangle' # Alternatives: 'rectangle', 'full'
if shapetype=='full':
    dzlim = .2
if shapetype=='rectangle':
    nsmear = 0 # recommended: 6
if speedtype=='rotation' or speedtype=='div-less':
    bctype='zero'
elif speedtype=='uniform':
    bctype='periodic'
if speedtype=='uniform':
    V = np.array([.3, .7])
alpha = 1
Nx = 50
Ny = 50
Lx = 10
Ly = 10
gridtypes = ['uniform', 'uniform']
#gridtypes = ['random', 'random']
#gridtypes = ['uniform', 'random']

methods = ['CIR','LWtail','UTr','UTl']
methods = ['CIR_fem', 'LW_fem', 'ORD3_fem']
#methods = ['CIR_fem', 'CIR', 'LW_fem', 'LWtail']
methods = ['ORD3_fem', 'ORD5_fem', 'ORD7_fem']
#methods = ['ORD5_fem'] # just one
methods = ['LW_fem','ORD3_fem'] #, 'ORD4_fem', 'ORD5_fem']
methods = ['CIR_fem','ORD3_fem', 'ORD5_fem', 'ORD7_fem']
methods = ['ORD3_fem_fv', 'ORD3_fem_integr']#, 'ORD3_fem_cumint', 'UTr']
#methods = ['ORD3_fem_fv', 'ORD3_fem_integr', 'ORD5_fem_integr', 'ORD3_fem_cumint', 'ORD5_fem_cumint']
#methods = ['ORD3_fem']
ONLY_FEM = True # For calculation of the total integral -- easier for FEM methods


#%% Set up the grid and geometry
def get_dr(L, N, gridtype):
    if gridtype=='uniform':
        dr = L/N
    elif gridtype=='random':
        random_coef = 0.00001
        dr = fdo.random_grid_delta(L, N, random_coef)
    elif gridtype=='growing':
        refine_coef = 0.3
        dr = fdo.growing_grid_delta(L, N, L/N*refine_coef)
    else:
        raise Exception('unknown gridtype')
    return dr
if deterministic:
    np.random.seed(seed=0)
else:
    np.random.seed(seed=None)
dx = get_dr(Lx, Nx, gridtypes[0])
dy = get_dr(Ly, Ny, gridtypes[1])
gridx = fdo.Grid(num_cells=Nx, delta=dx, start=-Lx/2, periodic=(bctype=='periodic'))
gridy = fdo.Grid(num_cells=Ny, delta=dy, start=-Ly/2, periodic=(bctype=='periodic'))
geom = fdo.GeometryCartesian((gridx, gridy), nls=(5,5), nus=(5,5)) # ample wiggle room

#%% Extra geometrical stuff
ndim = geom.ndim

if do_centered:
    x = gridx.r_c
    y = gridy.r_c
    xb = gridx.r_n
    yb = gridy.r_n
else:
    x = gridx.r_n
    y = gridy.r_n
    xb = gridx.r_c
    yb = gridy.r_c

# The velocity template
if do_centered:
    ex = fdo.ExtendedArray(geom.ncells, stags=(0,1), nls=(0,1), nus=(0,1))
    ey = fdo.ExtendedArray(geom.ncells, stags=(1,0), nls=(1,0), nus=(1,0))
else:
    ex = fdo.ExtendedArray(geom.ncells, stags=(1,0), nls=(1,1), nus=(0,0))
    ey = fdo.ExtendedArray(geom.ncells, stags=(0,1), nls=(1,1), nus=(0,0))
ex.arr = np.ones(ex.nts)
ey.arr = np.ones(ey.nts)

if do_centered:
    # Seems a bit simpler, maybe it is more natural to have centered schemes?
    u_indep = fdo.ExtendedArray(geom.ncells, stags=(1,1), nls=(0,0), nus=(0,0))
else:
    if bctype=='periodic':
        u_indep = fdo.ExtendedArray(geom.ncells, stags=(0,0), nls=(0,0), nus=(-1,-1))
    elif bctype=='zero':
        u_indep = fdo.ExtendedArray(geom.ncells, stags=(0,0), nls=(-1,-1), nus=(-1,-1))
#u_indep = fdo.ExtendedArray(geom.ncells, stags=(0,0), nls=(0,0), nus=(0,0))
u_indep.arr = np.ones(u_indep.nts)


#%% Setup the interpolating scheme stencil polynomial
CIR_fem = AS.make_fem_scheme_sp(geom, u_indep, 0, [(0,0)])
stencil = AS.odd_order_stencil(ndim,1) #[(0,0), (1,0), (0,1)]
degree = 1
CIR = AS.make_interpolation_scheme_sp(geom, u_indep, degree, stencil)
LW_fem = AS.make_fem_scheme_sp(geom, u_indep, degree, stencil)

LWstencil = AS.even_order_stencil(ndim,1)
LWtail = AS.make_interpolation_scheme_sp(geom, u_indep, 2, LWstencil)
# The default is 'face value', the same as the next one
ORD3_fem = AS.make_fem_scheme_sp(geom, u_indep, 2, LWstencil) # default
# 'face value' -- seems to be most accurate; results are very similar to the interpolating scheme
ORD3_fem_fv = AS.make_fem_scheme_sp(geom, u_indep, 2, LWstencil, fem_algorithm='face value')
# 'integration' -- the rotating structures spread out, without change of total
ORD3_fem_integr = AS.make_fem_scheme_sp(geom, u_indep, 2, LWstencil, fem_algorithm='integration')
# 'cumint' -- the rotating structures shrink, the total decreases.
ORD3_fem_cumint = AS.make_fem_scheme_sp(geom, u_indep, 2, LWstencil, fem_algorithm='cumint')
ORD3_fem_exp = AS.make_fem_scheme_sp(geom, u_indep, 2, AS.transformed_stencil(LWstencil, shift=(-1,0))) # unstab
ORD3_WB_fem = AS.make_fem_scheme_sp(geom, u_indep, 2, AS.warming_beam_stencil(ndim, 1)) # unstable
#ORD4_fem = AS.make_fem_scheme_sp(geom, u_indep, 3, AS.odd_left_stencil(ndim,2))
ORD4_fem = AS.make_fem_scheme_sp(geom, u_indep, 3, AS.odd_order_stencil(ndim,2))
O4_stencil = AS.even_order_stencil(ndim,2)
ORD5_fem = AS.make_fem_scheme_sp(geom, u_indep, 4, O4_stencil, fem_algorithm='face value')
ORD5_fem_integr = AS.make_fem_scheme_sp(geom, u_indep, 4, O4_stencil, fem_algorithm='integration')
ORD5_fem_cumint = AS.make_fem_scheme_sp(geom, u_indep, 4, O4_stencil, fem_algorithm='cumint')
ORD7_fem = AS.make_fem_scheme_sp(geom, u_indep, 6, AS.even_order_stencil(ndim,3))

#%% Tests of accuracy of interpolation. Introduce a "refined" geometry geomref.
if False:
    assert do_centered
    if False:
        u = fdo.ExtendedArray(geom.ncells, stags=(1,1), nls=(4,4), nus=(4,4))
        u.arr = np.random.rand(*u.nts)
    else:
        # u = np.sin(x)*np.sin(y), but integrate for more accuracy
        ucum = np.cos(xb)*np.cos(yb)
        u = EA._diff(EA._diff(ucum,0),1)/ORD3_fem.apparent_volume

    plt.figure(1000)
    plt.clf()
    ea_plot(geom, u)
    # Plot at the refined points
    Nxref = 50
    Nyref = 50
    gridxref = fdo.Grid(num_cells=Nxref, delta=Lx/Nxref, start=-Lx/2)
    gridyref = fdo.Grid(num_cells=Nyref, delta=Ly/Nyref, start=-Ly/2)
    geomref = fdo.GeometryCartesian((gridxref, gridyref), nls=(5,5), nus=(5,5))
    ucref = fdo.ExtendedArray(geomref.ncells, stags=(1,1), nls=(0,0), nus=(0,0))
    uref = fdo.ExtendedArray(geomref.ncells, stags=(0,0), nls=(0,0), nus=(0,0))
    xcref = gridxref.r_c
    ycref = gridyref.r_c
    xref = gridxref.r_n
    yref = gridyref.r_n
    coorgrid = [xcref[span(ucref,0)].flatten(), ycref[span(ucref,1)].flatten()]
    for ksp, sp in enumerate([CIR_fem, LW_fem, ORD3_fem, ORD5_fem, ORD7_fem]):
        sp._choose_stencil(None)
        ucref.arr = sp.interpolate(u, coorgrid, is_grid=True)
        plt.figure(1001+ksp)
        plt.clf()
        ucheck = np.sin(xcref)*np.sin(ycref)
        ea_plot(geomref, ucref-ucheck)
        #ea_plot(geomref, uref)

    plt.figure(2000)
    plt.clf()
    ucumref = (-np.cos(xref)+np.cos(xb[-1]))*(-np.cos(yref)+np.cos(yb[-1]))
    ea_plot(geomref, ucumref)
    coor = CO.ndgrid(xref[span(uref,0)].flatten(), yref[span(uref,1)].flatten())
    for ksp, sp in enumerate([CIR_fem, LW_fem, ORD3_fem, ORD5_fem, ORD7_fem]):
        sp._choose_stencil(None)
        sp.cumint_prepare(u)
        uref.arr = sp.cumint(coor, is_grid=False)
        plt.figure(2001+ksp)
        plt.clf()
        #ucheck = np.sin(xcref)*np.sin(ycref)
        ea_plot(geomref, uref-ucumref)
        #ea_plot(geomref, uref)

#%% Test
if do_centered:
    u = fdo.ExtendedArray(geom.ncells, stags=(1,1), nls=(2,2), nus=(2,2))
else:
    u = fdo.ExtendedArray(geom.ncells, stags=(0,0), nls=(2,2), nus=(2,2))
#u = u_indep.copy()
u.arr = np.random.rand(*u.nts)

def sp_cumint_test(sp, u):
    #sp = ORD3_fem
    sp._choose_stencil(None)
    sp.cumint_prepare(u)
    s = span(sp.u_indep)
    ndim = sp._geom.ndim
    coor1 = []
    coor2 = []
    for axis in range(ndim):
        r, rb = sp.r_rb(axis)
        coor1.append(rb.lo(axis).broadcast(sp.u_indep).arr)
        coor2.append(rb.hi(axis).broadcast(sp.u_indep).arr)
    tmp = sp.cumint([coor2[0], coor2[1]]) - sp.cumint([coor1[0], coor2[1]]) -\
        sp.cumint([coor2[0], coor1[1]]) + sp.cumint([coor1[0], coor1[1]])
    uint0 = u*sp.apparent_volume
    uint = uint0.copy_arr()
    uint[s]= tmp
    return uint - uint0
print('Cumulative integration error =', np.max(np.abs(sp_cumint_test(ORD3_fem, u))))

#%% More stuff
ORD3_fem_simple = AS.make_fem_scheme_sp(geom, u_indep, 3, AS.odd_right_stencil(ndim,2),
                                   flip_stencil=True,
                                   fem_algorithm='simple integration')
    
#%% UTOPIA (3rd-order) schemes
UTr = AS.make_interpolation_scheme_sp(geom, u_indep, 3, AS.odd_right_stencil(ndim,2))
UTl = AS.make_interpolation_scheme_sp(geom, u_indep, 3, AS.odd_left_stencil(ndim,2))
ORD5 = AS.make_interpolation_scheme_sp(geom, u_indep, 5, AS.odd_left_stencil(ndim,3))

#%% Table with schemes
INTERP_SCHEMES = {'CIR':CIR, 'LWtail':LWtail, 'UTr':UTr, 'UTl':UTl, 'ORD5':ORD5}
FEM_SCHEMES = {'CIR_fem':CIR_fem, 'LW_fem':LW_fem, 'ORD3_fem':ORD3_fem, 'ORD5_fem':ORD5_fem,
               'ORD7_fem':ORD7_fem, 'ORD3_fem_simple':ORD3_fem_simple, 'ORD3_fem_exp':ORD3_fem_exp,
               'ORD4_fem':ORD4_fem, 'ORD3_WB_fem':ORD3_WB_fem, 'ORD3_fem_fv':ORD3_fem_fv,
               'ORD3_fem_integr':ORD3_fem_integr, 'ORD3_fem_cumint':ORD3_fem_cumint,
               'ORD5_fem_integr':ORD5_fem_integr, 'ORD5_fem_cumint':ORD5_fem_cumint}

#%% The sanity check for the new algorithm
alg = 'ORD3_fem'
FEM = FEM_SCHEMES[alg]
print('Sanity check for scheme:', alg)
for p in FEM.stencil:
    Deltar1 = []
    Deltar2 = []
    for a, gr in enumerate(geom.grids):
        shift = list(p)
        shift[a] = p[a]-half
        if do_centered:
            d1 = gr.r_n.shifted(shift)-gr.r_c
        else:
            d1 = gr.r_c.shifted(shift)-gr.r_n
        Deltar1.append(d1)
        shift[a] = p[a]+half
        if do_centered:
            d2 = gr.r_n.shifted(shift)-gr.r_c
        else:
            d2 = gr.r_c.shifted(shift)-gr.r_n
        Deltar2.append(d2)
    # A more thorough sanity chack #2 -- integrate the neighbor cell
    D1 = tuple(d.broadcast(FEM.u_used) for d in Deltar1)
    D2 = tuple(d.broadcast(FEM.u_used) for d in Deltar2)
    u_integrated = FEM.integrate_on_displacement(u, (D1, D2))
    u_averaged = u_integrated/FEM.apparent_volume.shifted(p)
    print('Point =', p, ', method error =', np.max(np.abs(u_averaged-u.shifted(p))))

#%% Generalized advection step
def advection_step(alg, u, r, w):
    if alg in INTERP_SCHEMES.keys():
        return INTERP_SCHEMES[alg].interpolate_on_displacement(u, r)-u
    elif alg in FEM_SCHEMES.keys():
        return FEM_SCHEMES[alg].fem_advection_step(u, w)
    elif alg[:5]=='test_':
        sp = FEM_SCHEMES[alg[5:]]
        return sp._fem_step_cumint(u, w)
    else:
        raise Exception('Unknown alg: '+alg)

#%% The initial value
def smear(f,n):
    for k in range(n):
        if do_centered:
            f.setv = fdo.c_mean(fdo.n_mean(fdo.c_mean(fdo.n_mean(f,0),0),1),1)
        else:
            f.setv = fdo.n_mean(fdo.c_mean(fdo.n_mean(fdo.c_mean(f,0),0),1),1)
    return f

if shapetype=='rectangle':
    if do_centered:
        # We need it to be slightly wider than u_indep to integrate over the whole region
        fini = fdo.ExtendedArray(geom.ncells, stags=(1, 1), nls=(1, 1), nus=(1, 1))
    else:
        fini = fdo.ExtendedArray(geom.ncells, stags=(0, 0), nls=(0, 0), nus=(0, 0))
    fini.arr = np.zeros(fini.nts)
    mask = (np.abs(x)<Lx/4) & (np.abs(y-Ly/6)<Ly/6)
    contour = np.array( [[Lx/4, Lx/4, -Lx/4, -Lx/4, Lx/4], [0, Ly/3, Ly/3, 0, 0]])
    fini[mask] = 1
    if nsmear>0:
        fini = smear(fini, nsmear)
    # Seed some instabilities
    noise = 0
    fini += noise*np.random.randn(*fini.nts)
elif shapetype=='full':
    if do_centered:
        fini = fdo.ExtendedArray(geom.ncells, stags=(1, 1), nls=(1, 1), nus=(1, 1))
    else:
        fini = fdo.ExtendedArray(geom.ncells, stags=(0, 0), nls=(1, 1), nus=(1, 1))
    fini.arr = np.ones(fini.nts)

def get_total(f):
    if ONLY_FEM:
        sp = ORD3_fem
        udv = f*sp.apparent_volume
        return np.sum(udv[span(sp.u_indep)]).decustomize()
    return geom.integrate(f, [[-Lx/2, Lx/2], [-Ly/2, Ly/2]]).item()
thetotal = neat_float(get_total(fini))

#%% Velocity
drmin = min(np.min(dx),np.min(dy))
if speedtype=='uniform':
    # c = V, so this may be used to determine the stability region, which is sum(V)<1 for odd-order methods
    # or all(abs(V)<1) for UTOPIA
    dt = alpha*drmin/np.sum(np.abs(V))
    c0 = V*dt
    # u_indep is set to ones
    r = tuple(-u_indep*x for x in c0)
    vx = V[0]*ex
    vy = V[1]*ey
    w = (-vx*dt, -vy*dt)
elif speedtype=='rotation' or speedtype=='div-less':
    vangle = 1 # angular velocity
    Rmax = np.sqrt((Lx/2)**2 + (Ly/2)**2)
    vmax = vangle*Rmax # maximum linear velocity
    dt = alpha*drmin/vmax
    # Zero-divergence velocity, curl of psi*z_unit
    if do_centered:
        psi = fdo.ExtendedArray(geom.ncells, stags=(0,0), nls=(5,5), nus=(5,5))
    else:
        psi = fdo.ExtendedArray(geom.ncells, stags=(1,1), nls=(5,5), nus=(5,5))
    psi.arr = np.full(psi.nts, fill_value=np.nan)
    rb = np.sqrt(xb**2+yb**2)
    if speedtype=='rotation':
        psi.setv = -rb**2/2*vangle # at points (i-1/2,j-1/2)
    elif speedtype=='div-less':
        psi.setv = (vmax*Rmax/np.pi)*np.cos(np.pi*xb/Lx)*np.cos(np.pi*yb/Ly)
    # if do_centered, vx and vy are still given at correct points (boundaries between cells)
    vx =  geom.grad(psi, 1) # vx at points (i-1/2,j)
    vy = -geom.grad(psi, 0) # vy at points (i,j-1/2)
    print('div(v0) =', np.max(np.abs(geom.divergence([vx, vy]))))
    if do_centered:
        vxc = geom.c_aver(vx, 0)
        vyc = geom.c_aver(vy, 1)
    else:
        vxc = fdo.n_mean(vx, 0)
        vyc = fdo.n_mean(vy, 1)
    vxn = LWtail.interpolate_on_displacement(vxc,
        (-vxc.view[span(u_indep)]*dt, -vyc.view[span(u_indep)]*dt))
    vyn = LWtail.interpolate_on_displacement(vyc,
        (-vxc.view[span(u_indep)]*dt, -vyc.view[span(u_indep)]*dt))
    vxndt = (vxn + vxc)*dt/2
    vyndt = (vyn + vyc)*dt/2
    r = (-vxndt, -vyndt) # Where the previous point is
    # For FEM schemes, velocity should stick out of the u_indep domain just enough so that
    # it after averaging in spectator axis covers all the corners
    if do_centered:
        w = (-vx.view[0:end, -half:end+half]*dt, -vy.view[-half:end+half, 0:end]*dt)
        #w = (-vx.view[-1:end+1, -half:end+half]*dt, -vy.view[-half:end+half, -1:end+1]*dt)
    else:
        w = (-vx.view[half:end-half,0:end]*dt, -vy.view[0:end,half:end-half]*dt)
    # Check the divergence
    divv = geom.divergence([w[0], w[1]])
    print('Divergence of velocity v =', np.max(np.abs(divv)))

#%% Initial values
nmethods = len(methods)
farr=[]
for kmethod, alg in enumerate(methods):
    d = 5
    if do_centered:
        f = fdo.Field('f_'+alg, geom.ncells, stags=(1,1), nls=(d+1,d+1), nus=(d+1,d+1))
        if bctype=='periodic':
            # The periodic boundary conditions may set f.bc[0]=f.bc[end], too -- 
            # it is independent of direction
            f.bc[span, end+half:end+d+half] = f.bc[span, half:d+half]
            f.bc[span, -d-half:-half]  = f.bc[span, end-d-half:end-half]
            f.bc[end+half:end+d+half, half:end-half]  = f.bc[half:d+half, half:end-half]
            f.bc[-d-half:-half, half:end-half]  = f.bc[end-d-half:end-half, half:end-half]
        elif bctype=='zero':
            f.bc[span, end+half:end+d+half] = 0
            f.bc[span, -d-half:-half] = 0
            f.bc[end+half:end+d+half, half:end-half] = 0
            f.bc[-d-half:-half, half:end-half] = 0
        else:
            raise Exception('unknown bctype')
    else:
        f = fdo.Field('f_'+alg, geom.ncells, stags=(0,0), nls=(d,d), nus=(d,d))
        if bctype=='periodic':
            # The periodic boundary conditions may set f.bc[0]=f.bc[end], too -- 
            # it is independent of direciton
            f.bc[span,    end:end+d]  = f.bc[span,            0:d]
            f.bc[span,        -d:-1]  = f.bc[span,    end-d:end-1]
            f.bc[end:end+d, 0:end-1]  = f.bc[0:d,         0:end-1]
            f.bc[-d:-1,     0:end-1]  = f.bc[end-d:end-1, 0:end-1]
        elif bctype=='zero':
            f.bc[span, end:end+d] = 0
            f.bc[span, -d:0] = 0
            f.bc[end:end+d, 1:end-1] = 0
            f.bc[-d:0, 1:end-1] = 0
        else:
            raise Exception('unknown bctype')
    f.bc.freeze()
    f.setv = 0 # to clear the ghost cells
    f.setv = fini
    f.bc.apply()
    farr.append(f)

#%% Figures
plt.close('all')
figs = []
if shapetype=='full':
    zmin = 1-dzlim
    zmax = 1+dzlim
else:
    zmin = -.5
    zmax = 1.5
for kmethod in range(nmethods):
    if savefig is None:
        savedir = None
    else:
        savedir = figdir+'/'+methods[kmethod]+'_'+speedtype+'_'+str(fignumshift+kmethod)
        makedir(savedir)
    fig = UpdateablePcolor(fignumshift+kmethod, zmin=zmin, zmax=zmax, savedir=savedir, savefmts=savefig)
    figs.append(fig)

#%% Let's go!
Nt = 10000
nrotmax = 5
t = 0
lines = [None]*nmethods
def rotmat(th):
    s = np.sin(th)
    c = np.cos(th)
    return np.array([[c, -s], [s, c]])
for kt in range(Nt+1):
    for kmethod, alg in enumerate(methods):
        f = farr[kmethod]
        df = advection_step(alg, f, r, w)
        f += df
        assert not np.isnan(f.arr).any()
        f.bc.apply()
        assert not np.isnan(f.arr).any()
    t += dt
    last_plot = False
    if speedtype=='rotation':
        nrot = t*vangle/(2*np.pi)
        last_plot=(nrot>nrotmax)
    if kt<100 or (kt<500 and kt % 10==0) or kt % 100==0 or last_plot:
        totals = [neat_float(get_total(f) - thetotal) for f in farr]
        print('kt =',kt, ', init total =', thetotal, ', delta totals =', totals)
        for kmethod in range(nmethods):
            fig = figs[kmethod]
            f = farr[kmethod]
            alg = methods[kmethod]
            if fig.started_plotting and not plt.fignum_exists(fig.fignum):
                print('Figure closed, exiting ...')
                plt.close('all')
                sys.exit()
            the_title = alg + ' in [{:g}, {:g}], '.format(np.min(f), np.max(f))
            if speedtype=='uniform':
                the_title += 'D = [{:g}, {:g}]'.format(t*V[0], t*V[1])
            elif speedtype=='rotation':
                the_title += 'nrot = {:g}'.format(nrot)
            else:
                the_title += 'kt = {:d}'.format(kt)
            fig.plot(geom, f, title=the_title, save=False)
            if shapetype=='rectangle' and speedtype=='rotation':
                if lines[kmethod] is not None:
                    lines[kmethod].remove()
                crot = rotmat(t*vangle) @ contour
                lines[kmethod],=fig.ax.plot(crot[0], crot[1],'w',linewidth=0.3)
            fig.save() # after we draw the little rectangle
            #fig.fig.canvas.draw()
        plt.pause(.1)
        #figs[0].fig.waitforbuttonpress() # progress by clicking mouse in Figure 100
    if last_plot:
        break

plt.show()
