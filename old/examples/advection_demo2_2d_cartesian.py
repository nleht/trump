#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec  4 19:18:10 2016

Solution of advective equation:

df/dt + div(v*f) = 0

using various linear schemes.

f in div(v*f) is represented as a "face value" of f

Literature:

Leonard [1979] - 3rd order upwind scheme in 1D (called QUICKEST)
Leonard and Niknafs [1991] - in 2D (called UTOPIA)
Leonard [1991] - the flux-limiting algorithm in 1D (called ULTIMATE)

@author: nle003
"""

#%% Preliminaries
# Imports
import sys
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import time
import fidelior_old as fdo
from fidelior_old import end, half, span, n_mean, c_mean, GeometryCartesian, GeometryCylindrical, \
    c_mean, n_mean, c_diff, n_diff, c_upc, n_upc, minmax, \
    n_interp_upc, c_interp_upc
from fidelior_old.plotting import UpdateablePcolor
fdo.set_global_debug_level(3) # 0 is dangerous!
fdo.set_sparse(True) # was by default, but just in case
if not fdo.numpy_override.entered:
    fdo.numpy_override.enter()

class neat_float(float):
    "Short-print of floats"
    def __str__(self):
        return "%0.5g" % self.real
    def __repr__(self):
        return self.__str__()


#%% Advection algorithms
def FaceValue_UTOPIA(geom, f, vx, vy):
    "vx, vy have dimensions of space"
    # The flux is fluxx=vxcx*fvx and fluxy=vycy*fvy
    # First-order face value
    #assert isinstance(f.geom,Geometry2D) and vx.geom is vy.geom is f.geom
    #g = f.geom
    dx = geom.grids[0].delta
    dy = geom.grids[1].delta
    if isinstance(geom, GeometryCylindrical):
        is_cyl=True
        g = geom.grids[0]
    elif isinstance(geom, GeometryCartesian):
        is_cyl=False
    else:
        raise Exception('Unknown geometry type')
    if is_cyl:
        fv1 = [c_upc(f, vx, 0)*g.r_c, c_upc(f*g.r_c, vy, 1)]
    else:
        fv1 = [c_upc(f,vx,0), c_upc(f,vy,1)]
    # Third order face value
    # It is only function of dfx,dfy, no f
    if is_cyl:
        dfx = geom._grad(f,0)*g.r_c
        dfy = geom._grad(f*g.r_n,1)
    else:
        dfx = geom._grad(f,0)
        dfy = geom._grad(f,1)       
    # Weighted-averaged with velocity
    dfxw = n_interp_upc(dfx, c_mean(vy,0)/dy, 1)
    dfyw = n_interp_upc(dfy, c_mean(vx,1)/dx, 0)
    # Corrected dfx,dfy for 3rd-order method
    nvx = dx * np.sign(vx)
    nvy = dy * np.sign(vy)
    dfxc = c_interp_upc(dfxw,(nvx+vx)/3/dx, 0)
    dfyc = c_interp_upc(dfyw,(nvy+vy)/3/dy, 1)
    #dfv3 = [(nvx-vx)/2.*dfxc - c_upc(n_upc(vy*dfy,vy,1),vx,0)/2.,
    #        (nvy-vy)/2.*dfyc - c_upc(n_upc(vx*dfx,vx,0),vy,1)/2.]
    dfv3 = [[(nvx-vx)/2.*dfxc, -c_upc(n_upc(vy*dfy,vy,1),vx,0)/2.],
            [(nvy-vy)/2.*dfyc, -c_upc(n_upc(vx*dfx,vx,0),vy,1)/2.]]
    # Face value is
    # f_x=fv1[0]+dfv3[0][0]+dfv3[0][1] at (i-1/2,j)
    # and
    # f_y=fv1[1]+dfv3[1][0]+dfv3[1][1] at (i,j-1/2)
    return (fv1,dfv3)

# The flux correction algorithm of Leonard [1991]
#def arrmin(f1,f2,s): return(f1+f2-s*np.abs(f1-f2))/2.
#def arrmin3(f1,f2,f3,s): return arrmin(f1,arrmin(f2,f3,s),s)
#def arrmax(f1,f2,s): return (f1+f2+s*np.abs(f1-f2))/2.
def minmax3(f1, f2, f3, s):
    return minmax(f1, minmax(f2, f3, s), s)
TVD_tmp = None
def TVD_ULTIMATE(geom, f, vcr, fv1, dfv3, apply=True):
    "as usual, v has dimensions of length"
    #assert isinstance(f.geom,Geometry2D) and vcr[0].geom is vcr[1].geom is f.geom
    if not apply:
        fluxl=[(fv1[0]+dfv3[0][0]+dfv3[0][1])*vcr[0],
               (fv1[1]+dfv3[1][0]+dfv3[1][1])*vcr[1]]
        return fluxl
    #g = f.geom
    if isinstance(geom, GeometryCylindrical):
        is_cyl=True
        g = geom.grids[0]
    elif isinstance(geom, GeometryCartesian):
        is_cyl=False
    else:
        raise Exception('Unknown geometry type')
    fluxl=[]
    divide_in_beginning = False # makes no difference, maybe False is more intuitive (recommended)
    for a in range(2):
        da = geom.grids[a].delta
        # All of the following variables have dimensions of f in cartesian or f*r in cylindrical
        # (except tmp,s which are dimensionless)
        #print('a=',a)
        # Leonard's [1991] phiC, phiD and phiU
        #fC = 0 # c_up(f,vcr[a],a) # same as fv1[a]
        if divide_in_beginning:
            if is_cyl:
                r = g.r_c
                #if a==0:
                #    r = g.r_c
                #else:
                #    r = c_ave(g.r_n,1)
            else:
                r = 1
            dfa = c_diff(f,a) #*da
        else:
            r = 1
            if is_cyl:
                if a==0:
                    dfa = c_diff(f,0)*g.r_c #*da
                if a==1:
                    dfa = c_diff(f*g.r_n,1) #*da
            else:
                dfa = c_diff(f,a) #*da
        fv = dfv3[a][0]/r
        fD = np.sign(vcr[a])*dfa
        CURV = c_upc(n_diff(dfa,a),vcr[a],a) # same sign indep of v
        fU = CURV - fD
        DEL= fD - fU
        fref = fU.copy()
        fref.arr = fU.arr.copy()
        inz=(vcr[a]!=0)
        tmp = fU.copy()
        tmp.arr = np.zeros(tmp.nts) #.alloc(np.double)
        tmp.setv = np.abs(vcr[a])/da
        fref[inz] -= fU[inz]/tmp[inz]
        # Note that when velocity is zero, flux is zero and there is nothing
        # to limit.
        s=np.sign(DEL)
        fvnew = minmax(minmax3(fv,fref,fD,s),0,-s)
        ic = (np.abs(CURV)>=np.abs(DEL))
        fvnew[ic] = 0
        fluxl.append(vcr[a]*(fv1[a] + fvnew*r + dfv3[a][1]))
    return fluxl

# Various algorithms for advection
def AdvectionStepI(geom,f,vx,vy,alg=None):
    "vx, vy have dimensions of space, i.e., are actual velocity times dt"
    #assert isinstance(f.geom,Geometry2D) and vx.geom is vy.geom is f.geom
    #g = f.geom
    dx = geom.grids[0].delta
    dy = geom.grids[1].delta
    if (vx.arr>dx).any() or (vy.arr>dy).any():
        print('WARNING: CFL condition is not satisfied in AdvectionStepI')
    cyl_flux_methods = ['CIR','UTOPIA','ULTIMATE']
    if alg in cyl_flux_methods:
        fv1, dfv3 = FaceValue_UTOPIA(geom, f, vx, vy) # in cylindrical, these are facevalue x radius
    if alg=='UTOPIA' or alg=='ULTIMATE':
        fluxx, fluxy = TVD_ULTIMATE(geom, f, (vx,vy), fv1, dfv3, apply=(alg=='ULTIMATE'))
    if alg=='CIR':
        # First-order upstream method [Courant, Isaacson and Rees, 1952]
        df = -geom.divergence([vx*fv1[0], vy*fv1[1]])  # is required df x radius
    elif alg=='MacCormack_Reverse':
        # Stable, 2nd order
        # Even more stable than Lax-Wendroff
        # Slightly better than MacCormack?? About the same
        # 1. Calculate interim value
        fi = f - geom.divergence([vx*c_upc(f,-vx, 0), vy*c_upc(f, -vy, 1)])
        df = (-f+fi)/2 - geom.divergence([vx*c_upc(fi, vx, 0), vy*c_upc(fi, vy, 1)])/2
    elif alg=='MacCormack':
        # Stable, 2nd order
        # 1. Calculate interim value
        fi = f - geom.divergence([vx*c_upc(f, vx, 0), vy*c_upc(f, vy, 1)])
        df = (-f+fi)/2 - geom.divergence([vx*c_upc(fi, -vx, 0), vy*c_upc(fi, -vy, 1)])/2.
    elif alg=='UTOPIA' or alg=='ULTIMATE':
        # Third-order upstream method, without or with flux correction
        df = -geom.divergence([fluxx,fluxy])
    else:
        raise Exception('Unknown advection algorithm: '+alg)
    if isinstance(geom, GeometryCartesian):
        return df
    elif isinstance(geom, GeometryCylindrical):
        # First of all, make sure we have the axis
        if not df.nls[0]>=0:
            raise Exception('AdvectionStepI: df is not extended to axis. Possible cure:\n'+\
                            '\tincrease nls[0] of '+f.name+', vr, vz or r/rc')
        if alg in cyl_flux_methods:
            return df/geom.grids[0].r_n
        else: # MacCormack methods
            return df
    else:
        raise Exception('Unknown geometry type')

################################################################################
#%% Testing
vmax = 0.45
do_opposite_rotation = False
num_smear = 0
do_log_plot = False

# Setup the grid and velocity field: CCW for r<R1, CW for R1<r<R2, w=vmax/R2
# The unknown function f is calculated on a Nx x Ny array, at x,y coordinates
Lx = 2
Ly = 2
#Lx = 200; Ly=200; dx=1; dy=1
Nx = 200
Ny = 200
dx = Lx/Nx
dy = Ly/Ny
gridx = fdo.Grid(num_cells=Nx, delta=dx, start=-Lx/2)
gridy = fdo.Grid(num_cells=Ny, delta=dy, start=-Ly/2)
geom = fdo.GeometryCartesian((gridx,gridy),nls=(3,3),nus=(3,3))
x = geom.grids[0].r_n
y = geom.grids[1].r_n
xc = geom.grids[0].r_c
yc = geom.grids[1].r_c
xpcolor = xc[-half:end+half].flatten()
ypcolor = yc[-half:end+half].flatten()
# xec = x[:].flatten()
# yec = y[:].flatten()

#%%
# Velocity (scaled), calculated on the grid edges as v = curl (psi z)
#R2=np.min([Lx/2-3*dx,Ly/2-3*dy])
R2 = np.sqrt(2)*Lx/2
if do_opposite_rotation:
    R1 = np.min([Lx/4,Ly/4])
else:
    R1 = 0
vangle=vmax/R2 # angular velocity

# Zero-divergence velocity, curl of psi*z_unit
#xe = geom.grids[0].r_c[-half:end+half].flatten()
#ye = geom.grids[1].r_c[-half:end+half].flatten()
ncells = geom.ncells
psi = fdo.ExtendedArray(ncells, stags=(1,1), nls=(3,3), nus=(3,3))
psi.arr = np.full(psi.nts, fill_value=np.nan)
rc = np.sqrt(xc**2+yc**2)
psi.setv = -rc**2/2*vangle # at points (i-1/2,j-1/2)
i2 = (rc>=R1)
i2common = fdo.common_index(i2, psi, rc)
psi[i2common] = rc[i2common]**2/2*vangle - R1**2*vangle
i3 = (rc>=R2)
psi[i3] = R2**2/2*vangle-R1**2*vangle
vx =  geom.grad(psi, 1) # vx at points (i-1/2,j)
vy = -geom.grad(psi, 0) # vy at points (i,j-1/2)
# Check if the divergence is zero at points (i,j)
print('max div v =', np.max(np.abs(geom.divergence([vx, vy]))), flush=True)
dt = 1/(np.max(vx)/dx+np.max(vy)/dy)

################################################################################
#%% Prepare the figures
methods=['CIR','MacCormack','MacCormack_Reverse','UTOPIA','ULTIMATE']
nmethods=len(methods)

#%%#############################################################################
# Initial value
fini = fdo.ExtendedArray(ncells, stags=(0,0),nls=(2,2),nus=(2,2))
fini.arr = np.zeros(fini.nts)
#yg,xg=np.meshgrid(fini.xe(1),fini.xe(0))
xsqc=0.; ysqc=Ly/6.
ax=Lx/4; ay=Ly/6
fini[(np.abs(x-xsqc)<ax) & (np.abs(y-ysqc)<ay)]=1.
def smear(ea,n=0):
    for k in range(n):
        ea.setv = n_mean(c_mean(n_mean(c_mean(ea,0),0),1),1)
    #return ea
smear(fini,num_smear)

################################################################################
#%% The main cycle
# Initial value
farr=[]

for kmethod in range(nmethods):
    if kmethod>=1: dd = 2
    else: dd = 1
    f0 = fdo.Field('f_'+methods[kmethod], ncells, stags=(0,0), nls=(dd,dd), nus=(dd,dd))
    tt1 = time.time()
    if dd==1:
        f0.bc[-1,-1:end+1]=0
        f0.bc[end+1,-1:end+1]=0
        f0.bc[:,-1]=0
        f0.bc[:,end+1]=f0.bc[:,end]
    else:
        f0.bc[-2:-1,-2:end+2]=0
        f0.bc[end+1:end+2,-2:end+2]=0
        f0.bc[:,-2:-1]=0
        f0.bc[:,end+1]=f0.bc[:,end]
        f0.bc[:,end+2]=f0.bc[:,end]
    tt2 = time.time()
    f0.bc.freeze() # takes pretty long
    tt3 = time.time()
    print('To record: t = ',tt2-tt1,', to freeze = ',tt3-tt2,flush=True)
    farr.append(f0)

#%%
for f0 in farr:
    f0.setv = fini
totals = [geom.integrate(f, [[-Lx/2, Lx/2], [-Ly/2, Ly/2]]).item() for f in farr]
print('totals =',totals)#xe = f0.pcolor_x(0)

#%% Initialize figures
plt.close('all')
savedirs=[None]*nmethods
info = lambda n,f,m: '[{:.3f},{:.3f}],n={:.2f},{:s}'.format(np.min(f.arr),np.max(f.arr),n,m)
zmin,zmax = (-8,.5) if do_log_plot else (-.4,1.4)

figs=[UpdateablePcolor(100+kmethod,figsize=(6,5),savedir=savedirs[kmethod],
                       cmap=mpl.cm.jet,zmin=zmin,zmax=zmax)
     for kmethod in range(nmethods)]


#figs = []
#for kmethod in range(nmethods):
#    fig = plt.figure(100+kmethod)
#    figs.append(fig)

#%%
d_n_turns = vangle*dt/(2*np.pi)
tot_turns = 100
print('Wait for',tot_turns,'turns')
Nt = np.int(np.ceil(tot_turns/d_n_turns))
for kt in range(Nt+1):
    for kmethod in range(nmethods):
        farr[kmethod] += AdvectionStepI(geom, farr[kmethod], vx*dt, vy*dt, methods[kmethod])
        farr[kmethod].bc.apply()
    #f = farr[-1]
    #fv1, dfv3 = FaceValue_UTOPIA(f,vx*dt,vy*dt)
    #fluxx,fluxy = TVD_ULTIMATE(f,(vx*dt,vy*dt),fv1,dfv3,apply=True)
    #f -= geom.Div(fluxx,fluxy)
    #f.bc.apply()
    if kt<100 or (kt<500 and kt % 10==0) or kt % 100==0 or kt==Nt:
        n_turns = d_n_turns*kt
        for kmethod in range(nmethods):
            f = farr[kmethod]
            dtot = geom.integrate(f, [[-Lx/2, Lx/2], [-Ly/2, Ly/2]]).item()-totals[kmethod]
            #fplot = np.log10(f+1e-8) if do_log_plot else f
            fig = figs[kmethod]
            if fig.started_plotting and not plt.fignum_exists(fig.fignum):
                print('Figure closed, exiting ...')
                plt.close('all')
                sys.exit()
            fig.plot(geom, f, title=info(n_turns,f,methods[kmethod]+' '+str(neat_float(dtot))))
#            fig = figs[kmethod]
#            fig.clear()
#            #plt.clf()
#            ax = fig.subplots()
#            ax.pcolor(xpcolor, ypcolor, f[:,:].T)
#            ax.set_aspect('equal')
#            #fig[kmethod].ax.pcolor(xplot, yplot, fplot[:,:].T)
#            ax.set_title(info(n_turns,f,methods[kmethod]+' '+str(neat_float(dtot))))
#            fig.canvas.draw()
        plt.pause(0.1)
        # fig[0].wait() # progress by clicking mouse in Figure 100

plt.show()
