# Examples (old)

## Important notice

Examples below are use the [_old_](/fidelior_old) interface, and will soon be rewritten! Please use the [_new FIDELIOR_](/fidelior) and [new examples](/examples).

## Advection equation solution in one dimension
Solution of one-dimensional advection equation
```math
\partial_t f = - v \partial_x f
```
is demonstrated in [this example](advection_demo1_1d.py).

We use several different methods to solve it. The examples of linear methods are (choose `method_set = 0` in the beginning of the file):

  * First order - CIR (Courant-Isaacson-Rees) method
  * Second order - MacCormack method (a more stable modification of Lax-Wendroff)
  * Third order - Leonard method
  * Fifth order - automatically generated.

All these methods are known from literature, but may also be automatically generated, as demonstrated in the code:

```python
SCHEMES = {'CIR\'': GeneratedScheme(N=1,M=-1),
           'LW\'': GeneratedScheme(N=2,M=-1),
           'WB\'': GeneratedScheme(N=2,M=-2),
           'Leonard\'': GeneratedScheme(N=3,M=-2),
           '5th order':GeneratedScheme(N=5,M=-3) }
```

![Advection in 1D: Linear methods](figures/advec1d_linear.png)

If one chooses `method_set = 1`, then several methods of second and third order with flux limiters are demonstrated. It is shown that the most popular second-order flux-limited schemes are not well reproducing the half-ellipse shape (there is steepening of gradients at its boundaries). The third-order method with flux-limiter (due to Leonard and called `ULTIMATE`) works best:

![Advection in 1D: Flux limiters](figures/advec1d_fluxlim.png)

## Advection equation in two dimensions

Then, a more complicated [example](advection_demo2_2d_cartesian.py) which demonstrate solution of advection equation

```math
\frac{\partial{f}}{\partial{t}} = -\nabla\cdot(\mathbf{v}f)
```

It describes rotation, i.e., uses divergenceless velocity given by

```math
\mathbf{v} = \omega(\hat{\mathbf{z}}\times\mathbf{r})
```
    
The featured third-order method is of upwind type and also uses a flux corrector due to the same
author. To quote the author, "the fourth-order diffusion kills the third-order dispersion", therefore
the results are even better than if the fourth-order method was used.

## Advection in three dimensions

See [this example](advection_demo3_3d_const_v.py). The first, second, and third order methods are demonstrated on a cartesian 100x100x100 grid. The algorithms were generated automatically, given the stencil. Here is the result, after advecting a box through `D` grid cells:

![Advection in 3D](figures/advec3d.png)

As we see, the 3rd-order method (`UTOPIA`, Leonard-Niknafs) preserved the shape of the box, while the 2nd-order (`LW`, Lax-Wendroff) has distorted it beyond recognition and the 1st-order (`CIR`, Courant-Isaacson-Rees) has diffused it away completely.

