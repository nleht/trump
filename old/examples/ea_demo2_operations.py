#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  9 15:54:06 2020

Demo of overloading functions for ExtendedArray

@author: nleht
"""

import numpy as np
import fidelior_old as fdo
from fidelior_old import ea, co, ExtendedArray
fdo.set_global_debug_level(4)

use_global_context = False
# Or, only EA
use_global_ea_context = False
# Or, we will just use 'with ...' construct in the code snippets where it is needed

if use_global_context:
   fdo.numpy_override.enter()

if use_global_ea_context:
    ea.EA_OVERRIDE.enter()

#%% Set up geometry
#grid = fdo.Grid(num_cells=4,delta=0.25,start=0)
#geom = fdo.geometry_flat((grid,))
ncells=(4,)

#%% Some custom operations
# allow_symbolic=False means that this operation is nonlinear and cannot be used in linear operators
@fdo.extend_vectorized_function(nin=1, allow_symbolic=False)
def non_negative(x):
    "Non-negative x"
    y = x.copy()
    y[x<0]=0
    return y

#%%
@fdo.extend_vectorized_function(nin=2,allow_symbolic=True,copy_info=False)
def my_plus(x,y):
    "Nocomment"
    return x+y

@fdo.extend_vectorized_function(nin=2,allow_symbolic=True,in_place=True)
def increment(x,y):
    x += y
    return x

#%% Set up arrays
c1 = ExtendedArray(ncells, stags=(0,), nls=(3,), nus=(2,))
c1.arr = np.random.randn(*c1.nts)
c2 = ExtendedArray(ncells, stags=(0,), nls=(2,), nus=(4,))
c2.arr = np.random.randn(*c2.nts)
c3 = ExtendedArray(ncells, stags=(0,), nls=(-1,), nus=(-1,))
c3.arr = np.random.randn(*c3.nts)
a1 = c1[:] # same as c1[0:end]
a2 = c2[:]
# Cannot use c3[0:end] because only points 1..end-1 are defined.
# But c3[span] works (same as c3.arr) and is the same as c3[1:end-1]
# Try span(c3)
print('c1 =',c1,'\nc2 =',c2,'\nc3 =',c3)

#%% NumPy functions have been overloaded
print(co.blue('\n*** Overloaded unary functions ***\n'))
print('Function list =',ea.NUMPY_FUNCS)
with ea.EA_OVERRIDE: # will be ignored if we already entered EA.EA_OVERRIDE or NUMPY_OVERRIDE
    print('For example,\nsin(c1) =',np.sin(c1))

#%% The 'minimum' operation
print(co.blue('\n*** Overloaded binary function: np.minimum ***'))
with ea.EA_OVERRIDE: # will be ignored if we already entered EA.EA_OVERRIDE or NUMPY_OVERRIDE
    d = np.minimum(c1,c2) # overloaded np.minimum
b = np.minimum(a1,a2)
print('a1 =',a1,'\na2 =',a2,'\nmin(a1,a2) =',b)
print('Must be the same:\n',c1[:],'\n',c2[:],'\n',d[:])
print('With ExtendedArrays:\nc1 =\t\t\t',c1,'\nc2 =\t\t\t',c2,'\nmin(c1,c2) =\t',d)

#%% A custom 'non-negative' operation
print(co.blue('\n*** Custom unary function: non_negative ***'))
print('c1 =\t\t\t\t',c1,'\nnon_negative(c1) =\t',non_negative(c1))

#%% A custom binary operation
print(co.blue('\n*** Custom binary function: plus ***'))
print('NumPy arrays:\n a1+a2 =',a1+a2,'\n my_plus(a1,a2) =',my_plus(a1,a2))
print('ExtendedArrays:\n c1+c2 =',c1+c2,'\n my_plus(c1,c2) =',my_plus(c1,c2))

#%% A custom in-place operation
print(co.blue('\n*** Increment ***'))
print('c1 = ',c1,'\nc2 =',c2)
increment(c1,c2)
print('increment(c1,c2)\nc1 = ',c1)

if use_global_ea_context:
    ea.EA_OVERRIDE.exit()
    
if use_global_context:
    fdo.numpy_override.exit()
