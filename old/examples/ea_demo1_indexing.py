#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May 31 00:04:27 2020

Demonstration of elementary operations with extended arrays.

How to write your own functions that can be applied to both regular NumPy arrays and ExtendedArrays.

@author: nleht
"""

import numpy as np
import fidelior_old as fdo
from fidelior_old import co, ExtendedArray, span, end, half, c_mean, c_diff, n_mean, n_diff, c_upc

#%% Set up geometry
#grid = fdo.Grid(num_cells=4,delta=0.25,start=0)
#geom = fdo.geometry_flat((grid,))
ncells = (4,)

#%% A demo of elementary features
print(co.blue('\n*** Elementary features ***\n'))
# Array tracking the boundaries (nodes)
eab = ExtendedArray(ncells, stags=(0,), nls=(3,), nus=(2,))
eab.arr = np.arange(1.,eab.nts[0]+1.,1.)**2 # this is the real storage
print('eab =',eab)
# Array tracking the centers of the cells
eac = c_mean(eab)
print('Average: c_mean(eab) = eac =', eac)
print('Gradient: c_diff(eab) = dea =', c_diff(eab))
print('Shifted: eab.shifted(1) =',eab.shifted((1,)))
v = eac.copy() # copies only the array structure, not the values, use copy_arr() to do that
v.arr = np.random.randn(*v.nts)
print('Conditionally shifted by half cell upstream velocity\n v =',v,'\n c_upc(eab,v) =',c_upc(eab,v))

#%% Some elementary indexing
print(co.blue('\n*** Elementary indexing ***\n'))
print('eab[0] ==',eab[0])
print('eac[',half,'] ==',eac[half])
print('eab[0:end] =',eab[0:end])
print('eab[:] =',eab[:]) # same as eab[0:end]
# You can address the storage directly
def repr_index(i):
    ir = eac.ea_index(i)
    return ', '.join(repr(i_) for i_ in ir)
print('eac.arr[0] = eac[', repr_index(0),'] =',eac.arr[0],' = ',eac[eac.ea_index(0)])
# If the half-index looks weird, you can customize it
old_half_string = fdo.ea.half_string
fdo.ea.half_string = '(1/2)'
print('eac.arr[0] = eac[',repr_index(0),'] =',eac.arr[0],' = ',eac[eac.ea_index(0)])
# Restore it
fdo.ea.half_string = old_half_string

#%% More complicated indexing
print(co.blue('\n*** Advanced indexing ***\n'))
i = co.customize(np.array([1,2,3])) # necessary for operations like array + user_defined_type
print('eab[',i,'] =',eab[i])
print('eac[',i+half,'] =',eac[i+half])
# Logical indexing
ealb = n_mean(eac).copy()
ealb.arr = (np.random.randn(*ealb.nts)<0)
ealc = eac.copy()
ealc.arr = (np.random.randn(*ealc.nts)<0)
print('Logical arrays:\n ealb =',ealb,'\n ealc =',ealc)
print('eab[ealb] =',eab[ealb])
print('eac[ealc] =',eac[ealc])
print('Assigning eab[ealb] = 0 ...')
eab[ealb] = 0
print('eab =',eab)
# The operation 'where' has been overloaded:
ii = np.where(ealb)
print('eab[where(ealb)] =',eab[ii])
print('eab[where(~ealb)] =',eab[np.where(~ealb)])
# -- notice that ~ has been overloaded, for more see next demo ea_demo2_operations.py

#%% Fractional indexing: only in the 'centered' arrays
print('eac[',half,'] =',eac[half])
print('eac[',end-half,'] =',eac[end-half])
print('eac[',i-half,'] =',eac[i-half])
print('eac[',half,':',end-half,'] =',eac[half:end-half])
print('eac[:] =',eac[:]) # same as eac[half:end-half]

#%% Error checking: make sure that incorrect indices are not passed
test_indices = [0, end, half, end-half,
                -1, -4, end+1, end+3,
                -half, -3-half, end+half, end+2+half,
                5, 5+half, 7, 6+half,
                # Slices: in Python, written as arr[:], arr[:end+1], arr[:,end+half]
                slice(None), slice(None,end+1), slice(None,end+half),
                # arr[end:0:-1], arr[end-half:half:-1]
                slice(end,0,-1), slice(end-half,half,-1),
                # 'span' produces all available values
                span,
                ealb,ealc]
print(co.blue('\n*** Array with stag == 0 ***\n'))
print('eab =',eab)
for index in test_indices:
    print('eab[',index,'] = ',end='')
    try:
        print(eab[index])
    except Exception as e:
        print(co.alert('<Caught'),co.error(repr(e)),co.alert('>'))

print(co.blue('\n*** Array with stag == 1 ***\n'))
print('eac =',eac)
for index in test_indices:
    print('eac[',index,'] = ',end='')
    try:
        print(eac[index])
    except Exception as e:
        print(co.alert('<Caught'),co.error(repr(e)),co.alert('>'))

