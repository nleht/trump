#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 29 15:00:12 2020

A bunch of plotting routines

@author: nleht
"""

import pickle
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
#import trump
#from . import extended_array as EA
from .extended_arrays import end, half, span, ExtendedArray, widened_span, HalfIndex, EndIndex
from .geometries import GeometryCylindrical


#%%#################################################################################################
def _geomx(geom, ea, axis, switch):
    return geom.grids[axis].r_n if ea.stags[axis]==switch else geom.grids[axis].r_c
def _le(index1, index2):
    if isinstance(index1, HalfIndex):
        assert isinstance(index2, HalfIndex)
        return _le(index1.int, index2.int)
    if isinstance(index1, EndIndex):
        assert isinstance(index2, EndIndex)
        return _le(index1.i, index2.i)
    return index1<=index2
def _min(index1, index2):
    b = _le(index1, index2)
    return index1 if b else index2
def _max(index1, index2):
    b = _le(index1, index2)
    return index2 if b else index1

class pcolor_data:
    def __init__(self, geom, ea, cylwideplot=True, pseudo=False):
        xplots = []
        slices = []
        for axis in range(ea.ndim):
            xe = _geomx(geom, ea, axis, 1)
            swe = widened_span(ea, axis=axis)
            sde = slice(0,end,None) if ea.stags[axis]==1 else slice(-half, end+half, None)
            se = slice(_max(swe.start, sde.start), _min(swe.stop, sde.stop), None)
            sw = span(ea, axis=axis)
            sd = slice(0,end,None) if ea.stags[axis]==0 else slice(half, end-half, None)
            s = slice(_max(sw.start, sd.start), _min(sw.stop, sd.stop), None)
            slices.append(s)
            xplots.append(xe[se].flatten())
        self.x, self.y = xplots
        x = self.x
        self.sx, self.sy = slices
        self.cyl = isinstance(geom, GeometryCylindrical) and cylwideplot
        self.pseudo = pseudo
        self.st = ea.stags[0]
        if self.cyl:
            if self.st==0:
                self.x=np.hstack((-x[::-1],x[2:]))
            else:
                self.x=np.hstack((-x[::-1],x[1:]))
    def __call__(self, ea):
        f = ea.view[self.sx, self.sy].arr
        if self.cyl:
            if self.st==0:
                f = np.vstack((f[::-1,:]*(-1 if self.pseudo else 1), f[1:,:]))
            else:
                f = np.vstack((f[::-1,:]*(-1 if self.pseudo else 1), f))
        return f

def ea_plot(geom, ea, plot_all=False, scale=1, plotargs=(), plotopts={}):
    """Quick and dirty plot"""
    assert isinstance(ea, ExtendedArray)
    if ea.ndim==1:
        assert geom.ndim==1
        g = geom.grids[0]
        x = (g.r_n if ea.stags[0]==0 else g.r_c)
        s = (span(ea) if plot_all else slice(None))
        return plt.plot(x[s]/scale, ea[s], *plotargs, **plotopts)
    elif ea.ndim==2:
        #xes = tuple(geom.grids[axis].r_c if ea.stags[axis]==0 else geom.grids[axis].r_n for axis in [0,1])
        xplots = []
        for axis in range(ea.ndim):
            xe = _geomx(geom, ea, axis, 1)
            if plot_all:
                s = widened_span(ea, axis)
            else:
                s = slice(0,end,None) if ea.stags[axis]==1 else slice(-half, end+half, None)
            #xlo = xe.lo(axis)[s].flatten()
            #xhi = xe.hi(axis)[s].flatten()
            #xplots.append(np.hstack((xlo[0], xhi)))
            xplots.append(xe[s].flatten())
        #print(xplots)
        eaplot = ea.arr.T if plot_all else ea[:,:].T
        tmp = plt.pcolor(xplots[0]/scale,xplots[1]/scale, eaplot, *plotargs, **plotopts)
        a=plt.gca()
        a.set_aspect('equal')
        plt.colorbar()
        return tmp
    else:
        raise Exception('ea_plot is not implemented for ndim>2')

def ea_plot_slice(geom, ea, ir=None, iz=None, scale=1, plotargs=(), plotopts={}):
    if ir is None:
        if iz is None: raise Exception('bla!')
        # Plot alont r
        r = _geomx(geom, ea, 0, 0)[span(ea,0)].flatten()
        plt.plot(r/scale, ea[span,iz],*plotargs,**plotopts)
    else:
        if iz is not None: raise Exception('bla?')
        z = _geomx(geom, ea, 1, 0)[span(ea,1)].flatten()
        plt.plot(z/scale, ea[ir,span].T,*plotargs,**plotopts)

def save_figure(fig,savedir,savefmts,kplot):
    "Independent of what kind of figure we are saving"
    fig.canvas.draw()
    if savedir != None:
        for savefmt in savefmts:
            figfname = savedir+'/img{:05d}.'.format(kplot)+savefmt
            if savefmt=='pkl':
                with open(figfname,'wb') as f:
                    pickle.dump(fig,f)
            else:
                fig.savefig(figfname)
    
# A useful class for pcolor plots
class UpdateablePcolor:
    "Combined for cartesian and cylindrical"
    def __init__(self, fignum=None, zmin=None, zmax=None, savedir=None, savefmts=['png'],
                 figsize=None, scale=1, cmap=mpl.cm.viridis, zoom=None):
        self.fignum=fignum
        self.started_plotting=False
        self.zmin0=zmin
        self.zmax0=zmax
        self.savefmts=savefmts
        self.savedir=savedir
        if self.savedir is not None:
            if isinstance(savefmts,list):
                self.savefmts=savefmts
            else:
                # Only one
                self.savefmts=[savefmts]
        self.figsize=figsize
        self.scale=scale
        self.cmap=cmap
        self.zoom=zoom
        self.kplot = 0
    def plot(self, geom, ea_orig, title=None, xlabel=None, ylabel=None, save=True, num=None):
        if not self.started_plotting:
            self.fig = plt.figure(self.fignum,figsize=self.figsize)
            self.fignum = self.fig.number
            print('Figure',self.fignum,': savedir =',self.savedir)
            self.fig.clear()
            self.ax=self.fig.gca()
            # The first plot
            # ax.set_position([.1,.1,(Lx/Ly)*0.8,0.8])
            self.pcd = pcolor_data(geom, ea_orig, cylwideplot=True, pseudo=False)
        fplot = self.pcd(ea_orig)
        if not self.started_plotting:
            self.xplot = self.pcd.x/self.scale
            self.yplot = self.pcd.y/self.scale
            self.pcolor = self.ax.pcolorfast(self.xplot, self.yplot, fplot.T, cmap=self.cmap)
            self.ax.set_aspect('equal')
            if self.zoom is None:
                self.ax.set_xlim(np.min(self.xplot),np.max(self.xplot))
                self.ax.set_ylim(np.min(self.yplot),np.max(self.yplot))
            else:
                self.ax.set_xlim(self.zoom[0][0]/self.scale,self.zoom[0][1]/self.scale)
                self.ax.set_ylim(self.zoom[1][0]/self.scale,self.zoom[1][1]/self.scale)
            self.sm = mpl.cm.ScalarMappable(cmap=self.cmap)
            self.sm.set_array(np.linspace(0,1,128))
            #self.kplot=0
        else:
            # Replacement plot
            if isinstance(self.pcolor, mpl.image.PcolorImage):
                # Why would they do that? Different interface for even-spaced and not-even-spaced
                self.pcolor.set_data(self.xplot, self.yplot, fplot.T)
            elif isinstance(self.pcolor, mpl.image.AxesImage):
                self.pcolor.set_data(fplot.T)
            else:
                raise SyntaxError('Unknown error')
        if self.zmin0==None:
            zmin=np.min(fplot)
        else:
            zmin=self.zmin0
        if self.zmax0==None:
            zmax=np.max(fplot)
        else:
            zmax=self.zmax0
        self.pcolor.set_clim(zmin,zmax)
        self.sm.set_clim(zmin,zmax)
        if not self.started_plotting:
            self.fig.colorbar(self.sm)
            self.started_plotting=True
        if title is not None:
            self.ax.set_title(title)
        if xlabel is not None:
            self.ax.set_xlabel(xlabel)
        if ylabel is not None:
            self.ax.set_ylabel(ylabel)
        if save:
            self.save(num=num)
        self.kplot += 1
    def save(self, num=None):
        kplot = (self.kplot if num is None else num)
        save_figure(self.fig, self.savedir, self.savefmts, kplot)
    def wait(self):
        self.fig.waitforbuttonpress() # progress by clicking mouse

from mpl_toolkits.axes_grid1 import make_axes_locatable
class UpdateablePcolorAxis:
    "Combined for cartesian and cylindrical"
    def __init__(self, ax, zmin=None, zmax=None, scale=1, pseudo=False, cmap=mpl.cm.viridis,
                 zoom=None):
        self.ax=ax
        self.started_plotting = False
        self.update_axes = True
        self.zmin0=zmin
        self.zmax0=zmax
        self.scale=scale
        self.cmap=cmap
        self.pseudo=pseudo
        self.zoom=zoom
    def plot(self, geom, ea_orig,title=''):
        if self.update_axes:
            self.pcd = pcolor_data(geom, ea_orig, pseudo=self.pseudo)
            # The first plot
            # ax.set_position([.1,.1,(Lx/Ly)*0.8,0.8])
        fplot = self.pcd(ea_orig)
        if self.update_axes:
            self.xplot = self.pcd.x
            self.yplot = self.pcd.y
            self.pcolor = self.ax.pcolorfast(self.xplot,self.yplot,fplot.T,cmap=self.cmap)
            self.ax.set_aspect('equal')
            if self.zoom is None:
                self.ax.set_xlim(np.min(self.xplot),np.max(self.xplot))
                self.ax.set_ylim(np.min(self.yplot),np.max(self.yplot))
            else:
                self.ax.set_xlim(self.zoom[0][0]/self.scale,self.zoom[0][1]/self.scale)
                self.ax.set_ylim(self.zoom[1][0]/self.scale,self.zoom[1][1]/self.scale)
        else:
            # Replacement plot
            if isinstance(self.pcolor, mpl.image.PcolorImage):
                # Why would they do that? Different interface for even-spaced and not-even-spaced
                self.pcolor.set_data(self.xplot, self.yplot, fplot.T)
            elif isinstance(self.pcolor, mpl.image.AxesImage):
                self.pcolor.set_data(fplot.T)
            else:
                raise SyntaxError('Unknown error')
        if self.zmin0==None:
            zmin=np.min(fplot)
        else:
            zmin=self.zmin0
        if self.zmax0==None:
            zmax=np.max(fplot)
        else:
            zmax=self.zmax0
        self.pcolor.set_clim(zmin,zmax)
        if not self.started_plotting:
            self.sm = mpl.cm.ScalarMappable(cmap=self.cmap)
            self.sm.set_array(np.linspace(0,1,128))            
        self.sm.set_clim(zmin,zmax)
        if not self.started_plotting:
            divider = make_axes_locatable(self.ax)
            self.cax = divider.append_axes('right', size='5%', pad=0.05)
            self.ax.figure.colorbar(self.sm, ax=self.ax, cax=self.cax, orientation='vertical')
            #self.ax.colorbar(self.sm)
            self.started_plotting=True
        self.ax.set_title(title)
        self.update_axes = False
    pass

class UpdateablePcolorSubplots:
    "Combined for cartesian and cylindrical"
    def __init__(self, nrows, ncols, num=None, zmin=None, zmax=None, savedir=None, savefmts=['png'],
                 figsize=None, pseudos=None, scale=1, cmap=mpl.cm.viridis, zoom=None):
        nsubplots=nrows*ncols
        self.savedir=savedir
        if isinstance(savefmts,list):
            self.savefmts=savefmts
        else:
            self.savefmts=[savefmts]
        plt.close(num)
        #fig=plt.figure(num,figsize=figsize)
        #fig.clear()
        self.fig,axs = plt.subplots(nrows, ncols, num=num, figsize=figsize)
        self.fig.tight_layout()
        if nsubplots>1:
            axs.resize(nsubplots)
        else:
            axs = [axs]
        if pseudos is None:
            pseudos = [False]*nsubplots
        self.uaxs = np.array([UpdateablePcolorAxis(axs[k], scale=scale, zmin=zmin, zmax=zmax,
            cmap=cmap, pseudo=pseudos[k],zoom=zoom) for k in range(nsubplots)])
        self.fignum = self.fig.number
    def info(self):
        print('Figure',self.fignum,': savedir =',self.savedir)
    def subplot(self,subnum,*arg,**kw):
        self.uaxs[subnum-1].plot(*arg,**kw)
    def save(self,kplot):
        save_figure(self.fig,self.savedir,self.savefmts,kplot)
    def close(self):
        plt.close(self.fignum)
    def wait(self):
        self.fig.waitforbuttonpress() # progress by clicking mouse

