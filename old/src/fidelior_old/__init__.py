#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec  6 14:57:42 2016

FIDELIOR (FInite-Difference-Equation LInear OperatoR package)
-------------------------------------------------------------

For examples, see examples.*

Version history:
    0.1 (versions were not tracked):
        -- initial release
    0.2:
        -- organized as a relative-path package, some files were renamed
        -- added nonuniform module for nonuniform geometries
        -- added methods copy_arr, shifted, __str__ to ExtendedArray
        -- rewrote the 'extend*' decorators
        -- added functions minimum, maximum, minmax
        -- less memory for printing out BC's
        -- added 'enforce' method in the Constraint class (not sure about its utility)
    0.3:
        -- the indices of center-cell arrays are now with 'half' index
    0.4:
        -- reorganized the code, better overrides of NumPy ufunc's, added symbolic
           capability to np.hstack, colorized code, fixed (c x f) where c is a numeric,
           f symbolic extended array; x is binary operation
    0.5:
        -- get rid of "Grid" namedtuple, reorganize how geometries are treated
        -- arbitrary dimensionality of extended_array
        -- renamed ExtendedArray into extended_array, Field into field, etc.
        -- non-uniform grids; the grid coordinates are described by the new 'incomplete dimension'
           extended_arrays
    0.6: (work in progress)
        -- renamed the package in order not to piss off anti-trumpers
        -- TODO: iterative solver capability when the previous (close) solution is known
        -- TODO: automatic difference schemes: see example of what is to come
           (for arbitrary dimensions) in advection_demo1_1d.py; the preview of what is to come
           is in `automatic_schemes.py` (under development)
           
@author: Nikolai G. Lehtinen (Nikolai.Lehtinen@uib.no)
"""

#%% Preliminaries
__version_major__ = 0
__version_minor__ = 6
__version_revision__ = 0
__version_branch__ = ''
__version__ = str(__version_major__) + '.' + str(__version_minor__) + '.' + \
    str(__version_revision__) + __version_branch__
print('Welcome to TRUMP (Total Removal of Unwanted Matrix Programming), v.',__version__)

from . import common as co
from . import symbolic_arrays as sy
from . import extended_arrays as ea
from .common import set_global_debug_level, ndgrid
from .symbolic_arrays import set_sparse
from .geometries import Grid, Geometry, GeometryCartesian, GeometryCylindrical, \
    random_grid_delta, growing_grid_delta
from .extended_arrays import ExtendedArray, end, span, half,\
    extend_vectorized_function, minmax, common_index
from .extended_array_operations import c_diff, n_diff, c_mean, n_mean,\
    c_ups, n_ups, c_upc, n_upc, c_upn, n_upn,\
    n_interp_ups, c_interp_ups, n_interp_upc, c_interp_upc, n_interp_upn, c_interp_upn
from .fields import Field, Solver

#%% Context to override NumPy functions, for both symbolic array and extended array operations
class NumPyOverrideManyContexts:
    """A context for overriding NumPy functions with TRUMP package. It is just a wrapper
    for two contexted overrides for SymbolicArrays and ExtendedArrays, to make sure they
    are entered and exited in the correct order. The context may be used as usual 'with'
    block:
        
    >>> with numpy_override:
            # use NumPy functions
            f = np.hstack((f1,f2)) # symbolic array override
            y = np.sin(x)
    
    So far, without any exception handling etc.
    """
    num_instances = 0
    def __init__(self, *args):
        "Singleton"
        if self.__class__.num_instances > 0:
            raise TypeError('Cannot have more than one numpy override context')
        self.__class__.num_instances += 1
        self.contexts = args
    def __enter__(self):
        for context in self.contexts:
            context.__enter__()
    def enter(self):
        self.__enter__()
    def __exit__(self, type, value, traceback):
        for context in reversed(self.contexts):
            context.__exit__(type, value, traceback)
    def exit(self):
        self.__exit__(None, None, None)
    def complete_exit(self):
        for context in reversed(self.contexts):
            context.complete_exit()
    @property
    def entered(self):
        enter_levels = [c.enter_level for c in self.contexts]
        enter_level = min(enter_levels)
        assert max(enter_levels)==enter_level
        return enter_level>0
        
numpy_override = NumPyOverrideManyContexts(sy.SY_OVERRIDE, ea.EA_OVERRIDE)

#%% In the beginning of your code, do the following:
if False:
    numpy_override.enter()
    set_global_debug_level(1) # Default is set to 1
    set_sparse(True) # Default is set to True

#%% Happy coding!
def print_motto():
    print(co.red('=== '), 'Keep ', co.blue('finite'), '-', co.red('difference '),
          'coding ', co.blue('great! '), co.red('==='), sep='')
    
print_motto()

