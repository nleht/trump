#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 26 15:21:05 2020

Common notations:
    stencil -- a list of tuples (points), denoting the shifts from the central point, e.g.,
        a forward CIR stencil [(0,0), (1,0), (0,1)]
    ndim -- number of dimensions
    npoints -- number of points in a stencil
    degree -- the degree of a stencil polynomial
    dof -- degrees of freedom, number of independent polynomials of a given degree
        (also the number of distinct atomic polynomials)
    atom -- atomic polynomial, like x^2y^4z, represented by a tuple of length ndim, like (2,4,1)
    u_ea -- the discretized function u_i represented by an extended_array
    Emat -- matrix of values E^i_{lj} = A_l(r'_{i+s_j}) of size (dof, npoints) [+ nts]
    Cmat -- coefficient C^i_{kl} of size (npoints,dof) [+ nts]
        For a uniform grid, there is only one Emat, Cmat (size does not have nts)
        For nonuniform grids, nts is the size of the extended_array to which they are applied

@author: nle003
"""

import numpy as np
import itertools
if True:
    from . import common as CO
    from . import extended_arrays as EA
    from . import end, half, span, c_mean, n_mean, c_diff, n_diff, c_upc
else:
    from fidelior import common as CO
    from fidelior import extended_arrays as EA
    from fidelior import end, half, span, c_mean, n_mean, c_diff, n_diff, c_upc

#%% A simple class to keep track of directions
class quadrants:
    def __init__(self, ndim):
        self.n = 2**ndim
        self.qaddr = np.zeros((2,)*ndim, dtype=np.int)
        self.qaddr.flat[:] = np.arange(self.n)
    def num(self, q):
        return self.qaddr[q]
    def is_neg(self, kq):
        return np.unravel_index(kq, self.qaddr.shape)
    def sign(self, kq):
        return -2*np.array(self.is_neg(kq))+1
    
CO.make_indented_printable(quadrants, multiline=False)

#%% Operations on stencils; automatically-generated stencils
# We remind that a stencil is a list of tuples denoting the shifts from the central point

# Operation on stencils

def transformed_stencil(stencil, permute=None, scale=None, shift=None):
    """Permute, then scale, then shift."""
    s = np.array(stencil)
    if permute is not None:
        s = s[:,permute]
    if scale is not None:
        s = s * scale
    if shift is not None:
        s = s + shift
    # Back to a list of tuples
    return [tuple(s_) for s_ in s]

def stencil_on_Grid(stencil, grids):
    """Make stencil nonuniform, with new coordinates given by grids."""
    s = np.array(stencil)
    s -= np.min(s, axis=0)
    snew = s.copy()
    for axis, grid in enumerate(grids):
        snew[:,axis] = np.array(grid)[s[:,axis]]
    return [tuple(s_) for s_ in snew]

def boundary_thickness(stencil):
    return max(max(abs(s) for s in point) for point in stencil)

# Basic stencils

def odd_order_stencil(ndim, n, cover=0):
    """Automatically generate. Order is N=2n-1. If cover==1, then it is the union of all
    flipped stencils."""
    if ndim==1:
        s = [(Z,) for Z in range(-n, n+cover)]
        return s
    assert ndim==2 # sorry, ndim>2 not implemented
    s = []
    for Z in range(2*n+cover):
        Z1 = Z//2
        Z2 = Z-n-Z1
        for T in range(n+1-Z%2):
            s_ = (-Z1+T,-Z2-T)
            #print(Z, T, s_)
            s.append(s_)
    return s

def even_order_stencil(ndim, n):
    """A tail scheme. Order N=2*n.
    In 2D, for n>1 these are only useful as Warming-Beam schemes
    (flipped in both x and y and shifted by (-1,-1))
    because they are slightly unstable for small c. See demo_stability."""
    if ndim==1:
        s = [(Z,) for Z in range(-n,n+1)]
        return s
    assert ndim==2
    s = []
    for Z in range(2*n+2):
        Z1 = Z//2
        Z2 = Z-n-1-Z1
        for T in range(1-Z%2,n+1):
            s_ = (-Z1+T,-Z2-T)
            #print(Z, T, s_)
            s.append(s_)
    return s

# Derivative stencils

def odd_left_stencil(ndim, n):
    "Flip just one point"
    s = odd_order_stencil(ndim, n)
    s[s.index((1,-n+1))] = (1,n)
    return s

def odd_right_stencil(ndim, n):
    return transformed_stencil(odd_left_stencil(ndim, n), permute=(1,0))

def warming_beam_stencil(ndim, n):
    return transformed_stencil(even_order_stencil(ndim, n), scale=-1, shift=1)

def flipped_stencils(stencil, quad):
    stencils = []
    stencil_alldir = []
    for kq in range(quad.n):
        # Transform the stencil to the given quadrant
        stencil1 = transformed_stencil(stencil, scale=quad.sign(kq))
        stencil_alldir, _, _ = CO.list_union(stencil_alldir, stencil1)
        stencils.append(stencil1)
    return stencil_alldir, stencils

#%% The atomic polynomial operations

def apply_atom(atom, r):
    r"""Apply atomic polynomial `atom` to the radius-vector `r`.
    
    `r` is an iterable (preferably, a tuple) of length `ndim`, whose elements are
    numbers or of type ``extended_array``. The result is either a number or an
    ``extended_array``."""
    muls = [xi**p for xi, p in zip(r, atom)]
    prod = muls[0]
    if all(mul.shape==prod.shape for mul in muls):
        return np.prod([xi**p for xi, p in zip(r, atom)], axis=0)
    else:
        # Broadcast!
        for mul in muls[1:]:
            prod = prod * mul
        return prod

def atomic_polynomials(ndim, degree):
    "Generate all atomic polynomials of ndim variables of given degree"
    res = list(itertools.combinations(np.arange(ndim + degree), ndim))
    atoms = []
    for p in res:
        a = 0
        atom = ()
        for i in p:
            atom += (i - a,)
            a = i + 1
        atoms.append(atom)
    # Check if we have all of them
    dof = np.math.factorial(degree+ndim) // np.math.factorial(ndim) // np.math.factorial(degree)
    assert len(atoms)==dof
    return atoms

#%% The most important class: the stencil polynomial

class stencil_polynomial:
    def __init__(self, geom, f_indep, degree, stencil, quad=None):
        self._geom = geom
        ndim = geom.ndim
        self.dr = [EA._diff(gr.r_c, a) for a,gr in enumerate(geom.grids)]
        self.dvolume = np.prod(self.dr)
        #self.ndim = ndim
        assert f_indep.ncells==geom.ncells
        self.size = f_indep.nts
        self.uniform = np.array([gr.uniform for gr in geom.grids])
        self.size_short = tuple(np.array(self.size)[~self.uniform])
        self.size_real = tuple((self.size[a] if not self.uniform[a] else 1) for a in range(ndim))
        #self.size_real = self.size # for debugging
        #self.size_short = self.size # for debugging
        self.f_indep = f_indep
        self.degree = degree
        self.atoms = atomic_polynomials(ndim, degree)
        dof = len(self.atoms)
        self.stencil = stencil
        npoints = len(stencil)
        self.quad = quad
        self.Cshape_real = (npoints, dof) + self.size_real
        self.Cshape_short = (npoints, dof) + self.size_short
        self.Cshape_full = (npoints, dof) + self.size 
        if quad is not None:
            self.Cshape_real = (quad.n,) + self.Cshape_real
            self.Cshape_short = (quad.n,) + self.Cshape_short
            self.Cshape_full = (quad.n,) + self.Cshape_full
        self._Cmat = np.zeros(self.Cshape_short) # squeezed
        self.nus = tuple(np.max([point[a] for point in stencil]) for a in range(ndim))
        self.nls = tuple(-np.min([point[a] for point in stencil]) for a in range(ndim))
        self.nus_min = np.array(self.f_indep.nus) + np.array(self.nus)
        self.nls_min = np.array(self.f_indep.nls) + np.array(self.nls)
        # Set some values so that there is no error
        self._Cmat_s = None
        self._Cmat_u = None
        self._Cu_w = None
    # The next few function describe separate steps for finding the next value
    def _check_inputs(self, u, ws):
        assert isinstance(u, EA.extended_array) and u.ncells==self.f_indep.ncells and \
            u.stags==self.f_indep.stags
        # Extended checks for the size: u must have enough margin to update f_indep
        assert all(np.array(u.nus)>=self.nus_min)
        assert all(np.array(u.nls)>=self.nls_min)
        ndim = self._geom.ndim
        for w in ws:
            assert len(w)==ndim
            for wa in w:
                assert isinstance(wa, EA.extended_array) and EA.same_ea_structure(wa, self.f_indep)
    def _choose_stencil(self, w):
        "Select only C.shape = size + (npoints, dof) into self._Cmat_s"
        #self.check_inputs(u, (w,))
        ndim = self._geom.ndim
        Cmat_ = self._Cmat.reshape(self.Cshape_real) # unsqueeze (maybe not necessary)
        if self.quad is not None:
            q = tuple((wa < 0).arr.astype(np.int) for wa in w) # q[a].shape==size
            kq = self.quad.num(q) # kq.shape==size
            self._Cmat_s = CO.choose(kq, Cmat_, axis=0, which=range(3,3+ndim))
        else:
            self._Cmat_s = Cmat_
        # Now self._Cmat_s.shape = (npoints, dof) + size_real
    def _apply_u(self, u):
        "Apply self._Cmat_s to u, get self._Cmat_u of shape (dof,) + size"
        s = span(self.f_indep)
        self._Cmat_u = np.zeros((len(self.atoms),) + self.size)
        for k, shift in enumerate(self.stencil):
            us = u.shifted(shift)[s][None,:] # us.shape==(1,) + size
            C = self._Cmat_s[k] # C.shape==(dof,) + size_real
            self._Cmat_u += us*C # broadcasted, hopefully, to shape=(dof,)+size
    def interpolate(self, u, w):
        self._check_inputs(u, (w,))
        self._choose_stencil(w)
        self._apply_u(u) # produces self._Cmat_u.shape = (dof,) + size
        Ar = self._get_Ar(0, (w,)) # produces Ar.shape = (dof,) + size
        res = self.f_indep.copy()
        res.arr = np.sum(self._Cmat_u * Ar, axis=0)
        return res
    def integrate(self, u, w1, w2):
        self._check_inputs(u, (w1,w2))
        self._choose_stencil(w1)
        self._apply_u(u) # produces self._Cmat_u.shape = (dof,) + size
        Ar = self._get_Ar(1, (w1, w2)) # produces Ar.shape = (dof,) + size
        res = self.f_indep.copy()
        res.arr = np.sum(self._Cmat_u * Ar, axis=0)
        return res
    def _get_Ar(self, operation, ws):
        "This operation assumes that all ws have the same size (and stags)"
        #ndim = self._geom.ndim
        if operation==0:
            # Interpolation at w
            w, = ws
            Ar = np.array([apply_atom(atom, w).arr for atom in self.atoms])
        elif operation==1:
            # Integration within limits w1, w2
            w1, w2 = ws
            Ar = np.array([np.prod([(wa2.arr**(p+1)-wa1.arr**(p+1))/(p+1)
                for wa1, wa2, p in zip(w1, w2, atom)], axis=0)
                    for atom in self.atoms])
        elif operation==2:
            # Integration, divided by area
            w1, w2 = ws
            # Could be an even stricter formula in case wa1=wa2 for some points
            if False:
                Ar = np.array([np.prod([(wa2.arr**(p+1)-wa1.arr**(p+1))/(p+1)/(wa2.arr-wa1.arr)
                    for wa1, wa2, p in zip(w1, w2, atom)], axis=0)
                        for atom in self.atoms])
            else:
                Ar = []
                for atom in self.atoms:
                    prod = []
                    for wa1, wa2, p in zip(w1, w2, atom):
                        v1 = wa1.arr
                        v2 = wa2.arr
                        dv = v2-v1
                        #tmp = dv.copy()
                        i = (dv!=0)
                        dv[i] =  (v2[i]**(p+1)-v1[i]**(p+1))/(p+1)/dv[i]
                        #tmp = (v2**(p+1)-v1**(p+1))/(p+1)/dv
                        dv[~i] = v1[~i]**p
                        prod.append(dv)
                    Ar.append(np.prod(prod, axis=0))
                Ar = np.array(Ar)
        else:
            raise Exception('Unknown operation: '+str(operation))
        # Ar.shape == (dof,) + size_w
        return Ar
    def _fem_limits(self, w, axes):
        """We already have C*u=Cu=self._Cmat_u with Cu.shape = (dof,) + size.
        Below, i is half-integer index of w, and j is integer index of the cell I_j.
        We cycle over all geometrical axes, and `axis` is a boolean element of array `axes`.
        The following operations are performed on Cu:
        1. Cu is shifted upstream in several dimensions indicated by `axes`.
            If axis==True:
                take Cu_j, with j= (i+1/2, if w_i>0 else i-1/2), j is integer, for each i!
                Now we have Cu_i (stag=1 along the `axis`)
            If axes==False:
                There is no upstream shift!
            The resulting Cu_i has the same stags as all relevant (ie.axis=True) w
        2. Along each axis, the integration limits are:
            if axis==False:
                from x_{j-1/2}-x_j<0 
                to x_{j+1/2}-x_j>0
            if axis==True:
                from  s=x_{j-1/2}-x_j<0 (if w_i>0) or s=x_{j+1/2}-x_j>0, (if w_i<0)
                    (which may be generalized to s=x_i-x_j)
                to s+w (which must be inside cell I_j)
        This looks like the most tricky function in this class
        (the previous version was much worse!)
        """
        ndim = self._geom.ndim
        assert len(axes)==ndim
        wmod = []
        lolim = []
        hilim = []
        wa_shape = None
        for a in range(ndim):
            gr = self._geom.grids[a]
            rn = gr.r_n
            rc = gr.r_c
            if axes[a]:
                wa = w[a]
                for a1 in range(ndim):
                    if a1!=a and axes[a1]:
                        wamean = self._geom.c_aver(wa, a1)
                        #wa = wa.lo(a1).view[span(wamean)] # -- does not work!
                        #wa = fdo.c_upc(w[a], fdo.c_mean(w[a1], a), a1).view[span(wamean)]
                        # -- VERY slightly (~10%) less spreading, not worth the hassle!
                        wa = wamean
                wmod.append(wa)
                if wa_shape is None:
                    wa_shape=wa.copy()
                assert EA.same_ea_structure(wa, wa_shape)
                lo = rc - c_upc(rn, -wa, a)
                lolim.append(lo.broadcast(wa_shape))
                hilim.append(lo+wa)
            else:
                lolim.append(rc.lo(a) - rn)
                hilim.append(rc.hi(a) - rn)
        # Fix the shape of trivial limits
        if wa_shape is None:
            wa_shape = self.f_indep
        for a in range(ndim):
            if not axes[a]:
                lolim[a] = lolim[a].broadcast(wa_shape)
                hilim[a] = hilim[a].broadcast(wa_shape)
        # We already have the integration limits but do not have the upstream-shifted Cu_w
        Cu_w = 0
        Cu = self._Cmat_u
        order = sum(axes)
        assert len(wmod)==order
        quad = quadrants(order)
        for kq in range(quad.n):
            is_neg = quad.is_neg(kq) # tuple of ones and zeros
            tmp = tuple(wa.arr<0 if i else wa.arr>=0 for i,wa in zip(is_neg, wmod))
            select = np.logical_and.reduce(tmp)
            s = [slice(None)]
            ssel = (slice(-1) if i else slice(1, None) for i in is_neg) # shifted for relevant w
            s = (slice(None),) + tuple(next(ssel) if axis else slice(None) for axis in axes) # full index
            Cu_w += Cu[s]*select
        if quad.n==0:
            Cu_w = Cu
        self._Cu_w = Cu_w
        return tuple(lolim), tuple(hilim), wmod
    def _get_FG(self, operation, w, axes):
        """For the CENTERED polynomials only (h_i = r_i).
        We integrate in the limits given by w1, w2 which denote the diplacements of the lower
        left and upper right corners of the integration rectangle from the reference point h_i.
        kq indicates which quadrant to use."""
        assert operation in [1,2]
        w1, w2, wmod = self._fem_limits(w, axes)
        Ar = self._get_Ar(operation, (w1, w2))
        F = w1[0].copy()
        F.arr = np.sum(self._Cu_w * Ar, axis=0)
        #F = self._F_from_limits(w1, w2)
        return F, wmod
    def face_values(self, u, w):
        "Slightly faster? WARNING: Only 2D!"
        g = self._geom
        if not g.ndim in [1,2,3]:
            raise Exception('Sorry, the higher dimensions are not implemented yet!')
        self._choose_stencil(w)
        # -- same as self._Cmat_s = self._Cmat
        self._apply_u(u)
        # Calculate face values
        if g.ndim==1:
            fv, _ = self._get_FG(2, w, [True])
            return (fv,)
        elif g.ndim==2:
            Gx, _ = self._get_FG(2, w, [True, False])
            Gy, _ = self._get_FG(2, w, [False, True])
            Gxy, wmod = self._get_FG(2, w, [True, True])
            # Face values
            # fvx = Gx + 0.5*g.grad(wmod[1]*Gxy, 1)
            # fvy = Gy + 0.5*g.grad(wmod[0]*Gxy, 0)
            # Supposedly better results for u==1?
            # fvx = Gx + 0.5*n_mean(wmod[1],1)*g.grad(Gxy, 1)
            # fvy = Gy + 0.5*n_mean(wmod[0],0)*g.grad(Gxy, 0)
            # More accurate?
            fvx = Gx + 0.5*g.n_aver(wmod[1],1)*g.grad(Gxy, 1)
            fvy = Gy + 0.5*g.n_aver(wmod[0],0)*g.grad(Gxy, 0)            
            return (fvx, fvy)
        elif g.ndim==3:
            # Brute force for now
            Gx, _ = self._get_FG(2, w, [True, False, False])
            Gy, _ = self._get_FG(2, w, [False, True, False])
            Gz, _ = self._get_FG(2, w, [False, False, True])
            Gxy, wxy = self._get_FG(2, w, [True, True, False])
            Gyz, wyz = self._get_FG(2, w, [False, True, True])
            Gzx, wzx = self._get_FG(2, w, [True, False, True])
            Gxyz, _ = self._get_FG(2, w, [True, True, True])
            w1a = g.n_aver(wxy[1],1) # to <c,n,n> grid
            w2a = g.n_aver(wzx[2],2) # to <c,n,n> grid
            fvx = Gx + 0.5*w1a*g.grad(Gxy, 1) + 0.5*w2a*g.grad(Gzx, 2) \
                + (1./3.)*w1a*w2a*g.grad(g.grad(Gxyz, 1), 2)
            w2a = g.n_aver(wyz[2],2) # to <n,c,n> grid
            w0a = g.n_aver(wxy[0],0) # to <n,c,n> grid
            fvy = Gy + 0.5*w2a*g.grad(Gyz, 2) + 0.5*w0a*g.grad(Gxy, 0) \
                + (1./3.)*w2a*w0a*g.grad(g.grad(Gxyz, 2), 0)
            w0a = g.n_aver(wzx[0],0) # to <n,n,c> grid
            w1a = g.n_aver(wyz[1],1) # to <n,n,c> grid
            fvz = Gy + 0.5*w0a*g.grad(Gzx, 0) + 0.5*w1a*g.grad(Gyz, 1) \
                + (1./3.)*w0a*w1a*g.grad(g.grad(Gxyz, 0), 1)
            return (fvx, fvy, fvz)
    def fem_advection_step(self, u, w):
        # # Old algorithm, with too much spreading
        if False:
            #F, _ = self._get_F(w, [False, False])
            Fx, _ = self._get_FG(1, w, [True, False])
            Fy, _ = self._get_FG(1, w, [False, True])
            Fxy, wmod = self._get_FG(1, w, [True, True])
            du = n_diff(Fx, 0) + n_diff(Fy, 1) + n_diff(n_diff(Fxy, 0), 1)
            du /= self.dvolume
        # Use the face value approach
        fv = self.face_values(u, w)
        du = self._geom.divergence([wa*fva for wa, fva in zip(w, fv)])
        return du
        
CO.make_indented_printable(stencil_polynomial, replace_repr=True)

#%% Interpolation scheme setup
def interpolation_scheme_sp(geom, f_indep, degree, stencil, flip_stencil=True):
    r"""Calculate the stencil polynomial coefficient C for a non-uniform grid and all directions.
    Result: Cmat; Cmat.shape = (2**ndim,) + size_short + (npoints, dof)
    where size_short is the size of f_indep with uniform grid directions collapsed"""
    # 1. Get the all-direcitonal stencil by flipping and taking a union
    if flip_stencil:
        quad = quadrants(geom.ndim)
        stencil_alldir, stencils = flipped_stencils(stencil, quad)
    else:
        quad = None
        stencils = [stencil]
        stencil_alldir = stencil
    num_stencils = len(stencils)
    sp = stencil_polynomial(geom, f_indep, degree, stencil_alldir, quad)
    dof = len(sp.atoms)
    npoints1 = len(stencil)
    assert npoints1==dof # matrix must be invertible
    Emat_tmp = np.zeros((num_stencils, dof, npoints1) + sp.size_real)
    for kq in range(num_stencils):
        # Transform the stencil to the given quadrant
        stencil1 = stencils[kq] #transformed_stencil(stencil, scale=sp.quad.scale(kq))
        rk = []
        for a, gr in enumerate(geom.grids):
            # size is only different from 1 at axis a
            size=np.ones((geom.ndim,), dtype=np.int)
            if gr.uniform:
                rk1 = np.array([gr.delta*point[a] for point in stencil1])
                size[a] = 1
            else:
                rk1 = np.array([(gr.r_n.shifted(point) - gr.r_n)[span(f_indep,a)].flatten()
                       for point in stencil1])
                size[a] = f_indep.nts[a]
            rk.append(rk1.reshape((dof,)+tuple(size)))
        # rk.shape == (ndim, npoints1) + size, the first dimension is NOT array dimension
        Emat1 = np.array([apply_atom(atom, rk) for atom in sp.atoms])
        # Emat1.shape == (dof, npoints1) + size_real
        #   where size_real is the shape to which all 'size' are broadcast
        Emat_tmp[kq] = Emat1
    npoints = len(stencil_alldir)
    Emat_tmp = np.array(Emat_tmp) # shape == (num_stencils,) + (dof, npoints1) + size_real 
    # Invert in 1,2 dimensions -- that's what all transposes are about
    Cmat_tmp = CO.invert_many_matrices(Emat_tmp, axes=(1,2), alg=1)
    # Cmat_tmp.shape = (num_stencils,) + (npoints1, dof) + size_real 
    if flip_stencil:
        # Pad with zeros
        Cmat = np.zeros((num_stencils,) + (npoints, dof) + sp.size_real)
        for kq in range(num_stencils):
            #stencil1 = stencils[kq] # transformed_stencil(stencil, scale=sp.quad.scale(kq))
            Cmat[kq, CO.list_where(stencils[kq], sp.stencil),:] = Cmat_tmp[kq, :]
    else:
        Cmat = Cmat_tmp
    if not flip_stencil:
        Cmat = Cmat[0]
    assert sp.Cshape_real==Cmat.shape
    sp._Cmat = Cmat.reshape(sp.Cshape_short)
    return sp

#%% FEM scheme setup
def _reduced_ea(f_indep, uniform):
    stags = tuple(None if unif else v for v, unif in zip (f_indep.stags, uniform))
    nls = tuple(None if unif else v for v, unif in zip (f_indep.nls, uniform))
    nus = tuple(None if unif else v for v, unif in zip (f_indep.nus, uniform))
    return EA.ExtendedArray(f_indep.ncells, stags=stags, nls=nls, nus=nus)

def _full_ea(ea, val):
    tmp = ea.copy()
    tmp.arr = np.full(tmp.nts_real, fill_value=val)
    return tmp

def fem_scheme_sp(geom, f_indep, degree, stencil):
    # We need different polynomial for different directions!
    sp = stencil_polynomial(geom, f_indep, degree, stencil) # LOCAL stencil_polynomial!!!
    dof = len(sp.atoms)
    npoints = len(stencil)
    assert npoints==dof # matrix must be invertible
    Ebar_tmp = np.zeros((dof, npoints) + sp.size_real)
    reduced_f = _reduced_ea(f_indep, sp.uniform)
    for kp, point in enumerate(stencil):
        pointl = tuple(p-half for p in point)
        pointh = tuple(p+half for p in point)
        w1 = []
        w2 = []
        for a, gr in enumerate(geom.grids):
            rc = gr.r_c
            r = gr.r_n
            if sp.uniform[a]:
                w1.append(_full_ea(reduced_f, gr.delta*float(pointl[a])))
                w2.append(_full_ea(reduced_f, gr.delta*float(pointh[a])))
            else:
                w1.append((rc.shifted(pointl) - r).broadcast(reduced_f))
                w2.append((rc.shifted(pointh) - r).broadcast(reduced_f))
        Ar = sp._get_Ar(2, (w1, w2)).reshape((dof,)+sp.size_real)
        Ebar_tmp[:, kp, :] = Ar
    # Invert in (0,1) axes
    Cmat = CO.invert_many_matrices(Ebar_tmp, alg=1) # shape = (npoints, dof) + size_real
    assert sp.Cshape_real==Cmat.shape
    sp._Cmat = Cmat.reshape(sp.Cshape_short)
    #sp._r_w = None # Restore the old value
    return sp
