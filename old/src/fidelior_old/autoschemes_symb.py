#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 26 15:21:05 2020

UNDER CONSTRUCTION, DO NOT USE!

@author: nle003
"""

import numpy as np
import matplotlib.pyplot as plt
import itertools
#from fractions import Fraction
#import sympy
from sympy import Matrix, Integer, Rational # for inverting fractional matrices
#import trump.common as CO
from . import common as CO
from .extended_arrays import ExtendedArray

#%% Some stupid stuff
def _as_fraction(coef, frac_try):
    if frac_try is None:
        return False, None, None
    is_fraction = False
    for k in range(frac_try):
        denom = np.math.factorial(k)
        numer = np.round(coef*denom).astype(np.int)
        if np.allclose(numer, coef*denom):
            is_fraction=True
            break
    if not is_fraction:
        numer = None
        denom = None
    return is_fraction, numer, denom

# def _as_Fraction(matrix, frac_try):
#     is_fraction, numer, denom = _as_fraction(matrix, frac_try)
#     if not is_fraction:
#         return False, matrix
#     else:
#         tmp = np.ndarray(numer.shape, dtype=np.dtype('O'))
#         for i in range(tmp.size):
#             tmp.flat[i] = Fraction(numer.flat[i], denom)
#         return True, Matrix(tmp)
    
# Need Gaussian elimination routine or something for arbitrary objects (fractions, extended_arrays)

#%% Some polynomial output routines
def _str_power(v, p):
    return '' if p==0 else v + ('^'+str(p) if p>1 else '')

def _str_atom(atom, do_one=True):
    vnames = 'xyzt'
    s = ''.join(_str_power(v, p) for v, p in zip(vnames, atom))
    if s=='' and do_one:
        s='1'
    return s

def _str_posfrac(n, d, do_one=True, do_paren=False):
    "Output positive fraction n/d"
    assert n>0
    t = np.math.gcd(n, d)
    n //= t
    d //= t
    d_str = '/' + str(d) if d>1 else ''
    n_str = '' if (d_str=='' and n==1 and not do_one) else str(n)
    f_str = n_str + d_str
    if d_str != '' and do_paren:
        f_str = '(' + f_str + ')'
    return f_str

def _str_frac_atom(n, d, atom, do_sign=True, sep=''):
    "Signed fraction times atomic polynomial"
    if n==0:
        return ''
    s_str = '-' if n<0 else ('+' if do_sign else '')
    if s_str != '':
        s_str += sep
    pf_str = _str_posfrac(abs(n), d, do_one=False, do_paren=True)
    a_str = _str_atom(atom, do_one=(pf_str==''))
    return s_str + pf_str + a_str

def _str_float_atom(f, atom, do_sign=True, sep=''):
    "Signed float times atomic polynomial"
    if f==0:
        return ''
    s_str = '-' if f<0 else ('+' if do_sign else '')
    if s_str != '':
        s_str += sep
    fa = abs(f)
    is_one = np.allclose(np.double(fa), 1.)
    if isinstance(fa, Rational):
        if not isinstance(fa, Integer):
            fa_format = '(' + repr(fa) + ')'
        else:
            fa_format = repr(fa)
    else:
        fa_format = '{:g}'.format(fa)
    fa_str = '' if is_one else fa_format
    a_str = _str_atom(atom, do_one=(fa_str==''))
    return s_str + fa_str + a_str

def _str_frac_poly(numers, denom, atoms, sep=''):
    "Polynomial with fractional coefficients, with a common denominator"
    fracpoly_str = ''
    for numer, atom in zip(numers, atoms):
        term_str = _str_frac_atom(numer, denom, atom, do_sign=(fracpoly_str!=''), sep=sep)
        if term_str!='':
            fracpoly_str += sep + term_str
    return fracpoly_str


def _str_float_poly(coefs, atoms, sep=''):
    floatpoly_str = ''
    for coef, atom in zip(coefs, atoms):
        term_str = _str_float_atom(coef, atom, do_sign=(floatpoly_str!=''), sep=sep)
        if term_str!='':
            floatpoly_str += sep + term_str
    return floatpoly_str

def _str_stencil_poly(coef, atoms, stencil, frac_try=7, sep='', skip_zeros=True):
    is_fraction, numer, denom = _as_fraction(coef, frac_try)
    stencil_str = ''
    for i, shift in enumerate(stencil):
        if is_fraction:
            poly_str = _str_frac_poly(numer[i], denom, atoms, sep=sep)
        else:
            poly_str = _str_float_poly(coef[i,:], atoms, sep=sep)
        if poly_str=='':
            if skip_zeros:
                continue
            else:
                poly_str='0'
        stencil_str += ('' if stencil_str=='' else '+') + ' U' + repr(shift) + \
            ' * [' + poly_str + sep + '] '
    if stencil_str=='':
        stencil_str='0' # so that we do not return an empty string if there is nothing
    return stencil_str

#%% AutoScheme class
def apply_atom(atom, r):
    "Apply atomic polynomial to radius-vector r"
    return np.prod([xi**p for xi, p in zip(r, atom)])

def atomic_polynomials(ndim, order):
    "Generate all atomic polynomials of ndim variables of given order"
    res = list(itertools.combinations(np.arange(ndim + order), ndim))
    atoms = []
    for p in res:
        a = 0
        atom = ()
        for i in p:
            atom += (i - a,)
            a = i + 1
        atoms.append(atom)
    dof = np.math.factorial(order+ndim) // np.math.factorial(ndim) // np.math.factorial(order)
    assert len(atoms)==dof
    return atoms

class StencilPolynomial:
    """stencil_polynomial(ndim, order, stencil, coef=None)
    An arbitrary polynomial of r and stencil values with provided coefficients"""
    def __init__(self, ndim, order, stencil, is_symbolic=False):
        self.ndim = ndim
        self.order = order
        self.stencil = stencil
        self.atoms = None
        self.coefmat_ = None
        self.is_symbolic = is_symbolic
        if not is_symbolic:
            self.frac_try = 7 # To output coefs in terms of fractions
        self.print_ready = False
    @property
    def coefmat(self):
        return self.coefmat_
    @coefmat.setter
    def coefmat(self, v):
        if self.is_symbolic:
            assert isinstance(v, Matrix)
        assert v.shape==(self.npoints, self.dof)
        self.coefmat_ = v
        self.print_ready = True
    def default(self):
        self.atoms = atomic_polynomials(self.ndim, self.order)
        if self.is_symbolic:
            self.coefmat_ = Matrix(np.zeros((self.npoints,self.dof), dtype=np.int64))
        else:
            self.coefmat_ = np.zeros((self.npoints,self.dof))
        self.print_ready = True
    def copy(self):
        res = StencilPolynomial(self.ndim, self.order, self.stencil, is_symbolic=self.is_symbolic)
        res.atoms = self.atoms
        return res
    def index(self, i):
        point, poly = i
        if poly==slice(None):
            ipoly = slice(None)
        else:
            ipoly = self.atoms.index(poly)
        return self.stencil.index(point), ipoly
    def __getitem__(self, i):
        return self.coefmat_[self.index(i)]
    def __setitem__(self, i, v):
        if self.is_symbolic:
            assert isinstance(v, Matrix)
        self.coefmat_[self.index(i)] = v
    @property
    def dof(self):
        return len(self.atoms)
    @property
    def npoints(self):
        return len(self.stencil)
    def frac(self, new_frac_try):
        if not self.is_symbolic:
            self.frac_try=new_frac_try
        return self
    def _poly_operation(self, coef, operation, axis, atomds, arg=None):
        dofd = len(atomds)
        if self.is_symbolic:
            coefd = Matrix(np.zeros((dofd,),dtype=np.int64))
        else:
            coefd = np.zeros((dofd,))
        for i in range(self.dof):
            atom = self.atoms[i]
            if operation=='deriv':
                if atom[axis]==0:
                    continue
                atomd = tuple(p-1 if a==axis else p for a,p in enumerate(atom))
                # tmp = atom[axis]*coef[i]
                # print('orig =',tmp.__class__.__name__,'(',repr(tmp),')')
                # was = coefd[atomds.index(atomd)]
                # print('was =', was.__class__.__name__,'(',was,')')
                # coefd[atomds.index(atomd)] = was + tmp
                # print('assigned =',coefd[atomds.index(atomd)].__class__.__name__)
                coefd[atomds.index(atomd)] += atom[axis]*coef[i]
            elif operation=='times':
                atomd = tuple(p+1 if a==axis else p for a,p in enumerate(atom))
                coefd[atomds.index(atomd)] += coef[i]
            elif operation=='partial':
                atomd = tuple(0 if a==axis else p for a,p in enumerate(atom))
                coefd[atomds.index(atomd)] += arg**atom[axis]*coef[i]
            elif operation=='raise':
                coefd[atomds.index(atom)] += coef[i]
            elif operation=='divide':
                if atom[axis]==0:
                    assert np.allclose(coef[i],0)
                    continue
                atomd = tuple(p-1 if a==axis else p for a,p in enumerate(atom))
                coefd[atomds.index(atomd)] += coef[i]
            else:
                raise SyntaxError('Unknown operation: ' + str(operation))
        return coefd
    def _operation(self, operation, axis, arg=None):
        assert self.atoms is not None
        assert self.coefmat.shape == (len(self.stencil), self.dof)
        if operation=='deriv':
            orderd = self.order - 1
        elif operation=='times':
            orderd = self.order + 1
        elif operation=='divide':
            orderd = self.order - 1
        elif operation=='partial':
            orderd = self.order
        elif operation=='raise':
            orderd = self.order + 1
        else:
            raise SyntaxError('Unknown operation: '+operation)
        atomds = atomic_polynomials(self.ndim, orderd)
        coefmatd = []
        for p in range(self.npoints):
            coef = self.coefmat_[p,:]
            coefd = self._poly_operation(coef, operation, axis, atomds, arg=arg)
            coefmatd.append(coefd)
        res = StencilPolynomial(self.ndim, orderd, self.stencil, is_symbolic=self.is_symbolic)
        res.atoms = atomds
        #print(coefmatd)
        if self.is_symbolic:
            res.coefmat = Matrix([coefmatd]).T
        else:
            res.coefmat = np.array(coefmatd)
        return res
    def deriv(self, axis):
        return self._operation('deriv', axis)
    def times(self, axis):
        return self._operation('times', axis)
    def partial(self, axis, x):
        return self._operation('partial', axis, x)
    def raise_order(self):
        return self._operation('raise', None)
    def divide(self, axis):
        return self._operation('divide', axis)
    def print(self, sep=''):
        frac_try = (None if self.is_symbolic else self.frac_try)
        return _str_stencil_poly(self.coefmat, self.atoms, self.stencil, sep=sep, frac_try=frac_try)
    def __neg__(self):
        res = self.copy()
        res.coefmat_ = -self.coefmat_
        return res
    def __add__(self, si):
        if isinstance(si, StencilPolynomial):
            assert self.is_symbolic == si.is_symbolic
            assert self.order == si.order
            #assert self.stencil==si.stencil
            stencil, addr1, addr2 = CO.list_union(self.stencil, si.stencil)
            res = StencilPolynomial(self.ndim, self.order, stencil, is_symbolic=self.is_symbolic)
            res.atoms = self.atoms
            if res.is_symbolic:
                res.coefmat_ = Matrix(np.zeros((res.npoints, self.dof),dtype=np.int64))
                i1 = list(addr1)
                i2 = list(addr2)
                tmp = res.coefmat_[i1,:] + self.coefmat
                #print(tmp)
                for k,i_ in enumerate(i1):
                    res.coefmat_[i_,:] = tmp[k,:]
                #res.coefmat_[i1,:] = tmp
                tmp = res.coefmat_[i1,:] + si.coefmat
                for k,i_ in enumerate(i2):
                    res.coefmat_[i_,:] = tmp[k,:]                
                #res.coefmat_[i2,:] += si.coefmat
            else:
                res.coefmat_ = np.zeros((res.npoints,self.dof))
                res.coefmat_[addr1,:] += self.coefmat_[:,:]
                res.coefmat_[addr2,:] += si.coefmat_[:,:]
        else:
            res = self.copy()
            res.coefmat_ = self.coefmat_ + si
        return res
    def __radd__(self, si):
        return self + si
    def __sub__(self, si):
        return self + (-si)
    def __rsub__(self, si):
        return -self + si
    def __mul__(self, x):
        res = self.copy()
        res.coefmat_ = self.coefmat_*x
        return res
    def __rmul__(self, x):
        return self*x
    def __truediv__(self, x):
        return self*(1/x)
    def lap(self):
        res = None
        for axis in range(self.ndim):
            if res is None:
                res = self.deriv(axis).deriv(axis)
            else:
                res = res + self.deriv(axis).deriv(axis)
        return res
    def interpolate(self, ea, r):
        # r is (ndim,)
        atoms_r = np.array([apply_atom(atom, r) for atom in self.atoms])
        # atoms_r is (dof,)
        stencil_coefs = self.coefmat @ atoms_r
        # coefmat is (npoints, dof) -> stencil_coefs is (npoints,)
        res = 0
        for shift, coef in zip(self.stencil, stencil_coefs):
            res = res + ea.shifted(shift)*coef
        return res
    def __str__(self):
        return self._indented_repr()
    def __repr__(self):
        if self.print_ready:
            return self.print(sep=' ')
        else:
            return self.__str__()

CO.make_indented_printable(StencilPolynomial, replace_repr=False)

def point_atom_coefficients(geom, atoms, stencil, is_symbolic):
    dof = len(atoms)
    assert len(stencil)==dof
    stencil_a = np.array(stencil) # shape = (npoints, ndim)
    if is_symbolic:
        assert np.allclose(stencil_a, stencil_a.astype(np.int64))
    assert all([gr.uniform for gr in geom.grids])
    dr = np.array([gr.delta for gr in geom.grids])
    if is_symbolic:
        assert np.allclose(dr, dr.astype(np.int64))
    r = (dr[None,:]*stencil_a).T # Rely on broadcasting, shape = (ndim, npoints)
    alpha = np.array([np.prod([xi**p for xi, p in zip(r, atom)], axis=0) for atom in atoms])
    # alpha.shape == (dof, npoints) == (dof, dof)
    if is_symbolic:
        assert np.allclose(r, r.astype(np.int64))
        c =  Matrix(alpha.astype(np.int64)).inv()
    else:
        c = np.linalg.inv(alpha)
    return c

class InterpolatingScheme(StencilPolynomial):
    """Interpolating scheme"""
    def __init__(self, geom, order, stencil, is_symbolic=False):
        super().__init__(geom.ndim, order, stencil, is_symbolic=is_symbolic)
        self._geom = geom
        assert all([gr.uniform for gr in geom.grids]) # Nonunform is below
        self.atoms = atomic_polynomials(self.ndim, self.order)
        # Calculate the polynomials
        self.coefmat = point_atom_coefficients(geom, self.atoms, stencil, self.is_symbolic)

class CombinationScheme(StencilPolynomial):
    "Somewhat similar to composite_scheme in advection2d_nonuniform.py"
    def __init__(self, geom, order, stencil_list, coef_list):
        super().__init__(geom.ndim, order, [], is_symbolic=False)
        # self.stencil is set to []
        self._geom = geom
        assert all([gr.uniform for gr in geom.grids]) # Nonunform is below
        self.atoms = atomic_polynomials(self.ndim, self.order)
        self.stencil_list = stencil_list
        self.coef_list = coef_list
        dof = len(self.atoms)
        self.coefmat_ = 0
        for stencil, coef in zip(stencil_list, coef_list):
            # The addition with a coefficient
            self.stencil, addr1, addr2 = CO.list_union(self.stencil, stencil)
            coefmat = point_atom_coefficients(geom, self.atoms, stencil, is_symbolic=False)
            coefmat_ = np.zeros((len(self.stencil), dof))
            coefmat_[addr1,:] += self.coefmat_
            coefmat_[addr2,:] += coef*coefmat
            self.coefmat_ = coefmat_

#%% Some operations on stencils and schemes

def transformed_stencil(stencil, permute=None, scale=None, shift=None):
    """Permute, then scale, then shift."""
    s = np.array(stencil)
    if permute is not None:
        s = s[:,permute]
    if scale is not None:
        s = s * scale
    if shift is not None:
        s = s + shift
    # Back to a list of tuples
    return [tuple(s_) for s_ in s]

def transformed_InterpolatingScheme(scheme, permute=None, scale=None, shift=None):
    """Permute, then scale, then shift. For example, rotation by 90 degrees
    is with permute=(0,1) and scale=(-1,1). Permute is only necessary for non-symmetric schemes."""
    return InterpolatingScheme(scheme._geom, scheme.order, transformed_stencil(
            scheme.stencil, permute=permute, scale=scale, shift=shift),
            is_symbolic=scheme.is_symbolic)

def transformed_CombinationScheme(scheme, permute=None, scale=None, shift=None):
    """See transformed_interpolating_scheme"""
    transformed_stencils = [transformed_stencil(stencil, permute=permute, scale=scale, shift=shift)
                            for stencil in scheme.stencil_list]
    return CombinationScheme(scheme._geom, scheme.order, transformed_stencils, scheme.coef_list)

def transformed_scheme(scheme, permute=None, scale=None, shift=None):
    if isinstance(scheme, CombinationScheme):
        return transformed_CombinationScheme(scheme, permute=permute, scale=scale, shift=shift)
    elif isinstance(scheme, InterpolatingScheme):
        return transformed_InterpolatingScheme(scheme, permute=permute, scale=scale, shift=shift)
    else:
        raise Exception('Unknown scheme type')

def stencil_on_Grid(stencil, grids):
    """Make stencil nonuniform, with new coordinates given by grids."""
    s = np.array(stencil)
    s -= np.min(s, axis=0)
    snew = s.copy()
    for axis, grid in enumerate(grids):
        snew[:,axis] = np.array(grid)[s[:,axis]]
    return [tuple(s_) for s_ in snew]

def get_boundary_thickness(scheme):
    return max(max(abs(s) for s in point) for point in scheme.stencil)

#%% Automatically-generated stencils and schemes for advection

# Basic stencils

def odd_order_stencil(ndim, n, cover=0):
    """Automatically generate. Order is N=2n-1. If cover==1, then it is the union of all
    flipped stencils."""
    if ndim==1:
        s = [(Z,) for Z in range(-n, n+cover)]
        return s
    assert ndim==2 # sorry, ndim>2 not implemented
    s = []
    for Z in range(2*n+cover):
        Z1 = Z//2
        Z2 = Z-n-Z1
        for T in range(n+1-Z%2):
            s_ = (Z1-T,Z2+T)
            #print(Z, T, s_)
            s.append(s_)
    return s

def even_order_stencil(ndim, n):
    """A tail scheme. Order N=2*n.
    In 2D, for n>1 these are only useful as Warming-Beam schemes
    (flipped in both x and y and shifted by (-1,-1))
    because they are slightly unstable for small c. See demo_stability."""
    if ndim==1:
        s = [(Z,) for Z in range(-n,n+1)]
        return s
    assert ndim==2
    s = []
    for Z in range(2*n+2):
        Z1 = Z//2
        Z2 = Z-n-1-Z1
        for T in range(1-Z%2,n+1):
            s_ = (Z1-T,Z2+T)
            #print(Z, T, s_)
            s.append(s_)
    return s

# Derivative stencils

def odd_right_stencil(ndim, n):
    "Flip just one point"
    s = odd_order_stencil(ndim, n)
    s[s.index((-1,n-1))] = (-1,-n)
    return s

def odd_left_stencil(ndim, n):
    return transformed_stencil(odd_right_stencil(ndim, n), permute=(1,0))

def warming_beam_stencil(ndim, n):
    return transformed_stencil(even_order_stencil(ndim, n), scale=-1, shift=-1)

# The most stable schemes: these are combination schemes.

def fromm_scheme(geom, n):
    "Scheme of order 2*n, very stable (0<ci<1)"
    return CombinationScheme(geom, 2*n,
        [even_order_stencil(geom.ndim, n), warming_beam_stencil(geom.ndim, n)], [0.5, 0.5])
    # (even_order_interpolating_scheme(geom, n) + warming_beam_scheme(geom, n))/2

def xsym_scheme(geom, n):
    """Analog of Fromm scheme of order 2*n-1, very stable (0<ci<1), except for n==1"""
    return CombinationScheme(geom, 2*n-1,
        [odd_right_stencil(geom.ndim, n), odd_left_stencil(geom.ndim, n)], [0.5, 0.5])
    #  rr = odd_right_scheme(geom, n)
    # (rr + AS.transformed_interpolating_scheme(rr, permute=(1,0)))/2

def most_stable_scheme(geom, order):
    """Scheme stable for 0<ci<1 of arbitrary order (except for order==1)"""
    assert geom.ndim==2
    if order%2==0:
        n = order//2
        return fromm_scheme(geom, n)
    else:
        n = (order+1)//2
        if n>1:
            return xsym_scheme(geom, n)
        else:
            # There is no such scheme in 1st order, but return the second best, namely CIR
            # but as a combination
            # return combination_scheme(geom, order, [odd_order_stencil(geom.ndim, 1)], [1.])
            return InterpolatingScheme(geom, order, odd_order_stencil(geom.ndim, 1))

#%% Scheme for rotation: all quadrants are taken into account
class all_directions_scheme:
    """Now takes an arbitrary scheme which is valid for vi>=0"""
    def __init__(self, scheme0):
        """scheme0 may be an interpolation or combination scheme"""
        ndim = scheme0.ndim
        self.ndim = ndim
        self.qaddr = np.zeros((2,)*ndim, dtype=np.int)
        self.qaddr.flat[:] = np.arange(2**ndim)
        order = scheme0.order
        # Count the number of points in the combined stencil
        stencil0 = scheme0.stencil
        self.stencil = []
        for kq in range(2**ndim):
            q = np.unravel_index(kq, self.qaddr.shape)
            # Transform the stencil to the given quadrant
            #stencil = [tuple(-s if q[a]==1 else s for a,s in enumerate(point)) for point in stencil0]
            stencil = transformed_stencil(stencil0, scale=2*np.array(q)-1)
            self.stencil, _, _ = CO.list_union(self.stencil, stencil)
        self.order = order
        self.atoms = atomic_polynomials(ndim, order)
        assert self.atoms == scheme0.atoms
        dof = len(self.atoms)
        npoints = len(self.stencil)
        # Now, we need coefficients for different quadrants!
        self.coefmats = np.zeros((npoints, dof, 2**ndim))
        for kq in range(2**ndim):
            q = np.unravel_index(kq, self.qaddr.shape)
            scheme = transformed_scheme(scheme0, scale=2*np.array(q)-1)
            coefmat = np.zeros((npoints, dof))
            coefmat[CO.list_where(scheme.stencil, self.stencil),:] = scheme.coefmat_[:,:]
            self.coefmats[:,:,kq] = coefmat
    def interpolate(self, ea, r):
        "The new, optimized version"
        q = tuple((x < 0).arr.astype(np.int) for x in r) # q[a].shape==nts
        kq = self.qaddr[q] # kq.shape==nts
        coef = self.coefmats[:,:,kq] # coef.shape == (npoints, dof) + nts
        # atoms_r.shape == (dof,) + nts
        atoms_r = np.array([apply_atom(atom, r).arr for atom in self.atoms])
        # Must sum coef*atoms_r over dof to get an array of shape npoints x nts
        npoints = len(self.stencil)
        tmp = np.sum(np.tile(atoms_r,[npoints] + [1]*(self.ndim+1))*coef, axis=1) # npoints x nts
        coef_ea = r[0].copy()
        res = 0
        for i, shift in enumerate(self.stencil):
            coef_ea.arr = tmp[i]
            res = res + ea.shifted(shift)*coef_ea
        return res
    def __str__(self):
        return self._indented_repr()
    #def __repr__(self):
    #    return self.print(sep=' ')

CO.make_indented_printable(all_directions_scheme, replace_repr=False)

###################################################################################################
#%% Nonuniform grid
def point_atom_coefficients_nonuniform(geom, atoms, stencil, f_indep):
    dof = len(atoms)
    assert len(stencil)==dof
    r = np.array([[(gr.r_n.shifted(point) - gr.r_n).broadcast(f_indep).arr
                   for point in stencil] for gr in geom.grids])
    # r.shape == (ndim, npoints) + nts
    alpha = np.array([np.prod([xi**p for xi, p in zip(r, atom)], axis=0) for atom in atoms])
    # alpha.shape == (dof, npoints) + nts == (dof, dof) + nts
    # Can choose alg=0 (invert one-by-one), alg=1 (fully vectorized GE) or alg=2 (partially
    # vectorized GE). GE is Gaussian Elimination (with pivoting)
    c = CO.invert_many_matrices(alpha, alg=1) # we will change the alg
    return c

class stencil_polynomial_nonuniform:
    """stencil_polynomial(ndim, order, stencil, coef=None)
    An arbitrary polynomial of r and stencil values with provided coefficients"""
    def __init__(self, ndim, order, stencil, f_indep):
        self.ndim = ndim
        self.order = order
        self.stencil = stencil
        self.atoms = None
        self.f_indep = f_indep
        self._coefmat = None # this is going to be a matrix of extended_arrays
    def interpolate(self, ea, r):
        npoints = len(self.stencil)
        dof = len(self.atoms)
        atoms_r = np.array([apply_atom(atom, r) for atom in self.atoms]) # shape == (dof,)
        #print(atoms_r.shape)
        assert self._coefmat.shape==(npoints, dof)+self.f_indep.nts
        atoms_r = np.reshape(atoms_r, (dof,) + (1,)*self.ndim)
        atoms_r = np.tile(atoms_r, (1,) + self.f_indep.nts) # shape = (dof,) + nts
        #print(atoms_r.shape)
        # self._coefmat.shape == (npoints, dof) + nts
        # Same as in rotation case: sum over atoms to get array of shape npoints x nts
        tmp = np.sum(np.tile(atoms_r,[npoints] + [1]*(self.ndim+1))*self._coefmat, axis=1)
        # -- shape == (npoints,) + nts
        coef_ea = self.f_indep.copy()
        res = 0
        for i, shift in enumerate(self.stencil):
            coef_ea.arr = tmp[i]
            res = res + ea.shifted(shift)*coef_ea
        return res

CO.make_indented_printable(stencil_polynomial_nonuniform)

class interpolating_scheme_nonuniform(stencil_polynomial_nonuniform):
    def __init__(self, geom, order, stencil, f_indep, fill=True):
        super().__init__(geom.ndim, order, stencil, f_indep)
        assert geom.ndim==self.ndim
        self._geom = geom
        self.atoms = atomic_polynomials(self.ndim, self.order)
        # Calculate the polynomials
        if fill:
            self._coefmat = point_atom_coefficients_nonuniform(geom, self.atoms, stencil, f_indep)
        # shape == (npoints,dof) + nts
        # Next, same as in rotation case

#%% Plotting and analysis routines
def plot_stencil(stencil):
    plt.plot(0,0,'ro',markersize=10)
    plt.plot(*list(zip(*stencil)),'ko')
    plt.gca().set_aspect('equal')

def plot_scheme(scheme, rotation=None, sep=''):
    "Some code is from _str_stencil_poly"
    frac_try = (None if scheme.is_symbolic else scheme.frac_try)
    is_fraction, numer, denom = _as_fraction(scheme.coefmat_, frac_try)
    plt.figure()
    plt.plot(0,0,'ro')
    for i, point in enumerate(scheme.stencil):
        if is_fraction:
            poly_str = _str_frac_poly(numer[i], denom, scheme.atoms, sep=sep)
        else:
            poly_str = _str_float_poly(scheme.coefmat_[i,:], scheme.atoms, sep=sep)
        if poly_str=='':
            continue
        else:
            plt.text(point[0], point[1], r'$'+poly_str+r'$',
                     {'ha': 'left', 'va': 'bottom'}, rotation=rotation)
            plt.plot(point[0], point[1],'k.')
    plt.gca().set_aspect('equal')
    plt.Grid(True)

def stability(scheme, Nx, Ny, xlim=(-1,1), ylim=(-1,1), ke=5, klim=1, low_memory=True):
    """Stability region, calculated by brute force"""
    assert scheme.ndim==2
    kk = np.linspace(-klim, klim, 2**ke+1) # not sure how many we really need
    x_ = np.linspace(xlim[0], xlim[1], Nx)
    y_ = np.linspace(ylim[0], ylim[1], Ny)
    # For the new plt.pcolor
    xc_ = (x_[1:]+x_[:-1])/2
    yc_ = (y_[1:]+y_[:-1])/2
    xp_ = np.hstack((2*xc_[0]-xc_[1], xc_, 2*xc_[-1]-xc_[-2]))
    yp_ = np.hstack((2*yc_[0]-yc_[1], yc_, 2*yc_[-1]-yc_[-2]))
    x, y = CO.ndgrid(x_, y_)
    r = (x, y)
    Nk = len(kk)
    kx, ky = CO.ndgrid(kk, kk)
    f = np.array([np.exp((kx*point[0]+ky*point[1])*np.pi*1j) for point in scheme.stencil])
    # f.shape==(npoints, Nk, Nk)
    atoms_r = np.array([np.prod([xi**p for xi, p in zip(r, atom)], axis=0) for atom in scheme.atoms])
    # atoms_r.shape == (dof, Nx, Ny)
    if low_memory:
        fxcoef = f.reshape((scheme.npoints,Nk*Nk)).T @ scheme.coefmat_
        # fxcoef.shape == (Nk*Nk, dof)
        stab = np.zeros((Nx, Ny))
        for ix in range(Nx):
            for iy in range(Ny):
                #coef = scheme.coefmat_ @ atoms_r[:,ix,iy]
                stab[ix, iy] = np.max(np.abs(fxcoef @ atoms_r[:,ix,iy]))-1
    else:
        coef = np.sum(np.tile(scheme.coefmat_[:,:,None,None],[1,1,Nx,Ny])
                      * np.tile(atoms_r[None,:,:,:], [scheme.npoints, 1, 1, 1]), axis=1)
        # coef.shape == (npoints, Nx, Ny)
        # Do everything at once, but may take a lot of memory, esp. if Nk is large
        # And not really much faster!
        res = np.sum(np.tile(f.transpose(1,2,0)[:,:,:,None,None], [1, 1, 1, Nx, Ny])
                     * np.tile(coef[None,None,:,:,:],[Nk, Nk, 1, 1, 1]), axis=2)
        # res.shape = (Nk, Nk, Nx, Ny)
        stab = np.max(np.max(np.abs(res), axis=0), axis=0)-1
        # stab.shape = (Nx, Ny)
    # stab==0 means stable, stab>0 is unstable
    # stab<0 would mean stable but it never happens because kx=ky=0 always gives 0
    return x_, y_, stab, xp_, yp_

def plot_stability(xp, yp, stab):
    plt.figure()
    plt.pcolor(xp, yp, np.log10(stab+1e-10).T)
    plt.colorbar()
    plt.gca().set_aspect('equal')
    plt.Grid(True)
    #AS.plot_stencil()
    
def demo_stability(scheme, Nx, Ny, xlim=(-1,1), ylim=(-1,1), ke=5, klim=1):
    x, y, stab, xp, yp = stability(scheme, Nx, Ny, xlim=xlim, ylim=ylim, ke=ke, klim=klim)
    plot_stability(xp, yp, stab)
    plot_stencil(scheme.stencil)
