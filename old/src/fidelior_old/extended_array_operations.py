#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 15 01:08:30 2020

User-accessible error-checked elementary operations for extended_array

@author: nleht
"""

import numpy as np
from .extended_arrays import ExtendedArray, _diff, _mean, _up, _up1, _interp_up

# Too many functions, may be errors in checks and comments!

#%% Elementary operations
# Convention:
#   f is a node-field, u is a center-field
def c_diff(f, axis=None):
    "Difference applied to node-field, result is center-field"
    if f.ndim==1 and axis is None: axis=0
    assert f.stags[axis] == 0
    return _diff(f, axis)
def n_diff(u, axis=None):
    "Difference applied to center-field, result is node-field"
    if u.ndim==1 and axis is None: axis=0
    assert u.stags[axis] == 1
    return _diff(u, axis)
def c_mean(f, axis=None):
    "Average applied to node-field, result is center-field"
    if f.ndim==1 and axis is None: axis=0
    assert f.stags[axis] == 0
    return _mean(f, axis)
def n_mean(u, axis=None):
    "Average applied to center-field, result is node-field"
    if u.ndim==1 and axis is None: axis=0
    assert u.stags[axis] == 1
    return _mean(u, axis)

#%% Upstream values used in advection algorithms, based on _up()
# Convention:
#   f is a node-field, u is a center-field
#   v is a center-velocity, w is a node-velocity, s is a scalar (uniform) velocity

def c_ups(f, s, axis=None):
    """Upwind value applied to node-field, with scalar velocity; result is center-field.
    Velocity used is the dimensionless Courant–Friedrichs–Lewy parameter V*dt/dx"""
    assert isinstance(f, ExtendedArray) and np.isscalar(s)
    if f.ndim==1 and axis is None: axis=0
    assert f.stags[axis]==0
    return _up(f, s, axis)

def n_ups(u, s, axis=None):
    """Upwind value applied to center-field, with scalar velocity; result is node-field.
    Velocity used is the dimensionless Courant–Friedrichs–Lewy parameter V*dt/dx"""
    assert isinstance(u, ExtendedArray) and np.isscalar(s)
    if u.ndim==1 and axis is None: axis=0
    assert u.stags[axis]==1
    return _up(u, s, axis)   

def c_upc(f, v, axis=None):
    """Upwind value applied to node-field, with center-velocity; result is center-field.
    Velocity used is the dimensionless Courant–Friedrichs–Lewy parameter V*dt/dx"""
    assert isinstance(f, ExtendedArray) and isinstance(v, ExtendedArray)
    if f.ndim==1 and axis is None: axis=0
    for a in range(f.ndim):
        if a != axis: assert (f.stags[a]==v.stags[a] or f.stags[a] is None)
    assert f.stags[axis]==0 and v.stags[axis]==1
    return _up(f, v, axis)

def n_upc(u, v, axis=None):
    """Upwind value applied to center-field, with center-velocity; result is node-field.
    Velocity used is the dimensionless Courant–Friedrichs–Lewy parameter V*dt/dx"""
    assert isinstance(u, ExtendedArray) and isinstance(v, ExtendedArray)
    if u.ndim==1 and axis is None: axis=0
    for a in range(u.ndim):
        if a != axis: assert (u.stags[a]==v.stags[a] or u.stags[a] is None)
    assert u.stags[axis]==1 and v.stags[axis]==1
    return _up(u, v, axis)

def c_upn(f, w, axis=None):
    """Upwind value applied to node-field, with node-velocity; result is center-field.
    Velocity used is the dimensionless Courant–Friedrichs–Lewy parameter V*dt/dx"""
    assert isinstance(f, ExtendedArray) and isinstance(w, ExtendedArray)
    if f.ndim==1 and axis is None: axis=0
    for a in range(f.ndim):
        if a != axis: assert (f.stags[a]==w.stags[a] or f.stags[a] is None)
    assert f.stags[axis]==0 and w.stags[axis]==0
    return _up(f, w, axis)

def n_upn(u, w, axis=None):
    """Upwind value applied to center-field, with node-velocity; result is node-field.
    Velocity used is the dimensionless Courant–Friedrichs–Lewy parameter V*dt/dx"""
    assert isinstance(u, ExtendedArray) and isinstance(w, ExtendedArray)
    if u.ndim==1 and axis is None: axis=0
    for a in range(u.ndim):
        if a != axis: assert (u.stags[a]==w.stags[a] or u.stags[a] is None)
    assert u.stags[axis]==1 and w.stags[axis]==0
    return _up(u, w, axis)

#%% Upstream-interpolated values, used in advection algorithms based on _interp_up()
# For conventions, see the functions c_up*, n_up* defined above
def n_interp_ups(f, s, axis=None):
    """Value interpolated upwind, for node-field and scalar-velocity; result is node-field.
    Velocity used is the dimensionless Courant–Friedrichs–Lewy parameter V*dt/dx"""
    assert isinstance(f, ExtendedArray) and np.isscalar(s)
    if f.ndim==1 and axis is None: axis=0
    assert f.stags[axis]==0
    return _interp_up(f, s, axis)

def c_interp_ups(u, s, axis=None):
    """Value interpolated upwind, for center-field and scalar-velocity; result is center-field.
    Velocity used is the dimensionless Courant–Friedrichs–Lewy parameter V*dt/dx"""
    assert isinstance(u, ExtendedArray) and np.isscalar(s)
    if u.ndim==1 and axis is None: axis=0
    assert u.stags[axis]==1
    return _interp_up(u, s, axis)

def n_interp_upc(f, v, axis=None):
    """Value interpolated upwind, for node-field and center-velocity; result is node-field.
    Velocity used is the dimensionless Courant–Friedrichs–Lewy parameter V*dt/dx"""
    assert isinstance(f, ExtendedArray) and isinstance(v, ExtendedArray)
    if f.ndim==1 and axis is None: axis=0
    for a in range(f.ndim):
        if a != axis: assert f.stags[a]==v.stags[a]
    assert f.stags[axis]==0 and v.stags[axis]==1
    return _interp_up(f, v, axis)

def c_interp_upc(u, v, axis=None):
    """Value interpolated upwind, for center-field and center-velocity; result is center-field.
    Velocity used is the dimensionless Courant–Friedrichs–Lewy parameter V*dt/dx"""
    assert isinstance(u, ExtendedArray) and isinstance(v, ExtendedArray)
    if u.ndim==1 and axis is None: axis=0
    for a in range(u.ndim):
        if a != axis: assert u.stags[a]==v.stags[a]
    assert u.stags[axis]==1 and v.stags[axis]==1
    return _interp_up(u, v, axis)

def n_interp_upn(f, w, axis=None):
    """Value interpolated upwind, for node-field and node-velocity; result is node-field.
    Velocity used is the dimensionless Courant–Friedrichs–Lewy parameter V*dt/dx"""
    assert isinstance(f, ExtendedArray) and isinstance(w, ExtendedArray)
    if f.ndim==1 and axis is None: axis=0
    for a in range(f.ndim):
        if a != axis: assert f.stags[a]==w.stags[a]
    assert f.stags[axis]==0 and w.stags[axis]==0
    return _interp_up(f, w, axis)

def c_interp_upn(u, w, axis=None):
    """Value interpolated upwind, for center-field and node-velocity; result is center-field.
    Velocity used is the dimensionless Courant–Friedrichs–Lewy parameter V*dt/dx"""
    assert isinstance(u, ExtendedArray) and isinstance(w, ExtendedArray)
    if u.ndim==1 and axis is None: axis=0
    for a in range(u.ndim):
        if a != axis: assert u.stags[a]==w.stags[a]
    assert u.stags[axis]==1 and w.stags[axis]==0
    return _interp_up(u, w, axis)
