#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
FIDELIOR
--------

FInite-Difference-Equation LInear OperatoR package.

To get started, visit the project page at <https://gitlab.com/nleht/fidelior>.

FIDELIOR (c) by Nikolai G. Lehtinen

FIDELIOR is licensed under a
Creative Commons Attribution-NoDerivatives 4.0 International License.

You should have received a copy of the license along with this
work. If not, see <http://creativecommons.org/licenses/by-nd/4.0/>.
"""

__version__ = '0.7.6'
print('Welcome to FIDELIOR, v.',__version__)
print('(c) by Nikolai G. Lehtinen; licensed under CC BY-ND 4.0.')

from .core import *

print('Make finite-difference coding great again!')
