#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 19 13:12:32 2022

Setting up equations for multiple inter-related variables.

This file is a part of "Examples" section of FIDELIOR package <https://gitlab.com/nleht/fidelior>.

This file (c) by Nikolai G. Lehtinen

This file is licensed under a
Creative Commons Attribution 4.0 International License.

You should have received a copy of the license along with this
work. If not, see <http://creativecommons.org/licenses/by/4.0/>.
"""

import time
import numpy as np
from matplotlib import pyplot as plt
import fidelior as fdo
from fidelior import end, half, Box, EACollection

_debug = False # Extra information calculated and printed

# A very simple geometry with dx=dy=1
box = Box((30,25))
x_ = np.arange(box.num_cells[0]+1)
y_ = np.arange(box.num_cells[1]+1)
xav = (x_[1:] + x_[:-1])/2
yav = (y_[1:] + y_[:-1])/2

# Various knowns and placeholders for unknowns
Exv = box[half:end-half, 0:end]
Eyv = box[0:end, half:end-half]
rho = box[1:end-1, 1:end-1]
rho.assign(np.random.randn(*rho.nt))
dBzdt = box[half:end-half, half:end-half]
dBzdt.assign(np.random.randn(*dBzdt.nt))
dBzdt -= fdo.mean(dBzdt) # so that the circulation over boundary is zero
# Integrate dBzdt over surface
dPhizdt = fdo.cumsum(fdo.cumsum(dBzdt, axis=0), axis=1)[end, end] # the total dBz/dt flux
print('dPhizdt =', dPhizdt, '(must be zero)')

# A collection of sevaral variables (numeric values) -- for now, empty
unknowns = EACollection([Exv, Eyv])

t0 = time.time()
E = unknowns.sym('vars') # E is [Ex. Ey]
print('Init time:', time.time()-t0)

# Differential operators (dx=dy=1)
def div(E):
    return fdo.diff(E[0], 0) + fdo.diff(E[1], 1)

def curl_z(E):
    return fdo.diff(E[1], 0) - fdo.diff(E[0], 1)

#%% Set up equations/constraints
t0 = time.time()
(curl_z(E) == -dBzdt).constraint('Faraday')
(div(E) == rho).constraint('Gauss')

# Boundary conditions -- the boundary is conducting so E_parallel = 0
((E[0][:,0] == 0) & (E[0][:, end]==0)).constraint('Ex_boundary')
((E[1][0,:] == 0) & (E[1][end, half:end-1-half] == 0)).constraint('Ey_boundary')
# Note: one corner missing
# Eyv[end, end-half] == 0 automatically because integral of E along boundary is zero
# -- it is equal to the integral of curl over the whole volume.
print('Setup time:', time.time()-t0)

# Debugging information
if _debug:
    print('Are constraints non-conflicting?', fdo.good_constraints(E))
info = fdo.constraint_info(E) # or fdo.constraint_info(unknowns)
print('Constraint names and numbers:', info)
# We see that no more constraints can be set (because output has 'None: 0')

#%% We can solve an empty equation
t0 = time.time()
# 'solution' contains the numerical values for both Ex and Ey, stored in a 1D ndarray
# whose structure is unimportant -- it will only be used by the EACollection internally.
solution = fdo.empty_equality(unknowns).solve()
# Fill the numeric values in the numeric collection
unknowns.assign(solution)
# Now, Exv and Eyv have the values satisfying the above equations.
print('Solution time:', time.time()-t0)

print('Remaining value on the boundary Eyv[end, end-half] =', Eyv[end, end-half])
# Now, Exv and Eyv satisfy all of the above constraints

#%% Plot the results
Exc = fdo.aver(Exv, 1) # has size [half:end-half, half:end-half]
Eyc = fdo.aver(Eyv, 0) # has size [half:end-half, half:end-half]
plt.figure(1)
plt.clf()
plt.quiver(xav, yav, Exc[:,:].T, Eyc[:,:].T)
plt.gca().set_aspect('equal')
plt.xlabel('x')
plt.ylabel('y')
plt.title(r'Electric field satisfying $(\nabla\times E)_z=dB_z/dt$ and $\nabla\cdot E=\rho$')
