#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This is very similar to the quick test from the README file (see quick_test.py). 
Here, we also solve a Poisson equation on a 2D grid.
The difference is that we use boundary conditions which allow gauge
invariance of the electrostatic potential:
    phi -> phi + const
The solution is only possible when the total charge in the simulation
domain is zero.

This file is a part of "Examples" section of FIDELIOR package <https://gitlab.com/nleht/fidelior>.

This file (c) by Nikolai G. Lehtinen

This file is licensed under a
Creative Commons Attribution 4.0 International License.

You should have received a copy of the license along with this
work. If not, see <http://creativecommons.org/licenses/by/4.0/>.
"""

import numpy as np
from matplotlib import pyplot as  plt
import fidelior as fdo
from fidelior import end, half

need_gauge = True # Set to False to check with a problem that does not need the gauge condition
do_gauge = True # Try setting to False, the condition number is bad! (calculated for N<=50)

# Set up the simple geometry
N = 100 # set to N<=50 to check the bad condition number
x1 = np.arange(N+1)-N/2; y1 = x1
box = fdo.Box((N, N))
x, y = box.ndgrid(x1, y1)[0:end, 0:end]
gauge_location = (0, 0) # where we fix phi (must be within [0:end, 0:end])
exclude_location = (10, 10) # where we exclude rho from equation (must be within [1:end-1, 1:end-1])

# Charge density
x0 = 25; y0 = -25; w = 10;
rho = fdo.exp(-((x-x0)**2+(y-y0)**2)/(2*w**2)).restrict[1:end-1,1:end-1] # no rho at the border

# The charge must be zero
rho -= np.mean(rho[:,:])
print('Total charge =', np.sum(rho[:,:]), '(must be zero)')

#%% Unknown electrostatic potential
phi = box.sym('phi')[0:end, 0:end]

# These functions use 'fdo.diff(u, axis)' which is itself defined as
# def diff(u, axis): u.shift(half, axis) - u.shift(-half, axis)
def grad(u):
    return (fdo.diff(u, axis=0), fdo.diff(u, axis=1))
def div(A):
    return fdo.diff(A[0], axis=0) + fdo.diff(A[1], axis=1)

# Unknown electric field
E = grad(-phi)

# Boundary conditions are different from the file quick_test.py
(phi[0:1, :] == phi[end-1:end, :]).constraint('x-periodic')
if need_gauge:
    # With this b.c., transformation phi -> phi + const also gives a valid solution
    (E[1][1:end-1, [half, end-half]] == 1).constraint('Neumann') # Ey = 1 on top and bottom
else:
    # Transformation phi -> phi + const is impossible
    (phi[1:end-1, [0, end]] == 0).constraint('Dirichlet') # a simpler b.c. which does not require gauge

# Set up and solve the equation
if do_gauge and need_gauge:
    # Correct solution with fixing gauge - of course, will not work with need_gauge == False
    (phi[gauge_location] == 0).constraint('gauge') # can set to arbitrary constant
    include = ~rho.as_zeros('bool')
    include[exclude_location] = False
    equation = fdo.Equation(div(E)[include] == rho[include])
else:
    equation = fdo.Equation(div(E) == rho)
print('The constraints are:', fdo.constraint_info(phi))

if N <= 50:
    # To calculate the condition number, we need to convert to a dense matrix, so it should not be too large
    A = equation.system.M.A # the matrix whose condition number we want to calculate
    print('Condition number =', np.linalg.cond(A), '(bad if too large)')
    
phiv = equation.solve()

#%% Plot and check the error
plt.figure(1)
plt.clf()
plt.pcolor(x1, y1, phiv[:,:].T, shading='auto')
plt.gca().set_aspect('equal')
plt.colorbar()
print('Error =', fdo.max(fdo.abs(div(grad(-phiv))-rho)))
