#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 21 16:54:23 2022

Solving Poisson equation with a refined mesh.

The solutions at the coarser grid and at the finer grid are sewn together at the boundary
between them.

This file is a part of "Examples" section of FIDELIOR package <https://gitlab.com/nleht/fidelior>.

This file (c) by Nikolai G. Lehtinen

This file is licensed under a
Creative Commons Attribution 4.0 International License.

You should have received a copy of the license along with this
work. If not, see <http://creativecommons.org/licenses/by/4.0/>.
"""

#%% Preliminaries
import time
import numpy as np
from matplotlib import pyplot as plt
import fidelior as fdo
from fidelior import end, half

#%% Setup
# Coarse grid
Nx, Ny = 35, 20
# Location of the fine grid
sub1 = (slice(14,21), slice(7,13))

# Vary these variables to see how the solution changes
rho_x = 16
rho_y = 9
rho_w = .5
def rho_fun(x, y):
    return fdo.exp(-((x-rho_x)**2+(y-rho_y)**2)/(2*rho_w**2))

#%% A useful functions
def uniform_mesh(box, starts=None, dxs=None):
    if starts is None:
        starts = (0,)*box.ndim
    if dxs is None:
        dxs = (1,)*box.ndim
    return tuple(start + np.arange(nc+1, dtype='double')*dx
                 for nc, start, dx in zip(box.num_cells, starts, dxs))

#%% Geometry
# Coarse grid
box0 = fdo.Box((Nx, Ny)) # the big one
x_, y_ = uniform_mesh(box0)
x, y = box0.ndgrid(x_, y_)[0:end, 0:end]
# Fine grid
sub1in = tuple(slice(s.start+1, s.stop-1) for s in sub1)
sub1in2 = tuple(slice(s.start+2, s.stop-2) for s in sub1)
Nx1 = (sub1[0].stop-sub1[0].start)*2
Ny1 = (sub1[1].stop-sub1[1].start)*2
box1 = fdo.Box((Nx1, Ny1)) # Small one, located at sub1 = [14:21, 7:13]
x1_, y1_ = uniform_mesh(box1, (x_[sub1[0].start], y_[sub1[1].start]), (1/2, 1/2))
x1, y1 = box1.ndgrid(x1_, y1_)[0:end, 0:end]

#%% Variables
# Where to use lap(phi0)=rho0
area0 = ~box0.zeros('bool')[0:end, 0:end]
area0[0:end, [0,end]] = False
area0[[0,end], 0:end] = False
area0[sub1in] = False

figsize = (10, 6)
plt.figure(1, figsize=figsize)
plt.clf()
plt.plot(x._arr, y._arr, 'k.-')
plt.plot(x._arr.T, y._arr.T, 'k-')
plt.plot(x1._arr, y1._arr, 'r.-')
plt.plot(x1._arr.T, y1._arr.T, 'r-')
plt.gca().set_aspect('equal')
plt.plot(x[area0], y[area0], 'go', fillstyle='none')
plt.plot(x1[1:end-1,1:end-1], y1[1:end-1,1:end-1], 'bo', fillstyle='none')

rho0 = box0[1:end-1, 1:end-1].assign(rho_fun(x, y))
rho1 = box1[1:end-1, 1:end-1].assign(rho_fun(x1, y1))

phi0v = box0[0:end, 0:end]
phi1v = box1[0:end, 0:end]

phi = fdo.EACollection([phi0v, phi1v])

#%% Set up and solve equation
def Laplacian(p, dx):
    return (fdo.diff(fdo.diff(p, 0), 0) + fdo.diff(fdo.diff(p, 1), 1))/dx**2

phi0, phi1 = phi.sym('phi')

# Constraints
((phi0[[0,end],:]==0) & (phi0[1:end-1, [0,end]]==0)).constraint('outer boundary')
plt.figure(1)
plt.plot(x[[0,end],:], y[[0,end],:], 'yx')
plt.plot(x[1:end-1, [0,end]], y[1:end-1, [0,end]], 'yx')

# The common points on the coarse and fine grids
(phi0[sub1] == phi1[::2,::2]).constraint('common')

# Poisson equation(s).
# We set them up either here as constraints or (see below) as a regular equation
# (lap(phi0,1)[area0] == -rho0[area0]).constraint('phi0')
# (lap(phi1,1/2) == -rho1).constraint('phi1')

# Now, the tricky part: sew together at the boundary of the fine grid.
# Only at the points not present in the coarse grid -- those have been taken care of already.
horz1 = phi1[[0,end], 1:end:2] # on the left and right sides of the fine grid
vert1 = phi1[1:end:2, [0,end]] # on the top and the bottom of the fine grid
plt.figure(1)
plt.plot(x1[[0,end], 1:end:2], y1[[0,end], 1:end:2], 'mx')
plt.plot(x1[1:end:2, [0,end]], y1[1:end:2, [0,end]], 'mx')
plt.title('Coarse (black) and fine (red) grid\n'
          'Circles: Poisson equation on coarse (green), on fine (blue)\n'
          'Crosses: sew-together (magenta), outer boundary (yellow)')

# Interpolation at the midpoints at the boundary of the fine grid
order = 3
if order == 1:
    # Linear interpolation of intermediate points
    phix = (phi0.at((half, 0)) + phi0.at((-half, 0)))/2 # or just fdo.aver(phi0, 0)
    phiy = (phi0.at((0, half)) + phi0.at((0, -half)))/2 # or just fdo.aver(phi0, 1)
elif order == 2:
    # Second order upshifted
    phix = (phi0.at((-half, 0))*3 + phi0.at((half, 0))*6 - phi0.at((1+half, 0)))/8
    phiy = (phi0.at((0, -half))*3 + phi0.at((0, half))*6 - phi0.at((0, 1+half)))/8
elif order == 3:
    # Third-order interpolation at midpoints
    # Works only if the fine grid does not touch the boundary
    phix = ((phi0.at((half, 0)) + phi0.at((-half, 0)))*9 - \
            (phi0.at((1+half, 0)) + phi0.at((-1-half, 0))))/16
    phiy = ((phi0.at((0, half)) + phi0.at((0, -half)))*9 - \
            (phi0.at((0, 1+half)) + phi0.at((0, -1-half))))/16

# Same points on the coarse grid
sub1mid = tuple(slice(s.start+half, s.stop-half) for s in sub1)
horz0 = phiy[[sub1[0].start, sub1[0].stop], sub1mid[1]]
vert0 = phix[sub1mid[0], [sub1[1].start, sub1[1].stop]]

# Finally, set up the constraint
((horz1 == horz0) & (vert1 == vert0)).constraint('sew')

#%% Solve
equation = fdo.Equation((Laplacian(phi0, dx=1)[area0] == -rho0[area0]) &
                        (Laplacian(phi1, dx=1/2) == -rho1))
t0 = time.time()
solution = equation.solve()
# solution = fdo.empty_equality(phi).solve() # if Poisson equations were set up as constraints
print('Solution time:', time.time()-t0)
vmin, vmax = min(solution), max(solution)
phi.assign(solution)

#%%
plt.figure(2, figsize=figsize)
plt.clf()
plt.pcolor(x_, y_, fdo.aver(fdo.aver(phi0v, 0), 1)._arr.T, vmin=vmin, vmax=vmax, edgecolor='w')
plt.pcolor(x1_, y1_, fdo.aver(fdo.aver(phi1v, 0), 1)._arr.T, vmin=vmin, vmax=vmax, edgecolor='w')
plt.gca().set_aspect('equal')
plt.title(r'Solution of $\nabla^2\phi=-\rho$ with $\phi=0$ at boundary')
plt.xlabel('x')
plt.ylabel('y')
#plt.savefig('figures/example6.png')
#plt.colorbar()
