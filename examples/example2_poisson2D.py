#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jan  8 17:07:13 2022

This file is a part of "Examples" section of FIDELIOR package <https://gitlab.com/nleht/fidelior>.

This file (c) by Nikolai G. Lehtinen

This file is licensed under a
Creative Commons Attribution 4.0 International License.

You should have received a copy of the license along with this
work. If not, see <http://creativecommons.org/licenses/by/4.0/>.
"""

import numpy as np
import matplotlib.pyplot as plt
import fidelior as fdo
from fidelior import Box, end, half
fdo.set_sparse(True) # not necessary, this is the default -- check with fdo.USE_SPARSE

#%% Set up 2D Cartesian nonuniform grid
x_ = np.sinh(np.linspace(-10,10,101))
Nx = len(x_)-1
Ny = Nx

#%% Geometry
box = Box((Nx, Ny))
x, y = box.ndgrid(x_, x_)[0:end, 0:end]
dx = fdo.diff(x, 0)
dy = fdo.diff(y, 1)
dxc = fdo.aver(dx, 0)
dyc = fdo.aver(dy, 1)
r = fdo.sqrt(x**2+y**2) # fidelior implements all basic functions with ExtArray arguments

#%% Physics
phi = box.sym('phi')[0:end,0:end] # a symbolic array - or x.as_sym('phi')
w = 1 # the radius of Gaussian charge distribution
rho = box[1:end-1,1:end-1].assign(fdo.exp(-r**2/(2*w**2))/(2*np.pi*w**2))
r[Nx//2, Ny//2] = r[Nx//2, Ny//2+1] # To avoid division by zero

#%% The ratios Ex/phi, Ey/phi at the boundary for Robin boundary conditions
# These are the ratios -(grad G)_{x,y}/G where G is the Green's function for a source at r==0
ratiox = -x/r**2/fdo.log(r)
ratioy = -y/r**2/fdo.log(r)

#%% Discretized differential operators
def grad(u):
    return fdo.diff(u, 0)/dx, fdo.diff(u, 1)/dy

def div(A):
    return fdo.diff(A[0], 0)/dxc + fdo.diff(A[1], 1)/dyc

# Symbolic electric field (two components)
E = grad(-phi)

#%% Boundary conditions
robinx = fdo.aver(phi*ratiox, 0) - E[0]
robiny = fdo.aver(phi*ratioy, 1) - E[1]
bc_type = 'Robin_simple'
if bc_type=='Dirichlet':
    ((phi[0:end, 0]==0) & (phi[0:end, end]==0) & \
     (phi[0, 1:end-1]==0) & (phi[end, 1:end-1]==0)).constraint('Dirichlet')
elif bc_type=='Robin':
    ((robinx[half, 1:end-1] == 0) & (robinx[end-half, 1:end-1] == 0) & \
     (robiny[1:end-1, half] == 0) & (robiny[1:end-1, end-half] == 0)).constraint('Robin')
    # The corners are irrelevant, but necessary to have the correct number of equations
    ((phi[0,0] == (phi[1,0]+phi[0,1])/2) & (phi[0,end] == (phi[0,end-1]+phi[1,end])/2) & \
     (phi[end,0] == (phi[end,1]+phi[end-1,0])/2) & \
         (phi[end,end] == (phi[end-1,end]+phi[end,end-1])/2)).constraint('Robin_corners')
    # Could just use:
    # ((phi[0,0] == 0) & (phi[0,end] == 0) & \
    #    (phi[end,0] == 0) & (phi[end,end] == 0)).constraint('Robin_corners')
    # but then when plotting all values of phi, there would be a discontinuity in the corners
elif bc_type=='Robin_simple':
    # Corners are included in Robin BC. The disadvantage is that they are not symmetric anymore
    ((robinx[half, 0:end] == 0) & (robinx[end-half, 0:end] == 0) & \
     (robiny[1:end-1, half] == 0) & (robiny[1:end-1, end-half] == 0)).constraint('Robin')

#%% Check validity of constraints
if not fdo.good_constraints(phi):
    raise ValueError('There are conflicting constraints!')

#%% Solve it!
phiv = (div(E) == rho).solve() # Numerical value
phi0 = -fdo.log(r)/(2*np.pi)   # Theoretical value

#%% Plots
plt.figure(1)
plt.clf()
xav = (x_[1:]+x_[:-1])/2
plt.pcolor(xav, xav, phiv[1:end-1, 1:end-1].T)
plt.title('Potential')
plt.xlabel('x')
plt.ylabel('y')

#%%
plt.figure(2)
plt.clf()
plt.plot(x_, phiv[0:end, Ny//2])
plt.plot(x_, phi0[0:end, Ny//2])
plt.legend(['Calculated', 'Theoretical'])
plt.xlabel('x')
plt.title('Potential at y==0')

#%% The error
err = phiv - phi0
err[r<5] = np.nan # Exclude from plotting the area where delta function is not well represented
plt.figure(3)
plt.clf()
plt.plot(x_, err[0:end, Ny//2])
plt.xlabel('x')
plt.title('Error at y==0')

#%% The charge density
plt.figure(4)
plt.clf()
plt.pcolor(xav, xav, rho[1:end-1, 1:end-1].T)
plt.title('Charge density')
plt.xlabel('x')
plt.ylabel('y')

#%%
plt.figure(6)
plt.clf()
# Could do just Exv, Eyv = grad(-phiv), but let us demonstrate how operators are applied
Exv = E[0](phiv) # or E[0].apply(phiv)
Eyv = E[1](phiv)
Exc = fdo.aver(Exv, 1)
Eyc = fdo.aver(Eyv, 0)
plt.quiver(xav, xav, Exc[half:end-half, half:end-half].T, Eyc[half:end-half, half:end-half].T)
# or just plt.quiver(xav, xav, Exc._arr.T, Eyc._arr.T)
plt.title('Electric field')
plt.xlabel('x')
plt.ylabel('y')
