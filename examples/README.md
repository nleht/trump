# Examples

![License: CC BY 4.0](https://img.shields.io/badge/License-CC_BY_4.0-lightgrey.svg)

Examples, unlike the [code](/src/fidelior), are under Creative Commons [Attribution 4.0 International License (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/). It means that you may modify and distribute them, but still must give credit to the original author.

In addition to _FIDELIOR_ requirements, these examples also need [Matplotlib](https://matplotlib.org/) for plotting. The best way to run these examples is in [Spyder](https://www.spyder-ide.org/). If using [IPython](https://ipython.org/), you may need to type `plt.show()` after running the scripts in order to show the plots.

Before starting, make sure you are familiar with the [project description](https://gitlab.com/nleht/fidelior) and the [introduction to basic features](https://gitlab.com/nleht/fidelior/-/tree/master/doc).

Contents:
1. [Diffusion equation in 1D](#1-diffusion-equation-in-1d)
2. [Poisson equation in 2D](#2-poisson-equation-in-2d)
3. [Poisson equation in 3D](#3-poisson-equation-in-3d)
4. [Advection in 1D using dG method](#4-advection-in-1d-using-dg-method)
5. [Multiple unknown functions](#5-multiple-unknown-functions)
6. [Mesh refinement](#6-mesh-refinement)
7. [Advection in 2D and 3D](#7-advection-in-2d-and-3d)
8. [Poisson equation with a fixed gauge](#8-poisson-equation-with-a-fixed-gauge)

## 1. Diffusion equation in 1D

File: [`example1_diffusion1D.py`](example1_diffusion1D.py)

Application of an implicit scheme to a one-dimensional diffusion equation
```math
\frac{\partial f}{\partial t} = D\nabla^2 f
```
with boundary conditions _f_(1)=0 and _f_(0)=_g_(_t_), where the function _g_(_t_) is represented by Python function `left_edge(t)`. Time stepping is done by the implicit mid-point scheme
```math
\frac{f^{n+1}_k - f^n_k}{\Delta t} = D\frac{\left(\nabla^2 f^n\right)_k + \left(\nabla^2 f^{n+1}\right)_k}{2}
```
where $`f^{n+1}`$ and $`f^n`$ are the values of _f_  at the next and the current time step, respectively. This scheme translates into the following code:
```python
(f[0] == left_edge(0)).constraint('left') # variable bc at x==0
(f[end] == 0).constraint('right')         # fixed bc at x==1
equation = Equation(f - laplacian(f)*D*dt/2 == fv + laplacian(fv)*D*dt/2)
```
where `f` is a symbolic array representing the unknown $`f^{n+1}`$, and `fv` is a numeric array representing the given $`f^n`$.

In order to save computation time, this equation is not set up at each step, but only the inhomogeneous part (or right-hand side, RHS) is updated:
```python
equation.update_rhs('left', left_edge(t+dt))         # update one of the constraints
equation.update_rhs(None, fv + laplacian(fv)*D*dt/2) # update the main equation
fv = equation.solve() # or fv = fdo.solve(equation)
```
Note that the main equation uses `None` instead of a name when its RHS is updated.

## 2. Poisson equation in 2D

File: [`example2_poisson2D.py`](example2_poisson2D.py)

Find electrostatic potential with a given charge distribution and teh Robin boundary conditions (BC), in two dimensions and with a variable-step (non-uniform) grid.

The Robin BC are
```math
-\frac{\partial G/\partial n}{G}\phi + \frac{\partial\phi}{\partial n} = 0
```
where $`G = -\frac{1}{2\pi}\log r`$ is the Green function of the Poisson equation. The error is compared to the analytical solution and is demonstrated to be small.

## 3. Poisson equation in 3D

File: [`example3_poisson3D.py`](example3_poisson3D.py)

Find electrostatic potential _φ_ with a given charge distribution _ρ_ and three different boundary conditions.

Here is the complete code needed to find _φ_ in the third case:
```python
import numpy as np
import fidelior as fdo
from fidelior import end, half, Box, diff

Nx = 25; Ny = 25; Nz = 25;
box = Box((Nx, Ny, Nz))
dx = 1/Nx; dy = 1/Ny; dz = 1/Nz

ρ = box[1:end-1, 1:end-1, 1:end-1] # ρ not needed at the boundary; start at 1 and stop at end-1
ρ.assign(np.ones(ρ.nt)) # set ρ=1
φ = box.sym('φ')[0:end, 0:end, 0:end] # the unknown
Δφ = diff(diff(φ,0),0)/dx**2 + diff(diff(φ,1),1)/dy**2 + diff(diff(φ,2),2)/dz**2
# Set φ == 0 on all facets but one
((φ[end,:,:] == 0) & (φ[1:end-1,0,:] == 0) & (φ[1:end-1,end,:] == 0) &
 (φ[1:end-1,1:end-1,0] == 0) & (φ[1:end-1,1:end-1,end] == 0)).constraint('fixed')
(φ[0,:,:] == φ[1,:,:]).constraint('left') # ∂φ/∂n == 0 on the left facet
φ3 = (Δφ == -ρ).solve()
# Plot φ3
```
![Poisson 3D solution](figures/example3.png)

## 4. Advection in 1D using dG method

File: [`example4_dG_advection.py`](example4_dG_advection.py)

Our goal is to solve the one-dimensional advection equation,
```math
\frac{\partial u}{\partial t} + V \frac{\partial u}{\partial x} = 0
```
We use _FIDELIOR_ to implement the discontinuous Galerkin (dG) method (both upstream and symmetric). The dG method is a kind of FEM which uses discontinuous elements. It is often used for advection problems because continuous FEMs produce FD schemes which are unstable for explicit time-stepping schemes. In this example, we may specify an arbitrary order for dG elements, and choose among several explicit and implicit time-stepping schemes. The file [`dG_RK_tools.py`](dG_RK_tools.py) contains some tools for stability analysis of the time-stepping schemes used with the dG method. Unlike FD (see [Example 7](#7-advection-in-2d-and-3d) below), the dG method works well for higher orders on a highly non-uniform grid. The degree of random non-uniformity is specified with `rand_coef`, so that the lengths of elements are randomly distributed in the interval `[1-rand_coef, 1+rand_coef]`.

The figure below demonstrates advection with 6th order upstream discontinuous Galerkin spatial discretization method and with time-stepping done by 6th order [Gauss-Legendre method](https://en.wikipedia.org/wiki/Gauss%E2%80%93Legendre_method), with periodic boundary conditions. There are 50 cells, `n` indicates the number of completed loops, `V` is the Courant-Friedrichs-Lewy (CFL) number, which characterizes the velocity. The orange dots are the average values in each cell, and the blue line shows the sum of all finite elements. The blue line is discontinuous at cell boundaries, which, however, may be not visible in the plot, because the discontinuities are rather small.

![dG 1D advection](figures/example4.gif)

This example is a bit more advanced than the rest. Reading its code may be skipped when going through the examples for the first time.

## 5. Multiple unknown functions

File: [`example5_many_vars.py`](example5_many_vars.py)

Solve $`\nabla\cdot \mathbf{E} = \rho`$ and $`\nabla\times \mathbf{E} = -dB_z/dt`$ for arbitrary given _ρ_ and _dB_<sub>z</sub>/_dt_, in two dimensions. The unknown functions are _E_<sub>x</sub> and _E_<sub>y</sub>, which are related to each other in a non-trivial way.

The multiple unknown functions are set up as the following object:
```python
import fidelior as fdo
...
unknowns = fdo.EACollection([Exv, Eyv]) # Exv and Eyv are unallocated numeric arrays
E = unknowns.sym('vars') # E is [Ex. Ey], where Ex and Ey are symbolic arrays
```
Then, the we set up constraints and equations for `Ex` and `Ey`. The resulting system is solved in the following way:
```python
# An empty equation is solved because all equations were given as constraints
solution = fdo.empty_equality(unknowns).solve()
#'solution' is a numpy.ndarray and contains the numerical values for both Exv and Eyv.
# Fill the numeric values in the numeric collection:
unknowns.assign(solution)
# Now, Exv and Eyv have the values satisfying the above equations.
```
The result for random _ρ_ and _dB_<sub>z</sub>/_dt_:

![Quiver plot of Ex, Ey](figures/example5.png)

## 6. Mesh refinement

File: [`example6_refined_mesh.py`](example6_refined_mesh.py)

Solution of Poisson equation on a combination of a coarse and a fine mesh. The solution must be sewn together at the boundary between the two meshes. Like in [Example 5](#5-multiple-unknown-functions), the electrostatic potentials in the coarse-mesh (`phi0`) and fine-mesh (`phi1`) regions constitute a set of two unknown functions:
```python
import fidelior as fdo
...
box0 = fdo.Box((Nx, Ny)) # the box for the coarse mesh
...
box1 = fdo.Box((Nx1, Ny1)) # the box for the fine mesh
...
phi0v = box0[0:end, 0:end] # numerical value of phi on the coarse mesh
phi1v = box1[0:end, 0:end] # numerical value of phi on the fine mesh
phi = fdo.EACollection([phi0v, phi1v]) # the set of unknowns
phi0, phi1 = phi.sym('phi') # symbolic values of phi on coarse and fine mesh
... # set up constraints
# The system of equations for phi0 and phi1: coarse and fine Laplacian equated to rho
equation = fdo.Equation((Laplacian(phi0, dx=1)[area0] == -rho0[area0]) &
                        (Laplacian(phi1, dx=1/2) == -rho1))
solution = equation.solve()
phi.assign(solution)
# Now, phi0v and phi1v contain the found numerical values, which may be plotted.
```
![Refined Mesh](figures/example6.png)

## 7. Advection in 2D and 3D

File: [`example7_advection2D3D.py`](example7_advection2D3D.py)

Unlike [Example 4](#4-advection-in-1d-using-dg-method) above, we use finite-difference methods here. The most accurate scheme used is the 3rd-order conservative scheme, with time stepping done with the [Bogacki-Shampine embedded method](https://en.wikipedia.org/wiki/Bogacki%E2%80%93Shampine_method), which is used in a popular `ODE23` routine in MATLAB. Here is a demonstration of advection on the example of rotation:

| *Rotation in 2D* | *Rotation in 3D* |
| ---      | ---      |
| ![Rotation in 2D](figures/example7a.gif)   | ![Rotation in 3D](figures/example7b.gif)   |

Options (for both 2D and 3D) in this example include:
* Type of advection:
  - Translation
  - Rotation. Both of these types of advection have velocity of zero divergence (incompressible flow).
* Spatial FD schemes:
  - A central scheme (of the second order). This scheme is usually unstable for explicit time-stepping methods, in particular, for forward Euler.
  - A third order scheme in which the $`\nabla u`$ is evaluated at the updated point
  - A conservative third-order scheme.
* Time-stepping schemes:
  - Implicit mid-point scheme (see also [Example 1](#1-diffusion-equation-in-1d))
  - Embedded Runge-Kutta scheme ODE23 (see more info above)
  - MacCormack method - a 2nd order scheme equivalent to Lax-Wendroff method. It includes both spatial and temporal discretization.

## 8. Poisson equation with a fixed gauge

File: [`example8_fixing_gauge.py`](example8_fixing_gauge.py)

This example is a modification of the simple [demo](quick_test.py) from the [README](https://pypi.org/project/fidelior/) file, in which a Poisson equation was solved in a rectangular domain with Neumann and Dirichlet boundary conditions.

If all boundary conditions are either periodic or Neumann, then the solution of the Poisson equation $`\nabla^2 \phi = -\rho`$ continues to be valid after a gauge transformation $`\phi\rightarrow\phi+\mathit{const}`$. But then the solution is only possible if the total charge is zero, i.e., $`Q=\int \rho\,dV=0`$. This condition means that the discretized values of _ρ_ are not all independent, because one of them is fixed by _Q_=0. The numerical solution is implemented by excluding one equation from the system which represents the discretized Poisson equation, e.g, at index (_K_, _L_). This means that we do not use the value of _ρ_<sub>_K_, _L_</sub>, which is now assumed to be fixed by _Q_=0. The missing constraint is replaced with the gauge-fixing condition $`\phi_{I,J} = C`$, where the indices _I_ and _J_ may be chosen anywhere in the domain, i.e., they do not have to coincide with _K_, _L_. Here is how this is done in the code:
```python
(phi[I, J] == C).constraint('gauge')
include = ~rho.as_zeros('bool')
include[K, L] = False
# 'include' is used as a boolean index
equation = fdo.Equation(div(E)[include] == rho[include])
```
Note that if we do not fix the gauge and try to solve the equation unmodified, then the resulting matrix is singular. Because of numerical errors, SciPy does not realize that it is singular and still solves the equation. However, one may take a look at the condition number, which turns out to be enormous. In the example, the condition number is calculated if you specify size $`N\le 50`$.

## Other files

['example0_basics.py'](example0_basics.py) -- tests of some features used internally in _FIDELIOR_.

['quick_test.py'](quick_test.py) -- the demo from the README file on the [_FIDELIOR_ page on PyPI](https://pypi.org/project/fidelior/).

